#import "RecognitionViewController.h"
#import "AppDelegate.h"

//error Provide Application ID and Password
// To create an application and obtain a password,
// register at http://cloud.ocrsdk.com/Account/Register
// More info on getting your application id and password at
// http://ocrsdk.com/documentation/faq/#faq3

// Name of application you created
static NSString* MyApplicationID = @"Arandell";
// Password should be sent to your e-mail after application was created
static NSString* MyPassword = @"aOxAUX5/nxYTg1w2xGiScXOD";

@implementation RecognitionViewController

@synthesize textView;
@synthesize statusLabel;
@synthesize statusIndicator;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
	[self setTextView:nil];
	[self setStatusLabel:nil];
	[self setStatusIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	textView.hidden = YES;
	
	statusLabel.hidden = NO;
	statusIndicator. hidden = NO;
	
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	statusLabel.text = @"Loading image...";
	
	UIImage* image = [(AppDelegate*)[[UIApplication sharedApplication] delegate] imageToProcess];
	
	Client *client = [[Client alloc] initWithApplicationID:MyApplicationID password:MyPassword];
	[client setDelegate:self];
	
	if([[NSUserDefaults standardUserDefaults] stringForKey:@"installationID"] == nil) {
		NSString* deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
		
		NSLog(@"First run: obtaining installation ID..");
		NSString* installationID = [client activateNewInstallation:deviceID];
		NSLog(@"Done. Installation ID is \"%@\"", installationID);
		
		[[NSUserDefaults standardUserDefaults] setValue:installationID forKey:@"installationID"];
	}
	
	NSString* installationID = [[NSUserDefaults standardUserDefaults] stringForKey:@"installationID"];
	
	client.applicationID = [client.applicationID stringByAppendingString:installationID];
	
	ProcessingParams* params = [[ProcessingParams alloc] init];
    NSLog(@"%@",params);
	[client processBusinessCard:image withParams:params];
	
	statusLabel.text = @"Uploading image...";
	
    [super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return NO;
}

#pragma mark - ClientDelegate implementation

- (void)clientDidFinishUpload:(Client *)sender
{
	statusLabel.text = @"Processing image...";
}

- (void)clientDidFinishProcessing:(Client *)sender
{
	statusLabel.text = @"Downloading result...";
}

- (void)client:(Client *)sender didFinishDownloadData:(NSData *)downloadedData
{
	statusLabel.hidden = YES;
	statusIndicator.hidden = YES;
	
	textView.hidden = NO;
	
    [self getEachElement:downloadedData];
	NSString* result = [[NSString alloc] initWithData:downloadedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",result);
	textView.text = result; 
}
-(void)getEachElement:(NSData *)response
{
    NSString *str = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    NSArray *subStrings = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n"]];
    NSArray *getData = [[NSArray alloc]init];
    NSString *fullName;
    NSString *arr = @"";
//    NSMutableArray *arrayResult = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic_Ad = [[NSMutableDictionary alloc] init];
    for (int i=0;i<[subStrings count];i++)
    {
        arr = [subStrings objectAtIndex:i];
       // NSLog(@"%@",arr);
        NSArray *abc = [arr componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@";"]];
        
        if([[abc objectAtIndex:0] isEqualToString:@"FN"])
        {
            getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"] ];
            
            if([getData count] > 1)
                fullName = [getData objectAtIndex:1];
            
            if ([fullName length])
            {
                [dic_Ad setObject:fullName forKey:@"fullName"];
            }
            
        }
//        else if([[abc objectAtIndex:0] isEqualToString:@"N"])
//        {
//            getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]];
//            
//            if([getData count] > 1)
//                lastName = [getData objectAtIndex:1];
//            FirstName = [abc objectAtIndex:2];
//            [arrayResult addObject:[NSDictionary dictionaryWithObjectsAndKeys:FirstName,@"FirstName", nil]];
//        }
//        else if([[abc objectAtIndex:0] isEqualToString:@"TITLE"])
//        {
//            getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]];
//            
//            if([getData count] > 1)
//                title = [getData objectAtIndex:1];
//            [arrayResult addObject:[NSDictionary dictionaryWithObjectsAndKeys:title,@"title", nil]];
//        }
        else if([[abc objectAtIndex:0] isEqualToString:@"TEL"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            if([abc count] == 3)
            {
                getData=[[abc objectAtIndex:2] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
                
                if([[abc objectAtIndex:1] isEqualToString:@"WORK"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
                else if([[abc objectAtIndex:1] isEqualToString:@"CELL"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
                else if([[abc objectAtIndex:1] isEqualToString:@"VOICE"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
            }
            else if([abc count] == 4)
            {
                getData=[[abc objectAtIndex:3] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
                
                if([[abc objectAtIndex:1] isEqualToString:@"WORK"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
                else if([[abc objectAtIndex:1] isEqualToString:@"CELL"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
                else if([[abc objectAtIndex:1] isEqualToString:@"VOICE"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }

            }
            else
            {
                getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]];
                
                if([[getData objectAtIndex:0] isEqualToString:@"WORK"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
                else if([[getData objectAtIndex:0] isEqualToString:@"FAX"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
                else if([[getData objectAtIndex:0] isEqualToString:@"VOICE"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Tel"];
                }
            }
            NSLog(@"%@",dict);
            if ([dict valueForKey:@"Tel"])
            {
                [dic_Ad setObject:[dict valueForKey:@"Tel"] forKey:@"Tel"];
            }
        }
       else if([[abc objectAtIndex:0] isEqualToString:@"EMAIL"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            
            if([abc count] == 3)
            {
                getData=[[abc objectAtIndex:2] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
                
                if([[abc objectAtIndex:1] isEqualToString:@"WORK"])
                {
                    if([getData count] > 1)
                        [dict setObject:[getData objectAtIndex:1] forKey:@"Email"];
                }
            }
            else
            {
                getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
                
                if([getData count] > 1)
                    [dict setObject:[getData objectAtIndex:1] forKey:@"Email"];
            }
           // NSLog(@"%@",dict);
            if ([dict valueForKey:@"Email"])
            {
                [dic_Ad setObject:[dict valueForKey:@"Email"] forKey:@"Email"];
            }
        }
//        else if([[abc objectAtIndex:0] isEqualToString:@"ORG"])
//        {
//            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//            
//            if([abc count] == 5)
//            {
//                getData=[[abc objectAtIndex:2] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
//                
//                if([[abc objectAtIndex:1] isEqualToString:@"WORK"])
//                {
//                    NSString *orgStr = @"";
//                    if([getData count] > 1)
//                    {
//                        orgStr = [getData objectAtIndex:1];
//                    }
//                    [dict setObject:[orgStr stringByAppendingString:[abc objectAtIndex:4]] forKey:@"Org"];
//                }
//            }
//            else
//            {
//                getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet:  [NSCharacterSet characterSetWithCharactersInString:@":"]];
//                
//                if([getData count] > 1)
//                    [dict setObject:[getData objectAtIndex:1] forKey:@"Org"];
//            }
//           // NSLog(@"%@",dict);
//           [arrayResult addObject:[NSDictionary dictionaryWithObjectsAndKeys:[dict valueForKey:@"Org"],@"Org", nil]];
//        }
        else if([[abc objectAtIndex:0] isEqualToString:@"ADR"])
        {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            if([abc count] == 9)
            {
                [dict setObject:[abc objectAtIndex:4] forKey:@"Add"];
                [dict setObject:[abc objectAtIndex:8] forKey:@"Country"];
                [dict setObject:[abc objectAtIndex:7] forKey:@"Zip"];
                [dict setObject:[abc objectAtIndex:5] forKey:@"City"];
            }
            else
            {
                
            }
           // NSLog(@"%@",dict);
            [dic_Ad setObject:dict forKey:@"Address"];
           // [arrayResult addObject:[NSDictionary dictionaryWithObjectsAndKeys:[dict valueForKey:@"ADR"],@"ADR", nil]];
        }
//        else if([[abc objectAtIndex:0] isEqualToString:@"URL"])
//        {
//            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//            if([abc count] == 3)
//            {
//                getData=[[abc objectAtIndex:2] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
//                
//                if([getData count] > 1)
//                    [dict setObject:[getData objectAtIndex:1] forKey:@"Url"];
//            }
//            else
//            {
//                getData=[[abc objectAtIndex:1] componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@":"]];
//                
//                if([getData count] > 1)
//                    [dict setObject:[getData objectAtIndex:1] forKey:@"Url"];
//            }
//            //NSLog(@"%@",dict);
//           [arrayResult addObject:[NSDictionary dictionaryWithObjectsAndKeys:[dict valueForKey:@"Url"],@"Url", nil]];
//        }
        else
        {
            
        }
    }
    NSLog(@"%@",dic_Ad);
}
- (void)client:(Client *)sender didFailedWithError:(NSError *)error
{
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:[error localizedDescription]
												   delegate:nil 
										  cancelButtonTitle:@"Cancel" 
										  otherButtonTitles:nil, nil];
	
	[alert show];
	
	statusLabel.text = [error localizedDescription];
	statusIndicator.hidden = YES;
}

@end
