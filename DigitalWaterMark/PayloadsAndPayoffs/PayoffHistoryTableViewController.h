//
//  PayoffHistoryTableViewController.h
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/10/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayoffDoc.h"
#import "DMPayload.h"

@protocol PayoffHistoryTableDelegate <NSObject>

-(void)didSelectItemWithTitle:(NSString*)title actionToken:(NSString*)actionToken content:(NSString *)content;

@end

@interface PayoffHistoryTableViewController : UITableViewController

@property (retain, nonatomic) NSMutableArray *payoffsHistory;
@property (weak) id<PayoffHistoryTableDelegate> delegate;

-(void)newItemAvailable:(PayoffDoc *)item;

@end
