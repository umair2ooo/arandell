//
//  ExtrasViewController.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/26/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "HelpViewController.h"
#import "TutorialViewController.h"
#import "WebViewController.h"
//#import "DemoConfig.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.view setBackgroundColor:HELP_BACKGROUND_COLOUR];
    
    CGFloat buttonSize = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 80.0 : 160.0;
    CGFloat buttonTextSize = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 20 : 28;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [backButton setTitle:@"Done" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(removeFromScreen) forControlEvents:UIControlEventTouchUpInside];
    backButton.titleLabel.font = [UIFont systemFontOfSize:buttonTextSize];
    [self.view addSubview:backButton];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:buttonSize
                                 ],
                                [NSLayoutConstraint constraintWithItem:backButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:buttonSize
                                 ]
                                ]
     ];
    
    UILabel *initialTextLabel = [[UILabel alloc] init];
    initialTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
    initialTextLabel.textColor = [UIColor whiteColor];
    initialTextLabel.numberOfLines = CGFLOAT_MAX;
    initialTextLabel.text = HELP_INITIAL_TEXT;
    initialTextLabel.font = [UIFont italicSystemFontOfSize:[[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 14 : 24];
    [self.view addSubview:initialTextLabel];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:initialTextLabel
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:backButton
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1.0
                                                              constant:-20.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:initialTextLabel
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:initialTextLabel
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:-10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:initialTextLabel
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeHeight
                                                            multiplier:0.3
                                                              constant:0.0
                                 ]
                                ]
     ];
    
    UIButton *tutorialButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    tutorialButton.translatesAutoresizingMaskIntoConstraints = NO;
    [tutorialButton setTitle:TUTORIAL_TITLE forState:UIControlStateNormal];
    [tutorialButton addTarget:self action:@selector(showTutorial) forControlEvents:UIControlEventTouchUpInside];
    tutorialButton.titleLabel.font = [UIFont boldSystemFontOfSize:buttonTextSize];
    [self.view addSubview:tutorialButton];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:tutorialButton
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:initialTextLabel
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:tutorialButton
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:tutorialButton
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:-10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:tutorialButton
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:buttonSize*0.7
                                 ]
                                ]
     ];
    
    UIButton *eulaButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    eulaButton.translatesAutoresizingMaskIntoConstraints = NO;
    [eulaButton setTitle:EULA_TITLE forState:UIControlStateNormal];
    [eulaButton addTarget:self action:@selector(showEULA) forControlEvents:UIControlEventTouchUpInside];
    eulaButton.titleLabel.font = [UIFont boldSystemFontOfSize:buttonTextSize];
    [self.view addSubview:eulaButton];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:eulaButton
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:tutorialButton
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1.0
                                                              constant:0.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:eulaButton
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:eulaButton
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:-10.0
                                 ],
                                [NSLayoutConstraint constraintWithItem:eulaButton
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:buttonSize*0.7
                                 ]
                                ]
     ];
    
}

-(void)showTutorial {
    TutorialViewController *tutorialVC = [[TutorialViewController alloc] init];
    [self presentViewController:tutorialVC animated:NO completion:nil];
}

-(void)showEULA {
    [self displayURLInWebView:[NSURL URLWithString:EULA_URL_STRING]];
}

-(void) displayURLInWebView:(NSURL *)url {
    WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:[NSBundle mainBundle]];
    [webView setUrl:url];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
    [webView.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                                   style:UIBarButtonItemStyleDone
                                                                                  target:webView
                                                                                  action:@selector(removeFromScreen)
                                                   ]
     ];
    [self presentViewController:navC animated:YES completion:nil];
}

-(void)removeFromScreen {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
