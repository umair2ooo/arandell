#import "NewDirectionViewController.h"

#import "LocationDataClass.h"
//#import "MapView.h"
#import "Place.h"

#import "Cell_directionOnMap.h"
#import "GetProductDataFromServer.h"
#import "StoreLocationDataClass.h"
#import <MapKit/MapKit.h>
#import "ScannedHistoryViewController.h"
#import "Singleton.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "CustomSpinnerView.h"
#import "ActivityIndicator.h"

#define k_gap 5.0
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#define k_float(x) [NSNumber numberWithFloat:x]
#define D(...) [NSDictionary dictionaryWithObjectsAndKeys:__VA_ARGS__, nil]


@interface NewDirectionViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, GetDateFromServer,DeleteObject>
{
    CLLocationDistance minimumDistance;
    NSString *currentLat;
    NSString *currentLong;
    UILabel *label_counter;
    
    Singleton *single;
}
@property(nonatomic, weak)LocationDataClass *location_inMap;
@property(nonatomic, strong)StoreLocationDataClass *obj_StoreLocation;
@property(nonatomic, strong)NSArray *array_store;
// Circle D spinner (branding overlay shown on detecting (resolving) new payload ...
@property (retain)  MBProgressHUD *progressHUD;
@property (retain)  CustomSpinnerView *customSpinner;

@property(nonatomic, strong)UITableView *tableView_;


@property (strong, nonatomic) IBOutlet MKMapView *mapView;
//@property(nonatomic, strong)MapView* mapView;
@property (nonatomic, strong)CLLocation *location_current;
@property (nonatomic, strong)NSDictionary *dic_nearestStore;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UITextField *text_Miles;
@property (weak, nonatomic) IBOutlet UITextField *text_City;
@property (weak, nonatomic) IBOutlet UITextField *text_State;
@property (weak, nonatomic) IBOutlet UITextField *text_ZipCode;
@property (weak, nonatomic) IBOutlet UIView *view_textFields;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;
@property(nonatomic, strong)GetProductDataFromServer *obj_GetProductDataFromServer;

- (IBAction)action_activity:(id)sender;
- (IBAction)action_Search:(id)sender;
- (IBAction)action_exit_map:(UIStoryboardSegue *)segue;

-(IBAction)action_goBack:(id)sender;

@end

@implementation NewDirectionViewController


@synthesize mapView = _mapView;
@synthesize location_current = _location_current;
@synthesize locationManager = _locationManager;
@synthesize dic_nearestStore = _dic_nearestStore;
@synthesize array_stores = _array_stores;
@synthesize location_inMap = _location_inMap;
@synthesize array_store = _array_store;

#pragma mark - creating map and drawing path
-(void)method_createMapAndDrawPath
{
//    [self.mapView setFrame:CGRectMake(k_gap, 120, self.view.frame.size.width-(k_gap*2), self.view.frame.size.height/3*2 -150)];
    

    if (label_counter == nil)
    {
        label_counter = [[UILabel alloc] initWithFrame:CGRectMake(IsPhone? k_gap : k_gap+20,
                                                                  self.mapView.frame.origin.y+self.mapView.frame.size.height,
                                                                  200,
                                                                  30)];
        
        label_counter.center = self.view.center;
        
        [label_counter setFrame:CGRectMake(label_counter.frame.origin.x,
                                           self.mapView.frame.origin.y+self.mapView.frame.size.height,
                                           200,
                                           30)];
    }
    
    if (!IsPhone)
        [label_counter setFont:[UIFont fontWithName:@"Helvetica" size:20.0]];
    
    [label_counter setBackgroundColor:[UIColor clearColor]];
    [label_counter setTextColor:[UIColor blackColor]];
    label_counter.text = [NSString stringWithFormat:@"Total Locations: %lu", (unsigned long)[self.array_stores count]];
    [self.view addSubview:label_counter];




    if (self.tableView_ == nil)
    {
        self.tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(k_gap,
                                                                        label_counter.frame.origin.y + label_counter.frame.size.height+k_gap,
                                                                        self.view.frame.size.width-(k_gap*2),
                                                                        self.view.frame.size.height -
                                                                        (label_counter.frame .origin.y + label_counter.frame.size.height+60))
                                                       style:UITableViewStylePlain];
    }

    self.tableView_.delegate = self;
    self.tableView_.dataSource = self;
    [self.view addSubview:self.tableView_];
    
    
    UINib *nib = nil;
    
    if (IsPhone)
    {
        nib = [UINib nibWithNibName:@"Cell_directionOnMap" bundle:nil];
    }
    else
    {
        nib = [UINib nibWithNibName:@"Cell_directionOnMap_iPad" bundle:nil];
    }
    
    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_directionOnMap"];
    
    
    [self.tableView_ selectRowAtIndexPath:0
                                 animated:YES
                           scrollPosition:UITableViewScrollPositionMiddle];
    
    
    [self.tableView_ reloadData];
    [self DropPin];
  //  [self method_dropPins_destination:self.location_inMap];
}



-(void)method_dropPins_destination:(LocationDataClass *)locationDC
{
    Place* home = [[Place alloc] init];
    home.name = locationDC.location_storeName;
    //    home.description = @"Sweet home";
    home.latitude = locationDC.location_lat;
    home.longitude = locationDC.location_long;
    
    
    Place* office = [[Place alloc] init];
    office.name = @"Current Location";
    //    office.description = @"Bad office";
    office.latitude = self.location_current.coordinate.latitude;
    office.longitude = self.location_current.coordinate.longitude;
    
    
    //[self.mapView showRouteFrom:home to:office];
    
    home = nil;
    office = nil;
}

#pragma mark - current location
-(void)method_currentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        //for background and foreground
        //[self.locationManager requestAlwaysAuthorization]
        //foreground
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    self.mapView.showsUserLocation = YES;
}


#pragma mark - find nearest store
-(void)method_findNearestStore_destinationDic:(LocationDataClass *)locaitonDC
{
    CLLocation *location_destination = [[CLLocation alloc] initWithLatitude:locaitonDC.location_lat
                                                                  longitude:locaitonDC.location_long];
    
    
    CLLocationDistance distance = [self.location_current distanceFromLocation:location_destination];
    
    if (minimumDistance)
    {
        if (distance < minimumDistance)
        {
            minimumDistance = distance;
            self.dic_nearestStore = nil;
            self.location_inMap = locaitonDC;
        }
    }
    else
    {
        minimumDistance = distance;
        self.location_inMap = locaitonDC;
    }
    
    location_destination = nil;
    
    NSLog(@"store distance: %f", distance/1000);                // in KM
}



#pragma mark - action_goBack
-(IBAction)action_goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    single = [Singleton retriveSingleton];
    
    [self.view_textFields setHidden:YES];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@k_backButtonImage]
//                                                                             style:UIBarButtonItemStyleDone
//                                                                            target:self
//                                                                            action:@selector(action_goBack)];
    self.navigationItem.hidesBackButton = YES;
    
    [self.array_stores enumerateObjectsUsingBlock:^(LocationDataClass *obj, NSUInteger idx, BOOL *stop)
     {
         DLog(@"%@", obj.location_storeName);
         DLog(@"%f", obj.location_lat);
         DLog(@"%f", obj.location_long);
     }];
    
    [self method_addPickerInKeyBoard];
    if (![CLLocationManager locationServicesEnabled])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Turn ON Location Service to Allow FavPlate to Determine Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self method_currentLocation];
    }
    
   // self.mapView.userInteractionEnabled = NO;
    
   // [self method_createMapAndDrawPath];
    
//[self performSelector:@selector(method_createMapAndDrawPath) withObject:nil afterDelay:0.1];
    
    
    
//    DLog(@"%@",[self.obj_GetProductDataFromServer method_fetchLocation:@"product_id" City:@"markham" State:@"state" Miles:@"100"]);
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(method_tabBar:)
                                                 name:k_notificationForTabBar
                                               object:nil];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    self.tableView_.delegate = self;
    self.tableView_.dataSource = self;
    self.tableView_ = nil;
    
    self.mapView = nil;
    
    self.array_stores = nil;

    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.locationManager = nil;
    self.location_current = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - add picker in text keyBoard
-(void)method_addPickerInKeyBoard
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel"
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(action_done)], nil];
    
    [numberToolbar sizeToFit];
    self.text_ZipCode.inputAccessoryView = numberToolbar;
    self.text_Miles.inputAccessoryView = numberToolbar;
    numberToolbar = nil;
}


#pragma mark - bar button
-(void)action_done
{
    if ([self.text_ZipCode isEditing])
    {
        [self.view_textFields setHidden:YES];
        [self.text_ZipCode resignFirstResponder];
        self.text_ZipCode.text = @"";
    }
    else
        [self.text_Miles resignFirstResponder];
}



#pragma mark - table view delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_stores count]?[self.array_stores count]:0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IsPhone)
    {
        return 110;
    }
    else
    {
        return 125;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_directionOnMap *cell = (Cell_directionOnMap *)[tableView dequeueReusableCellWithIdentifier:@"Cell_directionOnMap"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_directionOnMap" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.delegate = self;
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
   // self.location_inMap = [self.array_stores objectAtIndex:indexPath.row];
    self.obj_StoreLocation = [self.array_stores objectAtIndex:indexPath.row];
    cell.label_storeName.text = self.obj_StoreLocation.store_name;
    cell.label_storeDesc.text = self.obj_StoreLocation.store_address;
    //cell.lab.text = self.obj_StoreLocation.store_address;
    cell.button_Direction.tag = indexPath.row;
    cell.label_storeDistance.text = [NSString stringWithFormat:@"%.0f mi",[self method_calculateDistance:self.obj_StoreLocation.store_latitude lon:self.obj_StoreLocation.store_longitude]];
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    self.location_inMap = [self.array_stores objectAtIndex:indexPath.row];
//    
//    [tableView selectRowAtIndexPath:indexPath
//                           animated:YES
//                     scrollPosition:UITableViewScrollPositionMiddle];
    
  //  [self method_dropPins_destination:self.location_inMap];
    
    
    
    
    
    
    
    
//    if ([(LocationDataClass *)[self.array_stores objectAtIndex:indexPath.row] location_long] != self.location_inMap.location_long)
//    {
//        self.location_inMap = [self.array_stores objectAtIndex:indexPath.row];
//        
//        [tableView selectRowAtIndexPath:indexPath
//                               animated:YES
//                         scrollPosition:UITableViewScrollPositionMiddle];
//        
//        [self method_dropPins_destination:self.location_inMap];
//    }
}


#pragma mark - didUpdateToLocation
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    CLLocation *oldLocation;
    if (locations.count > 1)
    {
        oldLocation = [locations objectAtIndex:locations.count-2];
    }
    else
    {
        oldLocation = nil;
    }
    
    
    
    if (!self.location_current)
    {
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        
        
        self.location_current = newLocation;
        
        currentLat = [NSString stringWithFormat:@"%.8f", self.location_current.coordinate.latitude];
        currentLong = [NSString stringWithFormat:@"%.8f", self.location_current.coordinate.longitude];
//        for (unsigned i =0; i<[self.array_stores count]; i++)
//        {
//            [self method_findNearestStore_destinationDic:[self.array_stores objectAtIndex:i]];
//        }
        
        //[self method_createMapAndDrawPath];
    }
    
    newLocation = nil;
    oldLocation = nil;
}



- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (!self.location_current)
    {
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        self.location_current = newLocation;
        
//        for (unsigned i =0; i<[self.array_stores count]; i++)
//        {
//            [self method_findNearestStore_destinationDic:[self.array_stores objectAtIndex:i]];
//        }
        
       // [self method_createMapAndDrawPath];
    }
}


#pragma mark - textfield Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField != self.text_ZipCode)
        [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if (textField == self.text_ZipCode)
    {
        [self.view_textFields setHidden:NO];
    }
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField == self.text_ZipCode)
    {
    }
    return YES;
}
#pragma mark - action_Search
- (IBAction)action_Search:(id)sender
{
    //markham
    
    [self.view endEditing:YES];
    if ([self.text_ZipCode.text length])
    {
       // [self showSpinner];
        [[ActivityIndicator currentIndicator] show];
        [self.view_textFields setHidden:YES];
        AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
        requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
        requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        DLog(@"%@",self.text_ZipCode.text);
        NSString *strUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@",self.text_ZipCode.text];
        strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSError *error = nil;
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
             
             if (error != nil)
             {
                 NSLog(@"json: %@", json);
//                 [self removeSpinner];
                 [[ActivityIndicator currentIndicator] hide];
             }
             else
             {
                 NSLog(@"Array: %@", json);
                 if ([json valueForKey:@k_Results])
                 {
                     if ([[json valueForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
                     {
                         [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                     message:@"Invalid zip code."
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles:nil, nil]
                          show];
                         [self method_failureZipCodeResponse];
                         //[self removeSpinner];
                          [[ActivityIndicator currentIndicator] hide];
                     }
                     else
                     {
                         if ([[json valueForKey:@k_Results] count] == 1)
                         {
                             if ([[[json valueForKey:@k_Results] objectAtIndex:0] valueForKey:@"geometry"])
                             {
                                 self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
                                 self.obj_GetProductDataFromServer.delegate = self;
                                 [self.obj_GetProductDataFromServer method_fetchLocationByZipcode_Latitude:[[[[[json valueForKey:@k_Results] objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"] Longitude:[[[[[json valueForKey:@k_Results] objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"]];
                             }
                             else
                             {
                                 //[self removeSpinner];
                                 [[ActivityIndicator currentIndicator] hide];
                                 [self method_failureZipCodeResponse];
                                 [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                             message:@"Invalid zip code."
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil, nil]
                                  show];
                                 
                             }
                         }
                         else
                         {
                             //[self removeSpinner];
                              [[ActivityIndicator currentIndicator] hide];
                             [self method_failureZipCodeResponse];
                             [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                         message:@"Invalid zip code."
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil, nil]
                              show];
                             
                         }
                     }
                 }
                 else
                 {
                     [self method_failureZipCodeResponse];
                     [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                 message:@"Invalid zip code."
                                                delegate:nil
                                       cancelButtonTitle:@"Ok"
                                       otherButtonTitles:nil, nil]
                      show];
                      [[ActivityIndicator currentIndicator] hide];
                     //[self removeSpinner];
                 }
             }
         }
                    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                       // [self removeSpinner];
                         [[ActivityIndicator currentIndicator] hide];
                        DLog(@"%@", error);
                    }];
        
    }
    else
    {
        //if we pass city empty then also web service returns stores
        self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
        self.obj_GetProductDataFromServer.delegate = self;
        [[ActivityIndicator currentIndicator] show];
        [self.obj_GetProductDataFromServer method_fetchLocation:@"product_id" City:self.text_City.text State:self.text_State.text Miles:self.text_Miles.text];
    }
}

#pragma mark - webservice response
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    // [self removeSpinner];
    [[ActivityIndicator currentIndicator] hide];
    
    if ([serviceName isEqualToString:k_webServiceGetMainProduct])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        self.array_stores = [[NSArray alloc] initWithArray:array_];
        NSLog(@"%@",self.array_stores);
        [self method_createMapAndDrawPath ];
    }
}

-(void)method_failureResponse
{
    self.array_stores = nil;
    [self method_createMapAndDrawPath ];
    [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"Data not found." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    [self.mapView removeAnnotations:[self.mapView annotations]];
    //[self removeSpinner];
    [[ActivityIndicator currentIndicator] hide];
}


#pragma mark - failureGoogleApiResponse
-(void)method_failureZipCodeResponse
{
    self.array_stores = nil;
    [self method_createMapAndDrawPath ];
}


#pragma mark -- DropPins
-(void)DropPin
{
    MKMapRect zoomRect = MKMapRectNull;
    for (int i = 0; i<[self.array_stores count]; i++)
    {
        self.obj_StoreLocation = [self.array_stores objectAtIndex:i];
        MKPointAnnotation *mapAnnotation = [[MKPointAnnotation alloc] init];
        mapAnnotation.title = self.obj_StoreLocation.store_name;
       // mapAnnotation.subtitle = [[restaurants objectAtIndex:i] valueForKey:@"address"];
        CLLocationCoordinate2D cordinate;
        cordinate.latitude = [self.obj_StoreLocation.store_latitude floatValue];
        cordinate.longitude = [self.obj_StoreLocation.store_longitude floatValue];
        mapAnnotation.coordinate = cordinate;
        [self.mapView addAnnotation:mapAnnotation];
        MKMapPoint annotationPoint = MKMapPointForCoordinate(mapAnnotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    if ([self.array_stores count])
    {
       [self.mapView setVisibleMapRect:zoomRect animated:YES];
    }
}
#pragma mark - method_direction
-(void)method_direction:(int)tag
{
    DLog(@"%d", tag);
    self.obj_StoreLocation = [self.array_stores objectAtIndex:tag];
    NSString* url = [NSString stringWithFormat: @"http://%@/maps?saddr=%@,%@&daddr=%@,%@",@"maps.apple.com",currentLat,currentLong,self.obj_StoreLocation.store_latitude,self.obj_StoreLocation.store_longitude];
    DLog(@"url:%@", url);
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}


#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForMap";
    
    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}


#pragma mark - action_exit_cart
- (IBAction)action_exit_map:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForMap"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;
        
        DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil]);
        
        single.string_productID = [NSString stringWithFormat:@"%@", obj.payOff_item.title];
        
//        [self.navigationController popViewControllerAnimated:YES];
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                       selector:@selector(method_popViewController)
                                       userInfo:nil
                                        repeats:NO];
        
//        GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
//        obj_getProductData.delegate = self;
//        [obj_getProductData method_makeIDFromPayLoad:obj.payOff_item.title];
//        
//        obj_getProductData = nil;
//        obj = nil;
    }
    else
    {
        DLog(@"Done only");
    }
}


-(void)method_popViewController
{
    [self.navigationController popViewControllerAnimated:NO];
}



#pragma mark - method_calculateDistance
-(float)method_calculateDistance:(NSString *)lat lon:(NSString *)lon
{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[currentLat floatValue] longitude:[currentLong floatValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[lat floatValue] longitude:[lon floatValue]];
    // now calculating the distance using inbuild method distanceFromLocation:
    float distInMeter = [location1 distanceFromLocation:location2]; // which returns in meters
    //converting meters to mile
    float distInMile = 0.000621371192 * distInMeter;
     NSLog(@"Actual Distance : %0f",distInMile);
    return distInMile;
}

#pragma mark - method_validateZipcode
-(BOOL)method_validateZipcode
{
    NSString *postcodeRegex = @"^(\\d{5}(-\\d{4})?|[a-z]\\d[a-z][- ]*\\d[a-z]\\d)$";//for canada and US only
    NSPredicate *postcodeValidate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", postcodeRegex];
    
    if ([postcodeValidate evaluateWithObject:self.text_ZipCode.text] == YES)
    {
        NSLog (@"Postcode is Valid");
        return YES;
    } else {
        NSLog (@"Postcode is Invalid");
        return NO;
    }
}
#pragma mark - showSpinner
-(void)showSpinner
{
    self.progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    self.progressHUD.center = self.view.center;//self.crosshairs.center;
    self.customSpinner = [[CustomSpinnerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.progressHUD setCustomView:self.customSpinner];
    [self.progressHUD setMode:MBProgressHUDModeCustomView];
    [self.view.window addSubview:self.progressHUD];
    [self.progressHUD show:YES];
    
    //    NSTimer *t = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(removeSpinner) userInfo:0 repeats:NO];
    //    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
}
#pragma mark - removeSpinner
-(void)removeSpinner {
    self.progressHUD.removeFromSuperViewOnHide = YES;
    [self.progressHUD hide:YES];
    [self.customSpinner stopAnimation];
    self.progressHUD = nil;
    self.customSpinner = nil;
}


#pragma mark - NSNotification method
-(void)method_tabBar:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:k_notificationForTabBar])
        [self.navigationController popToRootViewControllerAnimated:NO];
}
@end