#import "ProductDetailViewController.h"

#import "RXCustomTabBar.h"

#import "ProductsDataClass.h"
#import "LocationDataClass.h"
#import "AsyncImageView.h"
#import "NewDirectionViewController.h"
#include "ReviewsViewController.h"
#import "ItemAvailabilityViewController.h"
#import "ScannedHistoryViewController.h"
#import "SubmitFormViewController.h"
#import "ItemAvailabilityViewController.h"
#import "Singleton.h"
#import "AFNetworking.h"
#import "ColorsDataClass.h"
#import "GetProductDataFromServer.h"


@interface ProductDetailViewController ()<GetDateFromServer>
{
    Singleton *single;
}

- (IBAction)action_goToScan:(id)sender;
- (IBAction)done:(UIStoryboardSegue *)segue;
-(IBAction)action_goBack:(id)sender;

//@property(nonatomic, strong)ProductsDataClass *productsDataClass_obj;
@property(nonatomic, strong)GetProductDataFromServer *obj_GetProductDataFromServer;

@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_P_Image;
@property (weak, nonatomic) IBOutlet UILabel *label_productName;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;

- (IBAction)action_logOut:(id)sender;
- (IBAction)action_menu:(id)sender;


@end

@implementation ProductDetailViewController

@synthesize dic_dataFromPrevControler = _dic_dataFromPrevControler;
//@synthesize productsDataClass_obj = _productsDataClass_obj;
@synthesize obj_GetProductDataFromServer = _obj_GetProductDataFromServer;


#pragma mark - det data from server delegate
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    [self method_setValuesOnXIB];
}

//-(void)method_data
//{
////    self.productsDataClass_obj = single.productDetail;
////    [self method_setValuesOnXIB];
//}

#pragma mark - action_goBack
-(IBAction)action_goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - get product detail from Server
-(void)method_getProductDetailFromServer
{
    self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
    self.obj_GetProductDataFromServer.delegate = self;
    //    [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:[self.dic_dataFromPrevControler valueForKey:k_title]];
    
    [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:single.string_productID];
}

#pragma mark - cycle
-(void)viewWillAppear:(BOOL)animated
{
//    [super viewWillAppear:animated];
    
    if (single.isLogOut)
    {
        single.isLogOut = false;
        
        RXCustomTabBar *tabBar_ = (RXCustomTabBar *)self.tabBarController;
        [tabBar_ dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        self.navigationItem.hidesBackButton = YES;
        
        
        //    if (![self.label_productName.text isEqualToString:single.productDetail.p_name])
        //    {
        //        self.label_productName.text = @"";
        //        self.imageView_P_Image.image = nil;
        //    }
        
        [self method_getProductDetailFromServer];
        
        [self method_setValuesOnXIB];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(method_tabBar:)
                                                     name:k_notificationForTabBar
                                                   object:nil];
        
        //    DLog(@"Alhamdulillah: %@", self.dic_dataFromPrevControler);
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationItem.hidesBackButton = YES;
    
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@k_backButtonImage]
//                                                                             style:UIBarButtonItemStyleDone
//                                                                            target:self
//                                                                            action:@selector(action_goBack)];
    
    
    
    
    
    single = [Singleton retriveSingleton];
    
    if (single.array_sizesImages || single.array_colorsImages)
    {
        [single.array_sizesImages removeAllObjects];
        [single.array_colorsImages removeAllObjects];
    }
    
//    [self method_makeIDFromPayLoad];
    
    
//    DLog(@"%@", [self.dic_dataFromPrevControler valueForKey:@"title"]);
    
//    self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
//    self.obj_GetProductDataFromServer.delegate = self;
////    [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:[self.dic_dataFromPrevControler valueForKey:k_title]];
//    
//    [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:single.string_productID];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)viewDidDisappear:(BOOL)animated
{
//    //     All instances of TestClass will be notified
//    [[NSNotificationCenter defaultCenter] postNotificationName:@k_notification
//                                                        object:self
//                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:self.productsDataClass_obj.p_imageURL_AR, @k_ARImage, nil]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    single.string_imageURL = single.productDetail.p_imageURL_AR;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//#pragma mark - method_makeIDFromPayLoad
//-(void)method_makeIDFromPayLoad
//{
//    DLog(@"id: %@", [self.dic_dataFromPrevControler valueForKey:k_title]);
//    [self method_fetchData:[self.dic_dataFromPrevControler valueForKey:k_title]];
//}
//
//
//#pragma mark - method_fetchData
//- (void)method_fetchData:(NSString *)id_
//{
//        //New method
//        AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
//        requestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", nil];
//        requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
//        NSString *strUrl = [NSString stringWithFormat:@"http://192.168.16.38/wcs/resources/store/10001/productview/byId/10002"];//14451
////    http://119.63.131.227/webapp/wcs/stores/servlet/en/aurora
////    http://192.168.16.38/webapp/wcs/stores/servlet/en/aurora
//    
//    
//        strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [requestManager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
//         {
//             NSError *error = nil;
//             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
//    
//             if (error != nil)
//             {
//                 NSLog(@"json: %@", json);
//             }
//             else
//             {
//                 NSLog(@"Array: %@", json);
//                 if (single.array_productDetail)
//                 {
//                     [single.array_productDetail removeAllObjects];
//                     single.array_productDetail = nil;
//                 }
//                 single.array_productDetail = [[NSMutableArray alloc] init];
//                 
//                 
//                 /////////////////////   colors and sizes
//
//                 NSMutableArray *array_colors = [[NSMutableArray alloc] init];
//                 
//                 if ([[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_Attributes])
//                 {
//                     if ([[[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_Attributes] count])
//                     {
//                         
//                         [(NSArray *)[[[[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_Attributes] objectAtIndex:0] valueForKey:@k_Values] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                             
//                             [array_colors addObject:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                      [obj valueForKey:@k_uniqueID], @"uniqueID",
//                                                      [obj valueForKey:@k_values], @"values", nil]];
//                         }];
//                     }
//                 }
//                 
//                 
////                 DLog(@"%@", array_colors);
//                 
//                 
//                 
//                 NSMutableArray *array_tempColor = [[NSMutableArray alloc] init];
//                 
//                 if ([[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_SKUs])
//                 {
//                     if ([[[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_SKUs] count])
//                     {
//                         [array_colors enumerateObjectsUsingBlock:^(id obj_, NSUInteger idx, BOOL *stop) {
//                             
//                             __block int indexOfColor = 0;
//                             
//                             
//                             
//                             [(NSArray *)[[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_SKUs] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                                 
//                                 
//                                 indexOfColor = idx;
//                                 
//                            
//                                 if ([[[[[[obj valueForKey:@k_Attributes] objectAtIndex:1] valueForKey:@k_Values] objectAtIndex:0] valueForKey:@k_uniqueID] isEqualToString:[obj_ valueForKey:@k_uniqueID]])
//                                 {
//                                     [array_tempColor addObject:[[[[[obj valueForKey:@k_Attributes] objectAtIndex:0] valueForKey:@k_Values] objectAtIndex:0] valueForKey:@k_values]];
//                                     
//                                     //*stop = YES;    // Stop enumerating
//                                 }
//                                 
////                                 DLog(@"%@", array_tempColor);
//                             }];
//                             
//                             if (indexOfColor == [[[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@k_SKUs] count]-1)
//                             {
//                                 
//                                 NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[array_colors objectAtIndex:[array_colors indexOfObject:obj_]]];
//                                 [dic_temp setObject:[NSArray arrayWithArray:array_tempColor] forKey:@"sizes"];
//                                 
//                                 
//                                 [array_colors replaceObjectAtIndex:[array_colors indexOfObject:obj_] withObject:[NSDictionary dictionaryWithDictionary:dic_temp]];
//                                 
////                                 DLog(@"%@", array_colors);
//                                 
//                                 dic_temp = nil;
//                                 
//                                 [array_tempColor removeAllObjects];
//                                 indexOfColor = 0;
//                                 
////                                 DLog(@"%@", array_colors);
//                             }
//                         }];
//                     }
//                 }
//                 
//                 
//                 
//                 [array_colors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
//                     
//                     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"path CONTAINS[cd] %@", [obj valueForKey:@k_values]];
//                     
//                     NSArray *aNames = [[[[json valueForKey:@k_data] objectAtIndex:0] valueForKey:@"Attachments"] filteredArrayUsingPredicate:predicate];
//                     
//                     
//                     NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[array_colors objectAtIndex:[array_colors indexOfObject:obj]]];
//                     [dic_temp setObject:[[aNames objectAtIndex:0] valueForKey:@"path"] forKey:@"colorURL"];
//                     
//                     [array_colors replaceObjectAtIndex:[array_colors indexOfObject:obj] withObject:dic_temp];
//                     
//                     dic_temp = nil;
//                 }];
//                 
//                 
////                 DLog(@"%@", array_colors);
//                 
//                 
//                 /////////////////////  colors and sizes
//                 
//                 ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];
//                 
////                 [obj_ setP_colorURL_array:array_temp];
//                 
//                 [obj_ setP_array_colors:array_colors];
//                 
//                 
//                 DLog(@"%@", obj_.p_array_colors);
//                 
////                 [obj_ setP_size_array:array_temp:array_temp];
//                 
//                 
//                 
//                 [obj_ setP_id:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:k_id]];
//                 [obj_ setP_name:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_productName]];
//                 [obj_ setP_price:[[[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@"Price"] objectAtIndex:0] valueForKey:@k_price]];
//                 [obj_ setP_description:[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_productDesc]];
//                 NSString *imageUrl = [NSString stringWithFormat:@"%@%@",@k_ImageBaseUrlLocal,[[[json valueForKey:@"CatalogEntryView"] objectAtIndex:0] valueForKey:@k_detailImage]];
//                 [obj_ setP_imageURL:[NSURL URLWithString:imageUrl]];
//                 [obj_ setP_imageURL_AR:[NSURL URLWithString:imageUrl]];
////                 [obj_ setArray_location:(NSMutableArray *)[obj valueForKey:@k_locationInfo]];
//                 
//                 
//                 [single.array_productDetail addObject:obj_];
//                 
//                 
////                 DLog(@"p_colorURL: %@", obj_.p_colorURL_array);
////                 DLog(@"p_size: %@", obj_.p_size_array);
////                 DLog(@"p_totalQuantity: %d", obj_.p_totalQuantity);
////                 DLog(@"p_reviews: %@", obj_.p_reviews);
////                 DLog(@"p_colorName: %@", obj_.p_colorName);
//                 
//                 DLog(@"%@", obj_.p_id);
//                 DLog(@"%@", obj_.p_name);
//                 DLog(@"%@", obj_.p_price);
//                 DLog(@"%@", obj_.p_imageURL);
//                 DLog(@"%@", obj_.p_imageURL_AR);
//                 
//                 obj_ = nil;
//
//                 
//                 if ([single.array_productDetail count])
//                 {
//                     self.productsDataClass_obj = (ProductsDataClass *)[single.array_productDetail objectAtIndex:0];
//                     [self method_setValuesOnXIB:(ProductsDataClass *)[single.array_productDetail objectAtIndex:0]];
//                 }
//                 else
//                 {
//                     [[[UIAlertView alloc] initWithTitle:nil
//                                                 message:@"Product not matched"
//                                                delegate:nil
//                                       cancelButtonTitle:@"Ok"
//                                       otherButtonTitles:nil, nil]
//                      show];
//                 }
//             }
//         }
//                    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                        
//                    }];
//        
//
//    
//    
//    
//    
//    // old method
//    
////    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ARC_JSON-last - FinalRelease" ofType:@"json"];
////    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
////    NSError *error =  nil;
////    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
////    
////    DLog(@"%@", jsonData);
////    
////    
////    if ([jsonData isKindOfClass:[NSNull class]] || jsonData == nil)
////    {
////        [[[UIAlertView alloc] initWithTitle:@"Sorry"
////                                    message:@"There is something is wrong from server side, please try later"
////                                   delegate:nil
////                          cancelButtonTitle:@"Ok"
////                          otherButtonTitles:nil, nil]
////         show];
////    }
////    else
////    {
////        if (array_products)
////        {
////            [array_products removeAllObjects];
////            array_products = nil;
////        }
////        
////        array_products = [[NSMutableArray alloc] init];
////        
////        
////        [(NSArray *)[jsonData valueForKey:@k_data] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
////         {
////             if ([id_ isEqualToString:[obj valueForKey:k_id]])                  // matching the scanned product
////             {
////                 ProductsDataClass *obj_ = [[ProductsDataClass alloc] init];
////                 
////                 [obj_ setP_id:[obj valueForKey:k_id]];
////                 [obj_ setP_name:[obj valueForKey:@k_productName]];
////                 [obj_ setP_rating:[[obj valueForKey:@k_prodRating] intValue]];
////                 [obj_ setP_price:[obj valueForKey:@k_price]];
////                 [obj_ setP_sale:[obj valueForKey:@k_sale]];
////                 [obj_ setP_inStock:[obj valueForKey:@k_inStock]];
////                 [obj_ setP_model:[obj valueForKey:@k_modelNo]];
////                 [obj_ setP_skuNumber:[obj valueForKey:@k_skuNo]];
////                 [obj_ setP_description:[obj valueForKey:@k_productDesc]];
////                 [obj_ setP_imageURL:[NSURL URLWithString:[obj valueForKey:@k_detailImage]]];
////                 [obj_ setP_imageURL_AR:[NSURL URLWithString:[obj valueForKey:@k_ARImage]]];
////                 [obj_ setArray_location:(NSMutableArray *)[obj valueForKey:@k_locationInfo]];
////                 
////                 
////                 NSMutableArray *array_temp = [[NSMutableArray alloc] init];
////                 
////                 [[obj valueForKey:@k_color] enumerateObjectsUsingBlock:^(id obj__, NSUInteger idx, BOOL *stop)
////                 {
////                     if (obj__)
////                     {
////                         [array_temp addObject:obj__];
////                     }
////                     
////                 }];
////                 [obj_ setP_colorURL_array:array_temp];
////                 
////                 
////                 array_temp = nil;
////                 array_temp = [[NSMutableArray alloc] init];
////                 
////                 
////                 
////                 [[obj valueForKey:@k_size] enumerateObjectsUsingBlock:^(id obj__, NSUInteger idx, BOOL *stop)
////                  {
////                      if (obj__)
////                      {
////                          [array_temp addObject:obj__];
////                      }
////                      
////                  }];
////                 [obj_ setP_size_array:array_temp];
////                 
////                 array_temp = nil;
////                 
////                 
////                 
////                 
////                 [obj_ setP_totalQuantity:[[obj valueForKey:@k_totalQuantity] intValue]];
////                 [obj_ setP_reviews:[obj valueForKey:@k_reviews]];
////                 [obj_ setP_colorName:[obj valueForKey:@k_colorName]];
////                 
////                 
////                 [array_products addObject:obj_];
////                 
////                 
////                 DLog(@"p_colorURL: %@", obj_.p_colorURL_array);
////                 DLog(@"p_size: %@", obj_.p_size_array);
////                 DLog(@"p_totalQuantity: %d", obj_.p_totalQuantity);
////                 DLog(@"p_reviews: %@", obj_.p_reviews);
////                 DLog(@"p_colorName: %@", obj_.p_colorName);
////                 
////                 DLog(@"%@", obj_.p_id);
////                 DLog(@"%@", obj_.p_name);
////                 DLog(@"%d", obj_.p_rating);
////                 DLog(@"%@", obj_.p_price);
////                 DLog(@"%@", obj_.p_sale);
////                 DLog(@"%@", obj_.p_inStock);
////                 DLog(@"%@", obj_.p_model);
////                 DLog(@"%@", obj_.p_skuNumber);
////                 DLog(@"%@", obj_.p_imageURL);
////                 DLog(@"%@", obj_.p_imageURL_AR);
////                 DLog(@"%@", obj_.array_location);
////                 
////                 obj_ = nil;
////             }
////         }];
////        
////        if ([array_products count])
////        {
////            self.productsDataClass_obj = (ProductsDataClass *)[array_products objectAtIndex:0];
////            [self method_setValuesOnXIB:(ProductsDataClass *)[array_products objectAtIndex:0]];
////        }
////        else
////        {
////            [[[UIAlertView alloc] initWithTitle:nil
////                                        message:@"Product not matched"
////                                       delegate:nil
////                              cancelButtonTitle:@"Ok"
////                              otherButtonTitles:nil, nil]
////             show];
////        }
////    }
//}

#pragma mark - method_setValuesOnXIB
-(void)method_setValuesOnXIB
{
    //set placeholder image or cell won't update when image is loaded
    self.imageView_P_Image.image = [UIImage imageNamed:@"Placeholder.png"];
    
    //load the image
//    self.productsDataClass_obj = single.productDetail;
    
    [self.imageView_P_Image loadImageFromURL:[single.productDetail.p_imageURL absoluteString]];
    
    self.label_price.text = single.productDetail.p_price;
    
    self.label_productName.text = single.productDetail.p_name;
    
    
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                                      setting logos here of each Product
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    if ([single.string_productID isEqualToString:@"18074"] ||
//        [single.string_productID isEqualToString:@"18075"])
//    {
////        Salyers Fabulous Furs
//        self.imageView_companyLogo.image = [UIImage imageNamed:@"Salyers_Fabulous_Furs"];
//    }
//    else if ([single.string_productID isEqualToString:@"18076"] ||
//             [single.string_productID isEqualToString:@"18077"])
//    {
////        Graphik Dimensions
//        self.imageView_companyLogo.image = [UIImage imageNamed:@"Graphik_Dimensions"];
//    }
//    else if ([single.string_productID isEqualToString:@"18078"] ||
//             [single.string_productID isEqualToString:@"18079"])
//    {
////        Nautilus
//        self.imageView_companyLogo.image = [UIImage imageNamed:@"Nautilus"];
//    }
//    else if ([single.string_productID isEqualToString:@"18098"] ||
//             [single.string_productID isEqualToString:@"18080"] ||
//             [single.string_productID isEqualToString:@"18099"] ||
//             [single.string_productID isEqualToString:@"18081"] ||
//             [single.string_productID isEqualToString:@"18082"])
//    {
////        The Container Store
//        self.imageView_companyLogo.image = [UIImage imageNamed:@"The_Container_Store"];
//    }
//    else if ([single.string_productID isEqualToString:@"18083"] ||
//             [single.string_productID isEqualToString:@"18084"])
//    {
//        //        Beretta
//        self.imageView_companyLogo.image = [UIImage imageNamed:@"Beretta"];
//    }
//    else if ([single.string_productID isEqualToString:@"18086"] ||
//             [single.string_productID isEqualToString:@"18087"] ||
//             [single.string_productID isEqualToString:@"18088"] ||
//             [single.string_productID isEqualToString:@"18089"] ||
//             [single.string_productID isEqualToString:@"18090"])
//    {
//        //        Country Casual
//        self.imageView_companyLogo.image = [UIImage imageNamed:@"Country_Casual"];
//    }
//    else
//    {
//        self.imageView_companyLogo.image = nil;
//    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - prepare for segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_mapDirection"])
    {
        NewDirectionViewController *obj = (NewDirectionViewController *)segue.destinationViewController;
        
//        self.productsDataClass_obj = single.productDetail;
        obj.array_stores = single.productDetail.array_location;
        return;
    }
    
    if ([segue.identifier isEqualToString:@"segue_reviews"])
    {
//        ReviewsViewController *obj = (ReviewsViewController *)segue.destinationViewController;
        
//        self.productsDataClass_obj = single.productDetail;
//        obj.productsDataClass_obj = single.productDetail;
        return;
    }
    
    if ([segue.identifier isEqualToString:@"segue_submitForm"])
    {
//        SubmitFormViewController *obj = (SubmitFormViewController *)segue.destinationViewController;
        
//        self.productsDataClass_obj = single.productDetail;
//        obj.productsDataClass_obj = single.productDetail;
        return;
    }
    
    if ([segue.identifier isEqualToString:@"segue_itemAvailability"])
    {
//        ItemAvailabilityViewController *obj = (ItemAvailabilityViewController *)segue.destinationViewController;
        
//        self.productsDataClass_obj = single.productDetail;
//        obj.productsDataClass_obj = single.productDetail;
        return;
    }
    
    if ([segue.identifier isEqualToString:@"segue_activityHistory"] || [segue.identifier isEqualToString:@"segue_activityHistoryTwo"])
    {
        single.string_exitSegueIdentifier = @"segue_selectedProductForProductDetail";
        return;
    }
}


#pragma mark - action_goToScan
- (IBAction)action_goToScan:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - action_exitFrom Scanned History
- (IBAction)done:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForProductDetail"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;
        
        single.string_productID = [NSString stringWithFormat:@"%@", obj.payOff_item.title];
        [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:single.string_productID];
        
//        [self.dic_dataFromPrevControler setObject:[NSString stringWithFormat:@"%@", obj.payOff_item.title] forKey:k_title];
//        [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:[self.dic_dataFromPrevControler valueForKey:k_title]];
    }
    else
    {
        DLog(@"Done only");
    }
}

#pragma mark - NSNotification method
-(void)method_tabBar:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:k_notificationForTabBar])
        [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - action_logOut
- (IBAction)action_logOut:(id)sender
{
    RXCustomTabBar *tabBar_ = (RXCustomTabBar *)self.tabBarController;
    [tabBar_ dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - action_menu
- (IBAction)action_menu:(id)sender
{
}


@end