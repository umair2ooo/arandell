#import <Foundation/Foundation.h>

@interface ColorsDataClass : NSObject

@property (nonatomic, strong)NSString *color_url;
@property (nonatomic, strong)NSString *color_uniqueID;
@property (nonatomic, strong)NSString *color_name;
@property(nonatomic, strong)NSMutableArray *color_array_sizes;

@end