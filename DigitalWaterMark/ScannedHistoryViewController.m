#import "ScannedHistoryViewController.h"

#import "Cell_ScannedHistory.h"
#import "Singleton.h"
#import "SubmitFormViewController.h"

#define kPayoffHistoryDepth         30
#define kPayoffTimeoutInSeconds 259200


@interface ScannedHistoryViewController ()<UITableViewDataSource, UITableViewDelegate, BuyNow>
{
    Singleton *single;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView_;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;

- (IBAction)action_done:(id)sender;

@end

@implementation ScannedHistoryViewController


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    single = [Singleton retriveSingleton];
    
    
    UINib *nib = nil;
    
    if (IsPhone)
    {
        nib = [UINib nibWithNibName:@"Cell_ScannedHistory" bundle:nil];
    }
    else
    {
        nib = [UINib nibWithNibName:@"Cell_ScannedHistory_iPad" bundle:nil];
    }
    
    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_ScannedHistory"];
    
    
    
    
//    UINib *nib = [UINib nibWithNibName:@"Cell_ScannedHistory" bundle:nil];
//    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_ScannedHistory"];
    
    self.payoffsHistory = [[NSMutableArray alloc] initWithCapacity:kPayoffHistoryDepth];
    
    self.payoffsHistory = [PayoffDatabase loadPayoffDocs];
    
    [self reorderHistory];
    
    NSTimer *t = [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(reloadTable) userInfo:0 repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


-(void)viewDidDisappear:(BOOL)animated
{
}


#pragma mark - fetch scanned history
-(void)reloadTable
{
    BOOL needsReload = NO;
    
    for (int i=0; i<[self payoffsHistory].count; i++)
        if ([[((PayoffDoc *)self.payoffsHistory[i]).payoffItem date] timeIntervalSinceNow] < -kPayoffTimeoutInSeconds)
        {
            [((PayoffDoc *)self.payoffsHistory[i]) deleteDoc];
            [self.payoffsHistory removeObjectAtIndex:i];
            needsReload = YES;
        }
    if (needsReload)
    {
        [self.tableView_ reloadData];
    }
}

-(void)reorderHistory {
    self.payoffsHistory = [[self.payoffsHistory sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *date1 = [[(PayoffDoc*)obj1 payoffItem] date];
        NSDate *date2 = [[(PayoffDoc*)obj2 payoffItem] date];
        return [date2 compare:date1];
    }] mutableCopy];
    [self reloadTable];
}

-(void)newItemAvailable:(PayoffDoc *)doc {
    for (int i=0; i<[self.payoffsHistory count]; ) {
        PayoffDoc* existingDoc = (PayoffDoc*)self.payoffsHistory[i];
        if ([doc.payoffItem.payload isEqual:existingDoc.payoffItem.payload]) {
            NSLog(@"Incoming payload:%@, found previous item with same payload value in history.  Deleting existing (duplicate) payoff.", doc.payoffItem.payload);
            [existingDoc deleteDoc];
            [self.payoffsHistory removeObjectAtIndex:i];
            existingDoc = nil;
        } else {
            // if not removing an existing doc, then increment iterator
            i++;
        }
    }
    
    [self.payoffsHistory insertObject:doc atIndex:0];
    [doc saveData];
    
    if ([self.payoffsHistory count] > kPayoffHistoryDepth) {
        PayoffDoc *d = [self.payoffsHistory lastObject];
        [d deleteDoc];
        [self.payoffsHistory removeLastObject];
    }
    
    [self.tableView_ reloadData];
    [self.tableView_ scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - table view delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.payoffsHistory count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IsPhone)
    {
        return 80.0;
    }
    else
    {
        return 123;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_ScannedHistory *cell = (Cell_ScannedHistory *)[tableView dequeueReusableCellWithIdentifier:@"Cell_ScannedHistory"];
    
//    if (cell == nil)
//    {
//        // Load the top-level objects from the custom cell XIB.
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_ScannedHistory" owner:self options:nil];
//        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
//        cell = [topLevelObjects objectAtIndex:0];
//    }
    
    cell.delegate = self;
    PayoffItem *item = [((PayoffDoc*)self.payoffsHistory[indexPath.row]) payoffItem];

    cell.label_prodName.text = item.subtitle;
    
    
//    cell.label_prodName.text = item.title;
//    item.payload.
    [cell.imageView_prodImage setImage:[UIImage imageWithData:item.imageData]];

    item = nil;


    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.payOff_item = [((PayoffDoc*)self.payoffsHistory[indexPath.row]) payoffItem];
    
    
    single.productDetail = nil;
    
    [self performSegueWithIdentifier:single.string_exitSegueIdentifier sender:self];

//    [self performSelector:@selector(method_performTaskAfterDelay:) withObject:indexPath afterDelay:0.4];
}


#pragma mark - method_performTaskAfterDelay
-(void)method_performTaskAfterDelay:(NSIndexPath *)indexPath
{
    self.payOff_item = [((PayoffDoc*)self.payoffsHistory[indexPath.row]) payoffItem];

    
    [self performSegueWithIdentifier:single.string_exitSegueIdentifier sender:self];
}

#pragma mark - action_done
- (IBAction)action_done:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - method_BuyNow
-(void)method_BuyNow
{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SubmitFormViewController"] animated:YES];
//    UINavigationController *obj_Submitform = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmitFormViewController"] animated:YES];
//    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SubmitFormViewController"] animated:YES completion:nil];
}
@end