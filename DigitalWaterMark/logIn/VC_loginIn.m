#import "VC_loginIn.h"

#import "GetProductDataFromServer.h"
#import "scnnerViewController.h"
#import "RXCustomTabBar.h"


@interface VC_loginIn ()<GetDateFromServer>
{
}


@property (weak, nonatomic) IBOutlet UITextField *text_UserName;
@property (weak, nonatomic) IBOutlet UITextField *text_Password;
@property (weak, nonatomic) IBOutlet UIButton *button_login;




- (IBAction)action_login:(id)sender;
- (IBAction)unwindToVC_login:(UIStoryboardSegue *)unwindSegue;
- (IBAction)action_zipcode:(id)sender;
@end

@implementation VC_loginIn


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.button_login.layer.cornerRadius = 6.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


#pragma mark -method_login
-(void)method_Login:(NSDictionary *)dicloginCredential
{
    DLog(@"user login");
    
//    DLog(@"%@",dicloginCredential);
//    
//    NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/loginidentity",k_url];
//    DLog(@"strUrl: %@",strUrl);
//    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    GetProductDataFromServer *obj_loginIdentity = [GetProductDataFromServer new];
    obj_loginIdentity.delegate = self;
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.text_UserName.text,@"logonId",
                                self.text_Password.text,@"logonPassword", nil];
    
    [obj_loginIdentity method_loginidentity_serviceName:k_webServiceLoginIdentity dic_credentials:parameters];
    obj_loginIdentity = nil;
}




#pragma mark - method_guestLogin
-(void)method_guestLogin
{
    DLog(@"guest");
    GetProductDataFromServer *obj_loginIdentity = [GetProductDataFromServer new];
    obj_loginIdentity.delegate = self;
    [obj_loginIdentity method_guestLogin:k_webServiceGuestLogin];
    obj_loginIdentity = nil;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_zipcode"] || [segue.identifier isEqualToString:@"segue_register"])
    {
        self.text_UserName.text = @"";
        self.text_Password.text = @"";
    }
    
    
    if ([segue.identifier isEqualToString:@"segue_digimarc"])
    {
        RXCustomTabBar *tabBar = (RXCustomTabBar *)segue.destinationViewController;
        
        UINavigationController *nav = [tabBar.viewControllers objectAtIndex:2];

        scnnerViewController *obj = (scnnerViewController *) [nav topViewController];
        //obj.dic_tokens = (NSDictionary *)sender;
    }
}


#pragma mark - action_login
- (IBAction)action_login:(id)sender
{
    [self.view endEditing:YES];
    
    
    if ([self.text_UserName.text length] && [self.text_Password.text length])
    {
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.text_UserName.text,@"logonId",
                                    self.text_Password.text,@"logonPassword", nil];
        DLog(@"%@",parameters);
        [self method_Login:parameters];
            //[self performSegueWithIdentifier:@"segue_home" sender:nil];
    }
    else
    {
        if ([self.text_UserName.text length] == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter user name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}



#pragma mark - method_GotoHome
-(void)method_GotoHome
{
    [self performSegueWithIdentifier:@"segue_home" sender:nil];
}



#pragma mark - unwind from zip code controller
- (IBAction)unwindToVC_login:(UIStoryboardSegue *)unwindSegue
{
    DLog(@"exit and go to home screen");
    [self method_guestLogin];
}

- (IBAction)action_zipcode:(id)sender
{
    [self method_guestLogin];
}




//#pragma mark -
//#pragma mark -  POST by NSURL Session
//#pragma mark - NSURLResponseDelegate
//-(void)delegate_outPut:(NSData *)data urlRes:(NSURLResponse *)urlRes error_:(NSError *)error_ seriveName:(NSString *)serivename
//{
//    [SVProgressHUD dismiss];
//    
//    self.nsurl_obj.delegate = nil;
//    self.nsurl_obj = nil;
//    
//    if (error_ == nil)
//    {
//        // Convert JSON Object into Dictionary
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:
//                              NSJSONReadingMutableContainers error:&error_];
//        DLog(@"%@",JSON);
//        if ([JSON valueForKey:@"WCToken"])
//        {
//                ApplicationDelegate.single.isLoggedIn = NO;
//                ApplicationDelegate.single.string_WCToken = [JSON valueForKey:@"WCToken"];
//                ApplicationDelegate.single.string_WCTrustedToken = [JSON valueForKey:@"WCTrustedToken"];
//                [self performSegueWithIdentifier:@"segue_home" sender:nil];
//                DLog(@"WCToken = %@", ApplicationDelegate.single.string_WCToken);
//                DLog(@"WCTrustedToken = %@", ApplicationDelegate.single.string_WCTrustedToken);
//        }
//        else
//        {
//            if ([JSON valueForKey:@"errors"])
//            {
//                [[[UIAlertView alloc] initWithTitle:nil message:[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
//            }
//            else
//            {
//                [[[UIAlertView alloc] initWithTitle:nil
//                                            message:@"Authentication failed"
//                                           delegate:nil
//                                  cancelButtonTitle:@"Ok"
//                                  otherButtonTitles:nil, nil]
//                 show];
//            }
//        }
//    }
//    else
//    {
//        [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
//    }
//}





#pragma mark - web service delegate
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    if ([serviceName isEqualToString:k_webServiceGuestLogin] || [serviceName isEqualToString:k_webServiceLoginIdentity])
    {
        [self performSegueWithIdentifier:@"segue_digimarc" sender:dic_];
    }
//    else
//    {
//        
//        self.array_promotionCode = [[NSArray alloc] initWithArray:array_];
//        NSLog(@"%@",self.array_promotionCode);
//        [self.tableView_ reloadData];
//    }
}


@end