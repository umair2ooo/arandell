#import "AppDelegate.h"

#import "FBNetworkReachability.h"
#import "GetProductDataFromServer.h"
#import "AsyncImageView.h"


@interface AppDelegate ()
            

@end

@implementation AppDelegate


#pragma mark - internet settings
- (void)_didChangeConnectionMode:(NSNotification*)notification
{
    FBNetworkReachability *obj = [notification object];
    
    
    if ([[notification userInfo] valueForKey:@"flag"] &&
        [[[notification userInfo] valueForKey:@"flag"] intValue] == 65538 &&
        [[obj description] isEqualToString:@"Not available"])
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:k_networkError
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}



#pragma mark -  method_internetConnectivity
-(void)method_internetConnectivity
{
    // [1] test for async
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_didChangeConnectionMode:)
                                                 name:FBNetworkReachabilityDidChangeNotification
                                               object:nil];
    
    [[FBNetworkReachability sharedInstance] startNotifier];
}


-(NSString *)getUID
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)])
    {
        // This is will run if it is iOS6 and above
        return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
    return nil;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   // [AsyncImageLoader sharedLoader].cache = [AsyncImageLoader defaultCache];                  // Image YES/No


//    [UINavigationBar appearance].tintColor = [UIColor whiteColor];                                                              // back button color
    
    
//    GetProductDataFromServer *obj_GetProductDataFromServer = [GetProductDataFromServer new];
//    [obj_GetProductDataFromServer method_guestLogin];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];                                    // hide back button text
    
    
    
//    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:72.0/256 green:60.0/256 blue:78.0/256 alpha:1.0]];      // bar color
    
    
    // Customize the title text for *all* UINavigationBars                                                                      Bar text color
//    [[UINavigationBar appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0], UITextAttributeTextColor,
//      [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8], UITextAttributeTextShadowColor,
//      [NSValue valueWithUIOffset:UIOffsetMake(0, -1)], UITextAttributeTextShadowOffset,
//      [UIFont fontWithName:@"Arial-Bold" size:0.0], UITextAttributeFont,
//      nil]];
    
    
//    [[UINavigationBar appearance] setTranslucent:NO];
    
    
    [self method_internetConnectivity];
    
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    DLog(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    DLog(@"applicationDidEnterBackground");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    DLog(@"applicationWillEnterForeground");
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    DLog(@"applicationDidBecomeActive");
    
    if (![FBNetworkReachability sharedInstance].reachable)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:k_networkError
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    
    
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
