#import "ItemAvailabilityViewController.h"

#import "Singleton.h"
#import "AsyncImageView.h"
#import "Singleton.h"
#import <Social/Social.h>
#import "MGInstagram.h"
#import <Pinterest/Pinterest.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ThumbnailListView.h"
#import "ImageManager.h"
#import "MBProgressHUD.h"
#import "CustomSpinnerView.h"
#import "ActivityIndicator.h"
#import "ColorsDataClass.h"
#import "UIImage+UIImage_DrawText.h"
#import "GetProductDataFromServer.h"
#import "ScannedHistoryViewController.h"

#define k_gap 10.0
#define k_fontSizeOfColorsAndSizes 8.0

#define k_buttonTitle "Input Quantity"

@interface ItemAvailabilityViewController ()<UIAlertViewDelegate,MFMailComposeViewControllerDelegate,ThumbnailListViewDataSource,ThumbnailListViewDelegate, GetDateFromServer>
{
    Singleton *single;
    Pinterest*  _pinterest;
    
//    NSOperationQueue *operationQueue;
    
//    dispatch_queue_t imageQueue_;
    
    int int_indexOfSelectedColor;
    
    int int_indexOfSelectedSize;
}


@property (weak, nonatomic) IBOutlet UILabel *label_productName;
@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *label_available;
@property (weak, nonatomic) IBOutlet UILabel *label_quantity;

//@property (weak, nonatomic) IBOutlet UILabel *label_quantity;
@property (weak, nonatomic) IBOutlet UILabel *label_price;

@property (weak, nonatomic) IBOutlet UIButton *button_inputQuantity;

@property(strong,nonatomic) IBOutlet ThumbnailListView* thumbnailListView;
@property (weak, nonatomic) IBOutlet ThumbnailListView *thumbnailListViewSize;
@property (weak, nonatomic) IBOutlet UIView *view_colorsAndSizes;
@property (weak, nonatomic) IBOutlet UIButton *button_checkOut;
@property (weak, nonatomic) IBOutlet UIButton *button_addToCart;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;

// Circle D spinner (branding overlay shown on detecting (resolving) new payload ...
@property (retain)  MBProgressHUD *progressHUD;
@property (retain)  CustomSpinnerView *customSpinner;
@property(strong, nonatomic)ColorsDataClass *colorsDataClass_obj;

@property(nonatomic, strong)NSString *string_selected_S_And_C_Combinition;

@property(nonatomic, strong)GetProductDataFromServer *obj_GetProductDataFromServer;


- (IBAction)action_addToCart:(id)sender;
- (IBAction)action_inputQuantity:(id)sender;
- (IBAction)action_SocialSharing:(id)sender;
- (IBAction)action_activity:(id)sender;
- (IBAction)action_exit_itemAvail:(UIStoryboardSegue *)segue;
-(IBAction)action_goBack:(id)sender;

@end

@implementation ItemAvailabilityViewController

//@synthesize productsDataClass_obj = _productsDataClass_obj;
@synthesize colorsDataClass_obj = _colorsDataClass_obj;
@synthesize obj_GetProductDataFromServer = _obj_GetProductDataFromServer;



-(void)showSpinner {
    self.progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    self.progressHUD.center = self.view.center;//self.crosshairs.center;
    self.customSpinner = [[CustomSpinnerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.progressHUD setCustomView:self.customSpinner];
    [self.progressHUD setMode:MBProgressHUDModeCustomView];
    [self.view.window addSubview:self.progressHUD];
    [self.progressHUD show:YES];
    
    //    NSTimer *t = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(removeSpinner) userInfo:0 repeats:NO];
    //    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
}
-(void)removeSpinner {
    self.progressHUD.removeFromSuperViewOnHide = YES;
    [self.progressHUD hide:YES];
    [self.customSpinner stopAnimation];
    self.progressHUD = nil;
    self.customSpinner = nil;
}

//#pragma mark - method_setColorsImages
//-(void)method_setColorsImages
//{
//    _thumbnailListView.dataSource = self;
//    _thumbnailListView.delegate = self;
//    
//    int_indexOfSelectedColor = 0;
//    [_thumbnailListView reloadData];
//    [_thumbnailListView selectAtIndex:0];
//}
//
//
//
//#pragma mark - method_setSizeImages
//-(void)method_setSizeImages
//{
//    
//    _thumbnailListViewSize.dataSource = self;
//    _thumbnailListViewSize.delegate = self;
//    
//    [_thumbnailListViewSize reloadData];
//    [_thumbnailListViewSize selectAtIndex:0];
//}


#pragma mark - action_goBack
-(IBAction)action_goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark -method_getImageFromURL
- (void)method_getImageFromURL:(NSDictionary*)dict
{
    NSURL *url = [NSURL URLWithString:[dict objectForKey:@"url"]];
    UIButton *button = [dict objectForKey:@"button"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSError *error;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
//    UIImage *image = [[UIImage alloc] initWithData:data];
    
    [button setBackgroundImage:[[UIImage alloc] initWithData:data] forState:UIControlStateNormal];
}



#pragma mark - action_colorSelected
-(void)action_colorSelected:(UIButton *)sender
{
    self.string_selected_S_And_C_Combinition = nil;
    
    int_indexOfSelectedColor = sender.tag;
    
    NSArray *array_temp = self.thumbnailListView.subviews;
    
    [array_temp enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop){
        obj.layer.borderColor=[UIColor clearColor].CGColor;
    }];
    
    array_temp = nil;
    
    sender.layer.borderColor=[UIColor redColor].CGColor;
    
    
    array_temp = self.thumbnailListViewSize.subviews;
    
    [array_temp enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop){
        [obj removeFromSuperview];
    }];
    
    
    array_temp = nil;
    
    [self createSizesMenu];
}



#pragma mark - createColorsMenu
- (void)createColorsMenu
{
    NSArray *array_temp = self.thumbnailListView.subviews;

    id __xy;
    
    
    for (__xy in array_temp)
    {
        [__xy removeFromSuperview];
        __xy = nil;
    }
    array_temp = nil;
    
    
    
    array_temp = self.thumbnailListViewSize.subviews;
    for (__xy in array_temp)
    {
        [__xy removeFromSuperview];
        __xy = nil;
    }
    array_temp = nil;
    
    
    int x = 0;
    for (int i = 0; i < [single.productDetail.p_array_colors count]; i++)
    {
        self.colorsDataClass_obj = [single.productDetail.p_array_colors objectAtIndex:i];
//        DLog(@"%@", self.colorsDataClass_obj.color_url);
        
//        UIView *view_holder = [[UIView alloc] initWithFrame:CGRectMake(x+k_gap,
//                                                                       0,
//                                                                       self.thumbnailListView.frame.size.height,
//                                                                       self.thumbnailListView.frame.size.height)];
        
        
        
        AsyncImageView *imageView_temp = [[AsyncImageView alloc] initWithFrame:CGRectMake(x+k_gap,
                                                                                          0,
                                                                                    self.thumbnailListView.frame.size.height,
                                                                                    self.thumbnailListView.frame.size.height)];
        
        
        [imageView_temp loadImageFromURL:[NSString stringWithFormat:@"%@wcsstore/%@", k_url, self.colorsDataClass_obj.color_url]];
        
        imageView_temp.clipsToBounds = YES;
        
        //half of the width
//        imageView_temp.layer.cornerRadius = self.thumbnailListView.frame.size.height/2.0f;
        
        [imageView_temp setBackgroundColor:[UIColor clearColor]];

        
        
        
        
        
        
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x+k_gap,
                                                                      0,
                                                                      self.thumbnailListView.frame.size.height,
                                                                      self.thumbnailListView.frame.size.height)];
        
        [button setTag:i];
        
        

        
        [button addTarget:self action:@selector(action_colorSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        //Clip/Clear the other pieces whichever outside the rounded corner
        button.clipsToBounds = YES;
        
        //half of the width
//        button.layer.cornerRadius = self.thumbnailListView.frame.size.height/2.0f;
        
        [button setBackgroundColor:[UIColor clearColor]];
        
        button.layer.borderWidth=2.0f;
        button.layer.borderColor=[UIColor clearColor].CGColor;
        
        
        
        
        
//        [view_holder addSubview:imageView_temp];
//        [view_holder addSubview:button];
        
        [self.thumbnailListView addSubview:imageView_temp];
        [self.thumbnailListView addSubview:button];
        
        if (i==0)
        {
            int_indexOfSelectedColor = i;
            [self action_colorSelected:button];
        }
        
        
        x += button.frame.size.width+k_gap;
        
        
        //        [button setTitle:[NSString stringWithFormat:@"L"] forState:UIControlStateNormal];
        //        button.titleLabel.font = [UIFont systemFontOfSize:k_fontSizeOfColorsAndSizes];
        //        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        //        [button setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateSelected];
        //        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        //
        //
        //        [button setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        //        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
        
//        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
//                                                                                selector:@selector(method_getImageFromURL:)
//                                                                                  object:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                                                          [NSString stringWithFormat:@"%@wcsstore/%@", k_url,
//                                                                                           self.colorsDataClass_obj.color_url], @"url",
//                                                                                          button, @"button", nil]];
//        
//        [queue addOperation:operation];
    }
    
    self.thumbnailListView.contentSize = CGSizeMake(x, self.thumbnailListView.frame.size.height);
    self.thumbnailListView.backgroundColor = [UIColor clearColor];
}




#pragma mark - action_sizeSelected
-(void)action_sizeSelected:(UIButton *)sender
{
    int_indexOfSelectedSize = sender.tag;
    self.colorsDataClass_obj = [single.productDetail.p_array_colors objectAtIndex:int_indexOfSelectedColor];
    
    NSArray *array_temp = self.thumbnailListViewSize.subviews;
    
    [array_temp enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop){
        [obj setSelected:NO];
    }];

    
    if (!sender.selected)
    {
        sender.selected = YES;
    }
    
    self.string_selected_S_And_C_Combinition = [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:sender.tag] valueForKey:@K_SKUUniqueID];

    DLog(@"SKU ID: %@", self.string_selected_S_And_C_Combinition);
    
    if (!self.obj_GetProductDataFromServer)
    {
        self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
        self.obj_GetProductDataFromServer.delegate = self;
    }
    
//    [self.obj_GetProductDataFromServer method_getProductImageBySKU:self.string_selected_S_And_C_Combinition
//                                                       serviceName:k_webServiceGetProductBySKU];
    

    array_temp = nil;
}



#pragma mark - createSizesMenu
- (void)createSizesMenu
{
    self.colorsDataClass_obj = [single.productDetail.p_array_colors objectAtIndex:int_indexOfSelectedColor];
    
    int x = 0;
    for (int i = 0; i < [self.colorsDataClass_obj.color_array_sizes count]; i++)
    {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x+k_gap,
                                                                      0,
                                                                      self.thumbnailListViewSize.frame.size.height,
                                                                      self.thumbnailListViewSize.frame.size.height)];
        
        [button setTag:i];
        [button setTitle:[[self.colorsDataClass_obj.color_array_sizes objectAtIndex:i] valueForKey:@k_size] forState:UIControlStateNormal];
        
        button.titleLabel.font = [UIFont systemFontOfSize:k_fontSizeOfColorsAndSizes];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;

        [button addTarget:self action:@selector(action_sizeSelected:) forControlEvents:UIControlEventTouchUpInside];

        //Clip/Clear the other pieces whichever outside the rounded corner
        button.clipsToBounds = YES;

        //half of the width
//        button.layer.cornerRadius = self.thumbnailListViewSize.frame.size.height/2.0f;
        
        [button setBackgroundColor:[UIColor clearColor]];
        
        
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        

        button.layer.borderColor=[UIColor redColor].CGColor;
        button.layer.borderWidth=2.0f;
        
        [self.thumbnailListViewSize addSubview:button];
        
        x += button.frame.size.width+k_gap;
        
        if (i==0)
        {
            [self action_sizeSelected:button];
        }
    }
    
    self.thumbnailListViewSize.contentSize = CGSizeMake(x, self.thumbnailListViewSize.frame.size.height);
    self.thumbnailListViewSize.backgroundColor = [UIColor clearColor];
}


#pragma mark - method_inventoryAvailability
-(void)method_inventoryAvailability
{
    if (!self.obj_GetProductDataFromServer)
    {
        self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
        self.obj_GetProductDataFromServer.delegate = self;
    }
    
    DLog(@"%@", single.string_singleSKUUniqueID);
    
    DLog(@"%@", [NSDictionary dictionaryWithObjectsAndKeys:single.string_singleSKUUniqueID?single.string_singleSKUUniqueID:self.string_selected_S_And_C_Combinition, k_id, nil]);
    
    [self.obj_GetProductDataFromServer method_inventoryAvailability:[NSDictionary dictionaryWithObjectsAndKeys:single.string_singleSKUUniqueID?single.string_singleSKUUniqueID:self.string_selected_S_And_C_Combinition, k_id, nil]
                                                        serviceName:k_inventoryAvailability];
}


#pragma mark - method_setValuesOnXIB
-(void)method_setValuesOnXIB
{
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
    
    
    int_indexOfSelectedColor = 0;
    
    
    
    self.view_colorsAndSizes.hidden = YES;
    
    
//    if (!single.string_singleSKUUniqueID)               // it means there are multiple options for this product.
//    {
//        [self createColorsMenu];
//        self.view_colorsAndSizes.hidden = NO;
//    }
//    else
//    {
//        self.view_colorsAndSizes.hidden = YES;
//    }



    if (!single.array_sizesImages)
    {
        single.array_colorsImages = [[NSMutableArray alloc] init];
        single.array_sizesImages = [[NSMutableArray alloc] init];
    }

    self.label_productName.text = single.productDetail.p_name;
    
    
//    DLog(@"%@", single.productDetail.p_price);
//    DLog(@"%f", [single.productDetail.p_price floatValue]);
//    DLog(@"%@", [NSString stringWithFormat:@"%.2f", [single.productDetail.p_price floatValue]]);
    
    
    self.label_price.text = single.productDetail.p_price;
    
    
    self.imageView_product.image = [UIImage imageNamed:@"Placeholder.png"];
    [self.imageView_product loadImageFromURL:[single.productDetail.p_imageURL absoluteString]];
}


#pragma mark - cycle
-(void)viewWillAppear:(BOOL)animated
{
    [self.button_addToCart setEnabled:NO];
    [self.button_checkOut setEnabled:NO];

//    [super viewWillAppear:animated];
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.imageView_product.image = nil;
    
//    [self performSelectorOnMainThread:@selector(method_setValuesOnXIB) withObject:nil waitUntilDone:YES];
    [self method_setValuesOnXIB];
    
    
    [self method_inventoryAvailability];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(method_tabBar:)
                                                 name:k_notificationForTabBar
                                               object:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setBackgroundImage:[UIImage imageNamed:@k_backButtonImage] forState:UIControlStateNormal];
//    [button sizeToFit];
//    [button addTarget:self action:@selector(action_goBack) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@k_backButtonImage]
//                                                                             style:UIBarButtonItemStyleDone
//                                                                            target:self
//                                                                            action:@selector(action_goBack)];
//    self.navigationItem.hidesBackButton = YES;

    
    single = [Singleton retriveSingleton];
    
    
    

    DLog(@"color:%@", single.productDetail.p_colorURL_array);
    DLog(@"%@", single.productDetail.p_imageURL);
    
    

    // Initialize a Pinterest instance with our client_id
    _pinterest = [[Pinterest alloc] initWithClientId:@"1443279" urlSchemeSuffix:@"prod"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)viewDidDisappear:(BOOL)animated
{
//    //     All instances of TestClass will be notified
//    [[NSNotificationCenter defaultCenter] postNotificationName:@k_notification
//                                                        object:self
//                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:self.productsDataClass_obj.p_imageURL_AR, @k_ARImage, nil]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.obj_GetProductDataFromServer.delegate = nil;
    self.obj_GetProductDataFromServer = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark - action_addToCart
- (IBAction)action_addToCart:(id)sender
{
//    POST	/store/{storeId}/cart
    
    
    if (!self.string_selected_S_And_C_Combinition && !single.string_singleSKUUniqueID)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Select size first"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    else
    {
        if ([self.button_inputQuantity.titleLabel.text length] && ![self.button_inputQuantity.titleLabel.text isEqualToString:@k_buttonTitle])
        {
            if (!self.obj_GetProductDataFromServer)
            {
                self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
                self.obj_GetProductDataFromServer.delegate = self;
            }
            
            
            
            NSArray *array_temp = [NSArray arrayWithObjects:
                                   [NSDictionary dictionaryWithObjectsAndKeys:
                                    single.string_singleSKUUniqueID?single.string_singleSKUUniqueID:self.string_selected_S_And_C_Combinition,@"productId",
                                    self.button_inputQuantity.titleLabel.text,@"quantity", nil], nil];
            
            [self.obj_GetProductDataFromServer method_addToCart:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                 single.string_orderId?single.string_orderId:@".",@"orderId",
                                                                 array_temp, @"orderItem",
                                                                 @"0",@"x_calculateOrder",
                                                                 @"true",@"x_inventoryValidation", nil]
                                                    serviceName:k_webServiceAddToCart];
            
            array_temp = nil;
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Enter Quantity"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
}


#pragma mark - action_inputQuantity
- (IBAction)action_inputQuantity:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Quantity"
                              message:@"Please enter quantity:"
                              delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:@"Cancel", nil];
    
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    /* Display a numerical keypad for this text field */
    UITextField *textField = [alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    
    [alertView show];
}


#pragma mark - action_SocialSharing
- (IBAction)action_SocialSharing:(id)sender
{
    //Twitter
    if ([sender tag] == 0)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:self.label_productName.text];
            [tweetSheet addURL:[NSURL URLWithString:@"http://www.royalcyber.com/"]];
            [tweetSheet addImage:self.imageView_product.image];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Twitter account not found. Go to the Settings application to add your Twitter account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //facebook
    else if ([sender tag] == 1)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller setInitialText:self.label_productName.text];
            [controller addURL:[NSURL URLWithString:@"http://www.royalcyber.com/"]];
            [controller addImage:self.imageView_product.image];
            [self presentViewController:controller animated:YES completion:Nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Facebook account not found. Go to the Settings application to add your Facebook account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //pinterest
    else if ([sender tag] == 2)
    {
       // NSLog(@"%@",self.productsDataClass_obj.p_imageURL);
        [_pinterest createPinWithImageURL:single.productDetail.p_imageURL sourceURL:[NSURL URLWithString:@"http://www.royalcyber.com/"] description:self.label_productName.text];
    }
    //instagram
    else if ([sender tag] == 3)
    {
        if ([MGInstagram isAppInstalled])
        {
            [MGInstagram postImage:self.imageView_product.image withCaption:self.label_productName.text inView:self.view];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Alert"
                                       message:@"Instagram must be installed on the device in order to post photo"
                                      delegate:nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil, nil]
             show];
        }
    }
    //email
    else
    {
        if([MFMailComposeViewController canSendMail])
        {
            [[ActivityIndicator currentIndicator] show];
//            [self performSelector:@selector(showSpinner) onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
         //   [self showSpinner];
            MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
            emailDialog.mailComposeDelegate = self;
            [emailDialog setSubject:self.label_productName.text];
            [emailDialog setMessageBody:self.label_productName.text isHTML:NO];
            
            UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:single.productDetail.p_imageURL]];
            [emailDialog addAttachmentData:UIImageJPEGRepresentation(image, 1) mimeType:@"image/jpeg" fileName:@"product.jpeg"];
            
            [self presentViewController:emailDialog animated:YES completion:^(void)
             {
                 [[ActivityIndicator currentIndicator] hide];
                // [self removeSpinner];
                 NSLog(@"composer open");
             }];
            
            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
//                
//                MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
//                emailDialog.mailComposeDelegate = self;
//                [emailDialog setSubject:self.label_productName.text];
//                [emailDialog setMessageBody:self.label_productName.text isHTML:NO];
//                
//                UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.productsDataClass_obj.p_imageURL]];
//                [emailDialog addAttachmentData:UIImageJPEGRepresentation(image, 1) mimeType:@"image/jpeg" fileName:@"product.jpeg"];
//                dispatch_async(dispatch_get_main_queue(), ^{ // 2
//                    [self presentViewController:emailDialog animated:YES completion:^(void)
//                     {
//                         [self removeSpinner];
//                         NSLog(@"composer open");
//                     }]; // 3
//                    
//                    
//                });
//            });
            
            
            
            
            
            
            
            
            
            
            
//            MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
//            emailDialog.mailComposeDelegate = self;
//            NSMutableString *htmlMsg = [NSMutableString string];
//            [htmlMsg appendString:@"<html><body><p>"];
//            [htmlMsg appendString:self.label_productName.text];
//            [htmlMsg appendString:@": %@</p></body></html>"];
//            
//            NSData *jpegData = UIImageJPEGRepresentation(self.imageView_product.image, 1);
//            
//            NSString *fileName = @"product";
//            fileName = [fileName stringByAppendingPathExtension:@"jpeg"];
//            [emailDialog addAttachmentData:jpegData mimeType:@"image/jpeg" fileName:fileName];
//            
//            [emailDialog setSubject:self.label_productName.text];
//            [emailDialog setMessageBody:htmlMsg isHTML:YES];
//            
//            
//            [self presentViewController:emailDialog animated:YES completion:^(void)
//             {
//                 NSLog(@"composer open");
//             }];
//            
            
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Alert"
                                        message:@"Your Email account is not configured on device. Please configure your email settings in device settings."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
}


#pragma mark- uialert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if ([textField.text length])
        {
            [self.button_inputQuantity setTitle:textField.text forState:UIControlStateNormal];
        }
    }
}


#pragma mark- mailComposer Delegates
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
            
        case MFMailComposeResultCancelled:
            NSLog(@"%@",@"Result: canceled") ;
            break;
        case MFMailComposeResultSaved:
            NSLog(@"%@",@"Result: saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"%@",@"Result: Sent");
            break;
        }
        case MFMailComposeResultFailed:
            NSLog(@"%@",@"Result: Failed");
            break;
        default:
            NSLog(@"%@",@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//=================================================================================
#pragma mark - ThumbnailListViewDataSource
//=================================================================================
- (NSInteger)numberOfItemsInThumbnailListView:(ThumbnailListView*)thumbnailListView
{
    if (thumbnailListView.tag == 0)
        return [single.productDetail.p_array_colors count];
    else
    {
        self.colorsDataClass_obj = [single.productDetail.p_array_colors objectAtIndex:int_indexOfSelectedColor];
        
        return [self.colorsDataClass_obj.color_array_sizes count];
        
//        return [self.productsDataClass_obj.p_size_array count];
    }
}

- (UIImage*)thumbnailListView:(ThumbnailListView*)thumbnailListView
                 imageAtIndex:(NSInteger)index
{
    UIImage* thumbnailImage;

    if (thumbnailListView.tag == 0)
    {
        if ([single.array_colorsImages count] && index < [single.array_colorsImages count] && [single.array_colorsImages objectAtIndex:index])
        {
            return [single.array_colorsImages objectAtIndex:index];
        }
        
        else
        {
//            dispatch_async(imageQueue_, ^{
//                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[single.productDetail.p_colorURL_array objectAtIndex:index]]];
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    if (!imageData)
//                    {
//                        [single.array_colorsImages addObject:[UIImage imageNamed:@"Placeholder"]];
//                    }
//                    else
//                    {
//                        [single.array_colorsImages addObject:[UIImage imageWithData:imageData]];
//                    }
//                    
//                    
//                    [self.thumbnailListView reloadThumbnailImage];
//                });
//            });
        }
    }
    else
    {
        
        self.colorsDataClass_obj = [single.productDetail.p_array_colors objectAtIndex:int_indexOfSelectedColor];
        
        DLog(@"%@", [self.colorsDataClass_obj.color_array_sizes objectAtIndex:index]);
        
        if ([[self.colorsDataClass_obj.color_array_sizes objectAtIndex:index] isEqualToString:@"XS"])
        {
            thumbnailImage = [UIImage imageNamed:@"tag_XS"];
        }
        else if ([[self.colorsDataClass_obj.color_array_sizes objectAtIndex:index] isEqualToString:@"S"])
        {
            thumbnailImage = [UIImage imageNamed:@"tag_S"];
        }
        else if ([[self.colorsDataClass_obj.color_array_sizes objectAtIndex:index] isEqualToString:@"M"])
        {
            thumbnailImage = [UIImage imageNamed:@"tag_M"];
        }
        else if ([[self.colorsDataClass_obj.color_array_sizes objectAtIndex:index] isEqualToString:@"L"])
        {
            thumbnailImage = [UIImage imageNamed:@"tag_L"];
        }
        else if ([[self.colorsDataClass_obj.color_array_sizes objectAtIndex:index] isEqualToString:@"XL"])
        {
            thumbnailImage = [UIImage imageNamed:@"tag_XL"];
        }
        else if ([[self.colorsDataClass_obj.color_array_sizes objectAtIndex:index] isEqualToString:@"2XL"])
        {
            thumbnailImage = [UIImage imageNamed:@"tag_2XL"];
        }
        

//        thumbnailImage = [UIImage imageWithColor:[UIColor redColor]];
//        thumbnailImage = [UIImage drawText:self.colorsDataClass_obj.color_name inImage:thumbnailImage atPoint:CGPointMake(0, 0)];
//        
//        DLog(@"%f", thumbnailImage.size.width);
//        DLog(@"%f", thumbnailImage.size.height);
        
        
//        if ([single.array_sizesImages count] && index < [single.array_sizesImages count] && [single.array_sizesImages objectAtIndex:index])
//        {
//            return [single.array_sizesImages objectAtIndex:index];
//        }
//        
//        else
//        {
//            dispatch_async(imageQueue_, ^{
//                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[self.productsDataClass_obj.p_size_array objectAtIndex:index]]];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    if (!imageData)
//                    {
//                        [single.array_sizesImages addObject:[UIImage imageNamed:@"Placeholder"]];
//                    }
//                    else
//                    {
//                        [single.array_sizesImages addObject:[UIImage imageWithData:imageData]];
//                    }
//                    
//                    
//                    [self.thumbnailListViewSize reloadThumbnailImage];
//                });
//            });
//        }
    }
    return thumbnailImage;
}

//=================================================================================
#pragma mark - ThumbnailListViewDelegate
//=================================================================================
- (void)thumbnailListView:(ThumbnailListView*)thumbnailListView didSelectAtIndex:(NSInteger)index
{
    if (thumbnailListView == self.thumbnailListView)
    {
        int_indexOfSelectedColor = index;
    }
    
    
//    if (thumbnailListView.tag == 0)
//    {
//        self.imageView_product.image = [ImageManager imageNamed:@"Placeholder.png"];
//        // [self.imageView_product.image loadImageFromURL:[imagesURL objectAtIndex:indexPath.row]];
//        [self.imageView_product loadImageFromURL:[self.productsDataClass_obj.p_imageURL absoluteString]];
//    }
//    else
//    {
//        self.imageView_product.image = [ImageManager imageNamed:@"Placeholder.png"];
//        [self.imageView_product loadImageFromURL:[self.productsDataClass_obj.p_imageURL absoluteString]];
//    }
}

//- (CGSize)thumbnailListView:(ThumbnailListView *)tableView
//        sizeForImageAtIndex:(NSInteger)index
//{
//    return CGSizeMake(20, 20);
//}

//=================================================================================
#pragma mark - UIScrollViewDelegate
//=================================================================================
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //NSLog(@"%s",__func__);
    if( decelerate == NO ){
        [_thumbnailListView autoAdjustScroll];
        [_thumbnailListViewSize autoAdjustScroll];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //NSLog(@"%s",__func__);
    [_thumbnailListView autoAdjustScroll];
    [_thumbnailListViewSize autoAdjustScroll];
}




#pragma mark - web service delegate
//-(void)method_isDataSucess:(BOOL)bool_
//{
//    if (bool_)
//    {
////        [[[UIAlertView alloc] initWithTitle:nil
////                                    message:@"Successfully added"
////                                   delegate:nil
////                          cancelButtonTitle:@"Ok"
////                          otherButtonTitles:nil, nil]
////         show];
//
//
//        
////        single.productDetail.p_quantityRequired = [self.button_inputQuantity.titleLabel.text intValue];
////
////
//////        orderItemId
////        
////
////        if (![single.array_cartObjects count])
////        {
////            single.array_cartObjects =[[NSMutableArray alloc] init];
////        }
////        
////        
////        [single.array_cartObjects insertObject:[NSDictionary dictionaryWithObjectsAndKeys:
////                                                self.string_selected_S_And_C_Combinition, @"combinationalProductId",
////                                                single.productDetail.p_id, @"mainProductId",
////                                                self.colorsDataClass_obj.color_url, @"colorURL",
////                                                [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:@k_size], @"size",
////                                                [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue], @"Price",
////                                                self.label_productName.text = single.productDetail.p_name, @"productName",
////                                                self.button_inputQuantity.titleLabel.text, @"inputQuantity",
////                                                single.productDetail.p_imageURL, @"productImageURL", nil]  atIndex:0];
////        DLog(@"%@", [single.array_cartObjects objectAtIndex:0]);
//
//
//
////        DLog(@"id of specific product: %@", self.string_selected_S_And_C_Combinition);
////        DLog(@"product id: %@", single.productDetail.p_id);
////        DLog(@"color url: %@", self.colorsDataClass_obj.color_url);
////        DLog(@"size: %@", [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:@k_size]);
////        DLog(@"Price: %@", [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue]);
////        DLog(@"product name: %@", self.label_productName.text = single.productDetail.p_name);
////        DLog(@"input quantity: %@", self.button_inputQuantity.titleLabel.text);
////        DLog(@"%@", [single.productDetail.p_imageURL absoluteString]);
//
//
////        NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
////                                  self.string_selected_S_And_C_Combinition, @"combinationalProductId",
////                                  single.productDetail.p_id, @"mainProductId",
////                                  self.colorsDataClass_obj.color_url, @"colorURL",
////                                  [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:@k_size], @"size",
////                                  [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue], @"Price",
////                                  self.label_productName.text = single.productDetail.p_name, @"productName",
////                                  self.button_inputQuantity.titleLabel.text, @"inputQuantity",
////                                  single.productDetail.p_imageURL, @"productImageURL", nil];
////        
////        DLog(@"%@", dic_temp);
//        
////        ProductsDataClass *obj = [[ProductsDataClass alloc] init];
////        [obj setP_name:single.productDetail.p_name];
////        [obj setP_size_array:single.productDetail.p_size_array];
////        [obj setP_colorName:single.productDetail.p_colorName];
////        [obj setP_quantityRequired:[self.button_inputQuantity.titleLabel.text intValue]];
////        [obj setP_totalQuantity:single.productDetail.p_totalQuantity];
////        [obj setP_imageURL:single.productDetail.p_imageURL];
////        [obj setP_size_array:single.productDetail.p_size_array];
////        [obj setP_price:[single.productDetail.p_price stringByReplacingOccurrencesOfString:@"$: " withString:@""]];
////        
////        
////        if (![single.array_cartObjects count])
////        {
////            single.array_cartObjects =[[NSMutableArray alloc] init];
////        }
////        
////        
////        
////        [single.array_cartObjects insertObject:obj  atIndex:0];
////        
////        obj = nil;
////        
////        DLog(@"%@", [single.array_cartObjects objectAtIndex:0]);
////        
////        obj = [single.array_cartObjects objectAtIndex:0];
////        
////        DLog(@"%@", obj.p_id);
////        DLog(@"%@", obj.p_colorName);
////        DLog(@"%@", obj.p_colorURL_array);
////        DLog(@"%@", obj.p_size_array);
////        DLog(@"%@", obj.p_imageURL);
////        DLog(@"%d", obj.p_quantityRequired);
////        DLog(@"%@", obj.p_quantity);
////        DLog(@"%d", obj.p_totalQuantity);
//        
//        
//        
//    }
//    else
//    {
//        DLog(@"Item doesn't added");
//    }
//}

-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    if ([serviceName isEqualToString:k_webServiceGetProductBySKU])
    {
        DLog(@"%@", dic_);
        single.string_imageURL = nil;
        single.image_ARImage = nil;
        
        self.imageView_product.image = [UIImage imageNamed:@"Placeholder.png"];
        [self.imageView_product loadImageFromURL:[dic_ valueForKey:@k_detailImage]];
        
        single.string_imageURL = nil;
        single.string_imageURL = [NSURL URLWithString:[dic_ valueForKey:@k_detailImage]];
        return;
    }
    
    
    if ([serviceName isEqualToString:k_inventoryAvailability])
    {
        DLog(@"Alhamdulillah");
        
        DLog(@"%@", dic_);
        
        if ([dic_ valueForKey:@"InventoryAvailability"])
        {
            if ([[[dic_ valueForKey:@"InventoryAvailability"] objectAtIndex:0] valueForKey:@"inventoryStatus"])
            {
                self.label_available.text = [[[dic_ valueForKey:@"InventoryAvailability"] objectAtIndex:0] valueForKey:@"inventoryStatus"];
                
                if ([[[dic_ valueForKey:@"InventoryAvailability"] objectAtIndex:0] valueForKey:@"availableQuantity"])
                {
                    int int_temp = [[[[dic_ valueForKey:@"InventoryAvailability"] objectAtIndex:0] valueForKey:@"availableQuantity"] intValue];
                    
                    self.label_quantity.text = [NSString stringWithFormat:@"%d", int_temp];
                }
                
                if ([[[[dic_ valueForKey:@"InventoryAvailability"] objectAtIndex:0] valueForKey:@"inventoryStatus"] isEqualToString:@"Available"])
                {
                    [self.button_addToCart setEnabled:YES];
                    [self.button_checkOut setEnabled:YES];
                }
                else
                {
                    [self.button_addToCart setEnabled:NO];
                    [self.button_checkOut setEnabled:NO];
                }
            }
        }
    }
    
    
    if ([serviceName isEqualToString:k_webServiceAddToCart])
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Successfully added"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
        
        
        single.productDetail.p_quantityRequired = [self.button_inputQuantity.titleLabel.text intValue];
        
        
        //        orderItemId
        
        
        if (![single.array_cartObjects count])
        {
            single.array_cartObjects =[[NSMutableArray alloc] init];
        }
        
        
//        DLog(@"%@", [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue]);
//        
//        DLog(@"%@", [NSDictionary dictionaryWithObjectsAndKeys:
//                     [[[dic_ valueForKey:@"orderItem"] objectAtIndex:0] valueForKey:@"orderItemId"], @"orderItemId",
//                     self.string_selected_S_And_C_Combinition, @"combinationalProductId",
//                     single.productDetail.p_id, @"mainProductId",
//                     self.colorsDataClass_obj.color_url, @"colorURL",
//                     [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:@k_size], @"size",
//                     [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue], @"Price",
//                     self.label_productName.text = single.productDetail.p_name, @"productName",
//                     self.button_inputQuantity.titleLabel.text, @"inputQuantity",
//                     single.string_imageURL, @"productImageURL", nil]);
        
        
        
        DLog(@"%@", dic_);
        
        if (!single.string_singleSKUUniqueID)               // it means there are multiple options for this product.
        {
            [single.array_cartObjects insertObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                    [[[dic_ valueForKey:@"orderItem"] objectAtIndex:0] valueForKey:@"orderItemId"], @"orderItemId",
                                                    self.string_selected_S_And_C_Combinition, @"combinationalProductId",
                                                    single.productDetail.p_id, @"mainProductId",
                                                    self.colorsDataClass_obj.color_url, @"colorURL",
                                                    [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:@k_size], @"size",
//                                                    [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue], @"Price",
                                                    single.productDetail.p_price, @"Price",
                                                    self.label_productName.text = single.productDetail.p_name, @"productName",
                                                    self.button_inputQuantity.titleLabel.text, @"inputQuantity",
                                                    single.string_imageURL, @"productImageURL", nil]  atIndex:0];
        }
        else
        {
            [single.array_cartObjects insertObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                    [[[dic_ valueForKey:@"orderItem"] objectAtIndex:0] valueForKey:@"orderItemId"], @"orderItemId",
                                                    single.string_singleSKUUniqueID, @"combinationalProductId",
                                                    single.productDetail.p_id, @"mainProductId",
                                                    single.productDetail.p_price, @"Price",
                                                    self.label_productName.text = single.productDetail.p_name, @"productName",
                                                    self.button_inputQuantity.titleLabel.text, @"inputQuantity",
                                                    single.string_imageURL, @"productImageURL", nil]  atIndex:0];
        }
        
        
        
        DLog(@"%@", single.array_cartObjects);
        
    }
    
    if ([serviceName isEqualToString:k_webServiceGetMainProduct])
    {
//        [self performSelectorOnMainThread:@selector(method_setValuesOnXIB) withObject:nil waitUntilDone:YES];
        [self method_setValuesOnXIB];
    }
}

//-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ serviceName:(NSString *)serviceName
//{
//    if ([serviceName isEqualToString:k_webServiceProductImage])
//    {
//        DLog(@"%@", dic_);
//        single.string_imageURL = nil;
//        single.image_ARImage = nil;
//        
//        self.imageView_product.image = [UIImage imageNamed:@"Placeholder.png"];
//        [self.imageView_product loadImageFromURL:[dic_ valueForKey:@k_detailImage]];
//        
//        single.string_imageURL = nil;
//        single.string_imageURL = [NSURL URLWithString:[dic_ valueForKey:@k_detailImage]];
//        return;
//    }
//    
//    
//    if ([serviceName isEqualToString:k_webServiceAddToCart])
//    {
//        [[[UIAlertView alloc] initWithTitle:nil
//                                    message:@"Successfully added"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//        
//        
//        single.productDetail.p_quantityRequired = [self.button_inputQuantity.titleLabel.text intValue];
//        
//        
//        //        orderItemId
//        
//        
//        if (![single.array_cartObjects count])
//        {
//            single.array_cartObjects =[[NSMutableArray alloc] init];
//        }
//        
//        
//        DLog(@"%@", [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue]);
//        
//        [single.array_cartObjects insertObject:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                [[[dic_ valueForKey:@"orderItem"] objectAtIndex:0] valueForKey:@"orderItemId"], @"orderItemId",
//                                                self.string_selected_S_And_C_Combinition, @"combinationalProductId",
//                                                single.productDetail.p_id, @"mainProductId",
//                                                self.colorsDataClass_obj.color_url, @"colorURL",
//                                                [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:@k_size], @"size",
//                                                [[self.colorsDataClass_obj.color_array_sizes objectAtIndex:int_indexOfSelectedSize] valueForKey:K_SKUPriceValue], @"Price",
//                                                self.label_productName.text = single.productDetail.p_name, @"productName",
//                                                self.button_inputQuantity.titleLabel.text, @"inputQuantity",
//                                                single.string_imageURL, @"productImageURL", nil]  atIndex:0];
//        
//        DLog(@"%@", [single.array_cartObjects objectAtIndex:0]);
//
//    }
//}






#pragma mark - drawText in image
+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:12];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForItemAvail";
    
    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}


#pragma mark - action_exit_cart
- (IBAction)action_exit_itemAvail:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForItemAvail"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;
        
//        DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil]);
        
        if (!self.obj_GetProductDataFromServer)
        {
            self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
            self.obj_GetProductDataFromServer.delegate = self;
        }
        
        single.string_productID = [NSString stringWithFormat:@"%@", obj.payOff_item.title];
        
        [self.obj_GetProductDataFromServer method_makeIDFromPayLoad:single.string_productID];
    }
    else
    {
        DLog(@"Done only");
    }
}


#pragma mark - NSNotification method
-(void)method_tabBar:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:k_notificationForTabBar])
        [self.navigationController popToRootViewControllerAnimated:NO];
}
@end