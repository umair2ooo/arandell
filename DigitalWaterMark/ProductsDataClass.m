#import "ProductsDataClass.h"

#import "LocationDataClass.h"
#import "ColorsDataClass.h"




@implementation ProductsDataClass

@synthesize p_id;
@synthesize p_name;
@synthesize p_rating;
@synthesize p_price;
@synthesize p_sale;
@synthesize p_inStock;
@synthesize p_model;
@synthesize p_skuNumber;
@synthesize p_imageURL;
@synthesize p_imageURL_AR;
@synthesize array_location;
@synthesize p_description;
@synthesize p_colorURL_array;
@synthesize p_size_array;
@synthesize p_totalQuantity;
@synthesize p_quantityRequired;
@synthesize p_quantity;
@synthesize p_reviews;
@synthesize p_colorName;
@synthesize p_array_colors;
@synthesize p_array_stores;
@synthesize p_subTotal;
@synthesize p_subTotalCurrency;
@synthesize p_orderItemId;


-(void)setP_id:(NSString *)p_idx
{
    if (p_idx)
    {
        p_id = p_idx;
    }
    else
    {
        p_id = @"";
    }
}


-(void)setP_name:(NSString *)p_namex
{
    if (p_namex)
    {
        p_name = p_namex;
    }
    else
    {
        p_name = @"";
    }
}

-(void)setP_orderItemId:(NSString *)p_orderItemIdx
{
    if (p_orderItemIdx)
    {
        p_orderItemId = p_orderItemIdx;
    }
    else
    {
        p_orderItemId = @"";
    }
}


-(void)setP_rating:(int)p_ratingx
{
    if (p_ratingx)
    {
        p_rating = p_ratingx;
    }
    else
    {
        p_rating = 0;
    }
}


-(void)setP_price:(NSString *)p_pricex
{
    if (p_pricex)
    {
        p_price = [NSString stringWithFormat:@"$%.2f", [p_pricex floatValue]];
    }
    else
    {
        p_price = @"$0";
    }
}


-(void)setP_sale:(NSString *)p_salex
{
    if (p_salex)
    {
        p_sale = p_salex;
    }
    else
    {
        p_sale = @"0";
    }
}


-(void)setP_inStock:(NSString *)p_inStockx
{
    if (p_inStockx)
    {
        p_inStock = p_inStockx;
    }
    else
    {
        p_inStock = @"0";
    }
}


-(void)setP_model:(NSString *)p_modelx
{
    if (p_modelx)
    {
        p_model = p_modelx;
    }
    else
    {
        p_model = @"-";
    }
}


-(void)setP_skuNumber:(NSString *)p_skuNumberx
{
    if (p_skuNumberx)
    {
        p_skuNumber = p_skuNumberx;
    }
    else
    {
        p_skuNumber = @"-";
    }
}



-(void)setP_description:(NSString *)p_descriptionx
{
    if (p_descriptionx)
    {
        p_description = p_descriptionx;
    }
    else
    {
        p_description = @"-";
    }
}


-(void)setP_imageURL:(NSURL *)p_imageURLx
{
    if (p_imageURLx)
    {
        p_imageURL = p_imageURLx;
    }
    else
    {
        p_imageURL = nil;
    }
}


-(void)setP_imageURL_AR:(NSURL *)p_imageURL_ARx
{
    if (p_imageURL_ARx)
    {
        p_imageURL_AR = p_imageURL_ARx;
    }
    else
    {
        p_imageURL_AR = nil;
    }
}

-(void)setP_colorURL:(NSArray *)p_colorURLx
{
    if (p_colorURLx)
    {
        p_colorURL_array = [NSArray arrayWithArray:p_colorURLx];
    }
    else
    {
        p_colorURL_array = nil;
    }
}

-(void)setP_size:(NSArray *)p_sizex
{
    if (p_sizex)
    {
        p_size_array = [NSArray arrayWithArray:p_sizex];
    }
    else
    {
        p_size_array = nil;
    }
}


-(void)setP_totalQuantity:(int)p_totalQuantityx
{
    if (p_totalQuantityx)
    {
        p_totalQuantity = p_totalQuantityx;
    }
    else
    {
        p_totalQuantity = 0;
    }
}


-(void)setP_quantityRequired:(int)p_quantityRequiredx
{
    if (p_quantityRequiredx)
    {
        p_quantityRequired = p_quantityRequiredx;
    }
    else
    {
        p_quantityRequired = 0;
    }
}

-(void)setP_quantity:(NSString *)p_quantityx
{
    if (p_quantityx)
    {
        p_quantity = p_quantityx;
    }
    else
    {
        p_quantity = 0;
    }
}

-(void)setP_reviews:(NSString *)p_reviewsx
{
    if (p_reviewsx)
    {
        p_reviews = p_reviewsx;
    }
    else
    {
        p_reviews = nil;
    }
}



-(void)setP_colorName:(NSString *)p_colorNamex
{
    if (p_colorNamex)
    {
        p_colorName = p_colorNamex;
    }
    else
    {
        p_colorName = nil;
    }
}

-(void)setP_subTotal:(NSString *)p_subTotalx
{
    if (p_subTotalx)
    {
        p_subTotal = p_subTotalx;
    }
    else
    {
        p_subTotal = @"";
    }
}

-(void)setP_subTotalCurrency:(NSString *)p_subTotalCurrencyx
{
    if (p_subTotalCurrencyx)
    {
        p_subTotalCurrency = p_subTotalCurrencyx;
    }
    else
    {
        p_subTotalCurrency = @"";
    }
}




-(void)setArray_location:(NSMutableArray *)array_locationx
{
    if (array_locationx)
    {
        array_location = [[NSMutableArray alloc] init];
        [array_locationx enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            LocationDataClass *obj_ = [[LocationDataClass alloc] init];
            obj_.location_storeName = [obj valueForKey:@k_storeName];
            obj_.location_storeImageURL = [NSURL URLWithString:[obj valueForKey:@k_storeMap]];
            obj_.location_lat = [[obj valueForKey:@k_lat] floatValue];
            obj_.location_long = [[obj valueForKey:@k_long] floatValue];
            obj_.location_nearestStore = [[NSNumber numberWithInt:[[obj valueForKey:@k_nearestStore] intValue]] boolValue];
            obj_.location_Asle = [obj valueForKey:@k_aisle];
            obj_.location_Bay = [obj valueForKey:@k_bay];

            [array_location addObject:obj_];
            obj_ = nil;
        }];
    }
}


-(void)setP_array_colors:(NSMutableArray *)p_array_colorsx
{
    if (p_array_colorsx)
    {
        p_array_colors = [[NSMutableArray alloc] init];
        [p_array_colorsx enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             ColorsDataClass *obj_ = [[ColorsDataClass alloc] init];
             
             obj_.color_name = [obj valueForKey:@k_values];
             obj_.color_uniqueID = [obj valueForKey:@k_uniqueID];
             obj_.color_url = [obj valueForKey:@k_colorURL];
             obj_.color_array_sizes = [obj valueForKey:@k_sizes];
             
             [p_array_colors addObject:obj_];
             
             DLog(@"%@", p_array_colors);
             obj_ = nil;
         }];
    }
}
-(void)setP_array_stores:(NSMutableArray *)p_array_storex
{
    if (p_array_storex)
    {
        p_array_stores = [[NSMutableArray alloc] init];
        [p_array_storex enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             ColorsDataClass *obj_ = [[ColorsDataClass alloc] init];
             
             obj_.color_name = [obj valueForKey:@k_values];
             obj_.color_uniqueID = [obj valueForKey:@k_uniqueID];
             obj_.color_url = [obj valueForKey:@k_colorURL];
             obj_.color_array_sizes = [obj valueForKey:@k_sizes];
             
             [p_array_colors addObject:obj_];
             
             DLog(@"%@", p_array_colors);
             obj_ = nil;
         }];
    }
}

@end