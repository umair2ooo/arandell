#import <UIKit/UIKit.h>

@protocol BuyNow <NSObject>

-(void)method_BuyNow;

@end

@interface Cell_ScannedHistory : UITableViewCell
{
    
}
@property (nonatomic)id <BuyNow> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageView_prodImage;
@property (weak, nonatomic) IBOutlet UILabel *label_prodName;
- (IBAction)action_Buybutton:(id)sender;

@end