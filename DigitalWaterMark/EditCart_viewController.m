#import "EditCart_viewController.h"


#import "AsyncImageView.h"
#import "GetProductDataFromServer.h"
#import "ColorsDataClass.h"
#import "UIImage+UIImage_DrawText.h"
#import "GetProductDataFromServer.h"
#import "ProductsDataClass.h"

#define k_gap 10.0
#define k_fontSizeOfColorsAndSizes 14.0

@interface EditCart_viewController ()<GetDateFromServer>
{
    int int_indexOfSelectedColor;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scroller_color;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_size;
@property (weak, nonatomic) IBOutlet UITextField *textField_Quantity;

@property(nonatomic, strong)ProductsDataClass *obj_productsDataClass;
@property(nonatomic, strong)ColorsDataClass *obj_colorsDataClass;

@property(nonatomic, strong)NSString *string_selected_S_And_C_Combinition;

@property(nonatomic, strong)GetProductDataFromServer *obj_GetProductDataFromServer;
@property(nonatomic, strong)ProductsDataClass *obj_ProductsDataClass;

- (IBAction)action_Save:(id)sender;
- (IBAction)action_cancel:(id)sender;
@end

@implementation EditCart_viewController

@synthesize obj_GetProductDataFromServer = _obj_GetProductDataFromServer;
@synthesize string_selected_S_And_C_Combinition = _string_selected_S_And_C_Combinition;
@synthesize obj_productsDataClass = _obj_productsDataClass;

#pragma mark - action_colorSelected
-(void)action_colorSelected:(UIButton *)sender
{
    self.string_selected_S_And_C_Combinition = nil;

    int_indexOfSelectedColor = sender.tag;

    NSArray *array_temp = self.scroller_color.subviews;

    [array_temp enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop){
        obj.layer.borderColor=[UIColor clearColor].CGColor;
    }];

    array_temp = nil;

    sender.layer.borderColor=[UIColor redColor].CGColor;


    array_temp = self.scroller_size.subviews;

    [array_temp enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop){
        [obj removeFromSuperview];
    }];


    array_temp = nil;

    [self createSizesMenu];
}



#pragma mark - createColorsMenu
- (void)createColorsMenu
{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    int x = 0;
    for (int i = 0; i < [self.obj_productsDataClass.p_array_colors count]; i++)
    {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x+k_gap,
                                                                      0,
                                                                      self.scroller_color.frame.size.height,
                                                                      self.scroller_color.frame.size.height)];
        
        [button setTag:i];
        
        [button addTarget:self action:@selector(action_colorSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        //Clip/Clear the other pieces whichever outside the rounded corner
        button.clipsToBounds = YES;
        
        //half of the width
        button.layer.cornerRadius = self.scroller_color.frame.size.height/2.0f;
        
        [button setBackgroundColor:[UIColor clearColor]];
        
        
        button.layer.borderWidth=1.0f;
        
        [self.scroller_color addSubview:button];
        
        if (i==0)
        {
            int_indexOfSelectedColor = i;
            [self action_colorSelected:button];
        }
        
        
        x += button.frame.size.width+k_gap;
        
        
        
        
        self.obj_colorsDataClass = [self.obj_productsDataClass.p_array_colors objectAtIndex:i];
        DLog(@"%@", self.obj_colorsDataClass.color_url);
        
        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                selector:@selector(method_getImageFromURL:)
                                                                                  object:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@wcsstore/%@", k_url, self.obj_colorsDataClass.color_url], @"url", button, @"button", nil]];
        
        [queue addOperation:operation];
    }
    
    self.scroller_color.contentSize = CGSizeMake(x, self.scroller_color.frame.size.height);
    self.scroller_color.backgroundColor = [UIColor clearColor];
}




#pragma mark - action_sizeSelected
-(void)action_sizeSelected:(UIButton *)sender
{
    NSArray *array_temp = self.scroller_size.subviews;
    
    [array_temp enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop){
        [obj setSelected:NO];
    }];
    
    
    if (!sender.selected)
    {
        sender.selected = YES;
    }
    
    self.string_selected_S_And_C_Combinition = [[self.obj_colorsDataClass.color_array_sizes objectAtIndex:sender.tag] valueForKey:@K_SKUUniqueID];
    
    DLog(@"%@", self.string_selected_S_And_C_Combinition);
    
    
    array_temp = nil;
}



#pragma mark - createSizesMenu
- (void)createSizesMenu
{
    self.obj_colorsDataClass = [self.obj_productsDataClass.p_array_colors objectAtIndex:int_indexOfSelectedColor];
    
    int x = 0;
    for (int i = 0; i < [self.obj_colorsDataClass.color_array_sizes count]; i++)
    {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x+k_gap,
                                                                      0,
                                                                      self.scroller_size.frame.size.height,
                                                                      self.scroller_size.frame.size.height)];
        
        [button setTag:i];
        [button setTitle:[[self.obj_colorsDataClass.color_array_sizes objectAtIndex:i] valueForKey:@k_size] forState:UIControlStateNormal];
        
        button.titleLabel.font = [UIFont systemFontOfSize:k_fontSizeOfColorsAndSizes];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [button addTarget:self action:@selector(action_sizeSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        //Clip/Clear the other pieces whichever outside the rounded corner
        button.clipsToBounds = YES;
        
        //half of the width
        button.layer.cornerRadius = self.scroller_size.frame.size.height/2.0f;
        
        [button setBackgroundColor:[UIColor clearColor]];
        
        
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
        button.layer.borderColor=[UIColor redColor].CGColor;
        button.layer.borderWidth=2.0f;
        
        [self.scroller_size addSubview:button];
        
        x += button.frame.size.width+k_gap;
    }
    
    self.scroller_size.contentSize = CGSizeMake(x, self.scroller_size.frame.size.height);
    self.scroller_size.backgroundColor = [UIColor clearColor];
}



#pragma mark -method_getImageFromURL
- (void)method_getImageFromURL:(NSDictionary*)dict
{
    NSURL *url = [NSURL URLWithString:[dict objectForKey:@"url"]];
    UIButton *button = [dict objectForKey:@"button"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSError *error;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    //    UIImage *image = [[UIImage alloc] initWithData:data];
    
    [button setBackgroundImage:[[UIImage alloc] initWithData:data] forState:UIControlStateNormal];
}


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
    self.obj_GetProductDataFromServer.delegate = self;
    [self.obj_GetProductDataFromServer method_fetchDataToEditProduct:@"10002"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - get data from server delegate
-(void)method_dataFromServer:(ProductsDataClass *)product
{
    DLog(@"%@", product);
    self.obj_productsDataClass = product;
    [self createColorsMenu];
}
#pragma mark - webservice response
-(void)method_data
{
    
}
- (IBAction)action_Save:(id)sender
{
    self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
    self.obj_GetProductDataFromServer.delegate = self;
    [self.obj_GetProductDataFromServer method_editShoppingCart:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                self.obj_ProductsDataClass.p_orderItemId,@"orderItemId", nil] serviceName:k_webServiceEditCart];
}
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end