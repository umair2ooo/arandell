#import <UIKit/UIKit.h>

@protocol DeleteObject <NSObject>

-(void)method_direction:(int)tag;

@end

@interface Cell_directionOnMap : UITableViewCell
{
}
@property (weak, nonatomic) IBOutlet UILabel *label_storeName;
@property (weak, nonatomic) IBOutlet UILabel *label_storeDesc;
@property (weak, nonatomic) IBOutlet UILabel *label_storeDistance;
@property (weak, nonatomic) IBOutlet UIButton *button_Direction;

@property (nonatomic)id <DeleteObject> delegate;
-(void)method_setValues:(NSDictionary *)dic;
- (IBAction)action_DirectionToHere:(id)sender;

@end