//
//  DemoTableControllerViewController.h
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@class FPViewController;
@class DemoTableController;

@protocol DemoTableControllerDelegate <NSObject>

- (void)selectedValueFromDropDown:(NSString *)selectedValue controller:(DemoTableController *)controller;

@end

@interface DemoTableController : UITableViewController
{
    Singleton *single;
    NSArray *array_predictions;
}
@property(nonatomic,assign) FPViewController *delegate;
@property (nonatomic, strong) id <DemoTableControllerDelegate> delegated;
@property(nonatomic, strong)NSArray *array_predictions;

-(void)reloadList:(NSArray *)giveData;
@end
