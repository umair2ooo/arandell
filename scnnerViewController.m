#import "scnnerViewController.h"


// DMSDK Integration
//
#import "DMSManager.h"
#import "DMPayload.h"
#import "DMPayload+Internals.h"
#import "DMResolver.h"
#import "DMResolverSettings.h"
#import "GetProductDataFromServer.h"

// DMSDemo UI main view controls and data ...
//
#import "PayloadItem.h"
#import "PayoffDoc.h"
#import "PayoffDatabase.h"

#import "AudioVisualizer.h"
#import "CrosshairsView.h"

#import "MBProgressHUD.h"
#import "CustomSpinnerView.h"


#define kVisualizerHeight                   50
#define kPayoffHistoryTableOffFromCenter    80


// DMSDemo UI detail views ...
//
#import "HelpViewController.h"
#import "WebViewController.h"
#import "TextViewController.h"
#import "SettingsViewController.h"
#import "RXCustomTabBar.h"
#import "ProductDetailViewController.h"
#import "cell_promotionCodes.h"
#import "ScannedHistoryViewController.h"
#import "Singleton.h"

#define ROUND_BUTTON_WIDTH_HEIGHT 150.0

// DMSDemo list of lists diagnostic mode UI overlay (revealed with secret knock-knock gesture) ...
//
#define kMyApplicationName          @"DMSDemo"

#define kMyViewMode_Config          0
#define kMyViewMode_Errors          1
#define kMyViewMode_Warnings        2
#define kMyViewMode_Payoffs         3
#define kMyViewMode_Payloads        4
#define kMyViewMode_Index           5

#define kMyNavigationCellID         @"MyNavigationCell"
#define kMyIndexCellID              @"MyIndexCell"
#define kMyConfigCellID             @"MyConfigCell"
#define kMyErrorCellID              @"MyErrorCell"
#define kMyWarningCellID            @"MyWarningCell"
#define kMyPayoffCellID             @"MyPayoffCell"
#define kMyPayloadCellID            @"MyPayloadCell"

@class DMSManager;
@class DMSImageCameraSource;
@class DMSAudioMicAQSource;

@implementation warningItem
@end



@interface scnnerViewController ()<AVAudioPlayerDelegate, UITableViewDelegate, GetDateFromServer, UITableViewDataSource>
{
    // DMSDemo list of lists diagnostic UI data arrays ...
//    NSMutableArray* navigationItems;
    NSMutableArray* indexItems;
    NSMutableArray* configItems;
    NSMutableArray* errorItems;
    NSMutableArray* warningItems;
    NSMutableArray* payloadItems;
    
    NSArray *array_promotionCodes;
    
    int payloadItemNumber;
    
    
    BOOL bool_viewDisappeared;
    
    UIButton *button_scan;
    
    BOOL bool_isComingFromActivity;
    
    Singleton *single;
}
@property(nonatomic, strong)AVAudioPlayer *audioPlayer;

@property (retain) NSString* name;

// DMSDemo main UI view ...
//
@property (retain)  AVCaptureVideoPreviewLayer * previewLayer;
@property (retain)  CrosshairsView *crosshairs;
@property (retain)  AudioVisualizer *visualizer;

@property (retain)  NSDateFormatter* dateFormatter;

// Circle D spinner (branding overlay shown on detecting (resolving) new payload ...
@property (retain)  MBProgressHUD *progressHUD;
@property (retain)  CustomSpinnerView *customSpinner;

// DMSManager image/audio profile settings cache, used to save/restore profiles when temporarily setting to Idle for an alert view ...
@property           NSUInteger lastAudioProfile;
@property           NSUInteger lastImageProfile;

@property(nonatomic, strong)NSDictionary *dic_productInfo;

@property (weak, nonatomic) IBOutlet UITableView *tableView_;

@property(nonatomic, strong)NSArray *array_promotionCode;

- (IBAction)action_torch:(id)sender;
- (IBAction)action_activity:(id)sender;

- (IBAction)action_exit_scanner:(UIStoryboardSegue *)segue;

// DMSManager interfaces
//
- (void) initializeDms;
- (void) startDMSSources;
- (void) stopDMSSources;

//- (void) displayHelpView;
//- (void) displayTextViewWithText:(NSString*)text;
- (void) displayURLInWebView:(NSURL *)url;

@end

@implementation scnnerViewController

@synthesize dic_productInfo = _dic_productInfo;



#pragma mark - aperture sound
- (void) configureAVAudioSession
{
    //get your app's audioSession singleton object
    AVAudioSession* session = [AVAudioSession sharedInstance];
    
    //error handling
    BOOL success;
    NSError* error;
    
    //set the audioSession category.
    //Needs to be Record or PlayAndRecord to use audioRouteOverride:
    
    success = [session setCategory:AVAudioSessionCategoryPlayAndRecord
                             error:&error];
    
    if (!success)  NSLog(@"AVAudioSession error setting category:%@",error);
    
    //set the audioSession override
    success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
                                         error:&error];
    if (!success)  NSLog(@"AVAudioSession error overrideOutputAudioPort:%@",error);
    
    //activate the audio session
    success = [session setActive:YES error:&error];
    if (!success) NSLog(@"AVAudioSession error activating: %@",error);
    else NSLog(@"audioSession active");
}


-(void)method_apertureSound
{
    [self configureAVAudioSession];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"camera-shutter-click-01" ofType:@"mp3"]];
    NSError *error;
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    //    self.audioPlayer.currentTime = 0.0;
    self.audioPlayer.volume = 1.0;
    url = nil;
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    }
    else
    {
        [self.audioPlayer  play];
    }
    
    
    //    obj = nil;
}




-(void)method_sessionStartSound
{
    [self configureAVAudioSession];
    
//    camera-shutter-click-01
//    camera-focus-beep-01
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"camera-shutter-click-01" ofType:@"mp3"]];
    NSError *error;
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    //    self.audioPlayer.currentTime = 0.0;
    self.audioPlayer.volume = 1.0;
    url = nil;
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    }
    else
    {
        [self.audioPlayer  play];
    }
    
    
    //    obj = nil;
}





#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForScanner";

    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}

#pragma mark - action_exit_scanner
- (IBAction)action_exit_scanner:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForScanner"])
    {
        bool_isComingFromActivity = YES;
        

        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;
        
        self.dic_productInfo = [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(method_setupAndGoToTabBar) userInfo:nil repeats:NO];
    }
    else
    {
        DLog(@"Done only");
    }
}




#pragma mark - torch light on off
- (void)turnLightOn:(BOOL)on
{
    AVCaptureDevice *flashLight = [self flashLight];
    if ([flashLight lockForConfiguration:nil])
    {
        [flashLight setTorchMode:[flashLight isTorchActive] ? AVCaptureTorchModeOff : AVCaptureTorchModeOn];
        [flashLight unlockForConfiguration];
    }
}

- (AVCaptureDevice *)flashLight
{
    AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (![flashLight isTorchAvailable] ||
        ![flashLight isTorchModeSupported:AVCaptureTorchModeOn])
    {
        flashLight = nil;
    }
    return flashLight;
}

- (IBAction)action_torch:(id)sender
{
    [self turnLightOn:YES];
}


#pragma mark - beep functionality
-(void)method_startBeep
{
    if (self.audioPlayer)
    {
        self.audioPlayer.delegate = nil;
        self.audioPlayer = nil;
    }

   

    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"beep-06" ofType:@"mp3"]];
    NSError *error;
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    url = nil;
    
    self.audioPlayer .delegate = self;
    self.audioPlayer.currentTime = 0.1;
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    }
    else
    {
        [self.audioPlayer  play];
    }
}

-(void)method_stopBeep
{
    if (self.audioPlayer)
    {
        [self.audioPlayer stop];
        self.audioPlayer.delegate = nil;
        self.audioPlayer = nil;
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)aPlayer successfully:(BOOL)flag
{
    aPlayer.currentTime = 0.1;
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Add code here to do background processing
        //
        //
        [aPlayer play];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
        });
    });
}


#pragma mark - beep functionality end
#pragma mark -


#pragma mark - action_scan
-(IBAction)action_scan:(id)sender
{
//    [self method_sessionStartSound];
    
    [self.tableView_ setHidden:YES];
    [button_scan setHidden:YES];


    if (self.audioPlayer && [self.audioPlayer isPlaying])
    {
        [self method_stopBeep];
    }
    else
    {
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // Add code here to do background processing
            //
            //
//            [self method_startBeep];

            dispatch_async( dispatch_get_main_queue(), ^{
                // Add code here to update the UI/send notifications based on the
                // results of the background processing
            });
        });
    }


    [self setReadersToOn];
    
    
//    UIButton *btn = (UIButton *)sender;
//
//    if ([btn.titleLabel.text isEqualToString:@"Start Scan"])
//    {
//        [self method_buttonLayer:[UIColor redColor]];
//        
//        [btn setTitle:@"Scanning..." forState:UIControlStateNormal];
//        
//        [self method_blinkScanButton];
//        
//        [self setReadersToOn];
//    }
//    else
//    {
//        [self method_buttonLayer:[UIColor greenColor]];
//        
//        [btn setTitle:@"Start Scan" forState:UIControlStateNormal];
//        [button_scan.layer removeAllAnimations];
//        [self setReadersToIdle];
//    }
    
}

- (void)method_blinkScanButton
{
//    [UIView animateWithDuration:.05 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^ {
//        v.alpha = 1;
//    } completion:^(BOOL finished) {
////        [self flashOff:v];
//    }];
    
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration=0.25;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.0];
    [button_scan.layer addAnimation:theAnimation forKey:@"animateOpacity"]; //myButton.layer instead of theLayer
    //Note: don't forget to add QartzCore framework to your project
}

-(void)method_buttonLayer:(UIColor *)color_
{
    [[button_scan layer] setBorderColor:color_.CGColor];
}


-(void)method_addButtonToScan
{
    button_scan = [UIButton buttonWithType:UIButtonTypeCustom];

    //    [button_scan setTitle:@"Start Scan" forState:UIControlStateNormal];
    
    [button_scan setBackgroundImage:[UIImage imageNamed:@"scan_button"] forState:UIControlStateNormal];
    //width and height should be same value
    button_scan.frame = CGRectMake(0, 0, 64, 50);
    
    //Clip/Clear the other pieces whichever outside the rounded corner
    button_scan.clipsToBounds = YES;
    
    //half of the width
//    button_scan.layer.cornerRadius = ROUND_BUTTON_WIDTH_HEIGHT/2.0f;
//    button_scan.layer.borderColor=[UIColor redColor].CGColor;
//    button_scan.layer.borderWidth=2.0f;
    
    
    
    
    
//    button_scan = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button_scan setTitle:@"Start Scan" forState:UIControlStateNormal];

    button_scan.center = self.view.center;
    
//    [button_scan setFrame:CGRectMake((self.view.frame.size.width/2)-50,
//                                 self.view.frame.size.height-50,
//                                 100,
//                                 50)];

    
    [button_scan addTarget:self
               action:@selector(action_scan:)
     forControlEvents:UIControlEventTouchUpInside];
    
//    [[button_scan layer] setBorderWidth:2.0f];
    
//    [self method_buttonLayer:[UIColor greenColor]];
    
    [self.view insertSubview:button_scan aboveSubview:self.cameraPreviewContainer];
    
    button_scan.center = self.view.center;
    [button_scan setFrame:CGRectMake(IsPhone ? button_scan.frame.origin.x : 307,
                                     self.view.frame.size.height-100.0,
                                     IsPhone ? button_scan.frame.size.width : self.view.frame.size.width/5,
                                     button_scan.frame.size.height)];
    
    [self.view bringSubviewToFront:button_scan];
}

#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    //TODO: tableView setup
    
    bool_isComingFromActivity = NO;
    
    UINib *nib = [UINib nibWithNibName:@"cell_promotionCodes" bundle:nil];
    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"cell_promotionCodes"];
    
    array_promotionCodes = [NSArray arrayWithObjects:
                            @"Promotion One",
                            @"Promotion Two",
                            @"Promotion Three",
                            nil];
    
    [self.tableView_ reloadData];
    
    
    [self toggleShowDiagnosticsOverlay];
    
    // **************************************************************************************************
    //
    // Initialize the DMSDemo main panel UI views and layout ...
    //
    
    self.name = @"DMSDemo";
    
    self.cameraViewActive = YES;
    self.cameraPreviewContainer.hidden = NO;
    
    
    CGFloat cameraCenter = (self.view.frame.size.height / 4) + (kPayoffHistoryTableOffFromCenter / 2) - (kVisualizerHeight / 2) ;
    
    CGFloat crosshairsWidth = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? 300 : 150;
    
    self.crosshairs = [[CrosshairsView alloc] initWithFrame:CGRectMake(0, 0, crosshairsWidth, crosshairsWidth)];
    self.crosshairs.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.crosshairs setBackgroundColor:[UIColor redColor]];
    [self.view insertSubview:self.crosshairs aboveSubview:self.cameraPreviewContainer];
//    [self.view addConstraints:@[
//                                [NSLayoutConstraint constraintWithItem:self.crosshairs
//                                                             attribute:NSLayoutAttributeWidth
//                                                             relatedBy:NSLayoutRelationEqual
//                                                                toItem:nil
//                                                             attribute:NSLayoutAttributeNotAnAttribute
//                                                            multiplier:1.0
//                                                              constant:crosshairsWidth
//                                 ],
//                                [NSLayoutConstraint constraintWithItem:self.crosshairs
//                                                             attribute:NSLayoutAttributeHeight
//                                                             relatedBy:NSLayoutRelationEqual
//                                                                toItem:nil
//                                                             attribute:NSLayoutAttributeNotAnAttribute
//                                                            multiplier:1.0
//                                                              constant:crosshairsWidth
//                                 ],
//                                [NSLayoutConstraint constraintWithItem:self.crosshairs
//                                                             attribute:NSLayoutAttributeCenterX
//                                                             relatedBy:NSLayoutRelationEqual
//                                                                toItem:self.view
//                                                             attribute:NSLayoutAttributeCenterX
//                                                            multiplier:1.0
//                                                              constant:0.0
//                                 ],
//                                [NSLayoutConstraint constraintWithItem:self.crosshairs
//                                                             attribute:NSLayoutAttributeCenterY
//                                                             relatedBy:NSLayoutRelationEqual
//                                                                toItem:self.view
//                                                             attribute:NSLayoutAttributeCenterY
//                                                            multiplier:0.5
//                                                              constant:self.view.center.y-125
////                                 (kPayoffHistoryTableOffFromCenter / 2) - (kVisualizerHeight / 2)
//                                 ],
//                                ]
//     ];
    
    self.cameraPreviewContainer.center = ({
        CGPoint p = self.cameraPreviewContainer.center;
        p.y = cameraCenter;
        p;
    });
    
//    CGFloat buttonSize = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? 50.0 : 100.0;
    
//    UIButton *helpButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//        helpButton.transform = CGAffineTransformMakeScale(1.25, 1.25);
//    helpButton.translatesAutoresizingMaskIntoConstraints = NO;
//    [helpButton addTarget:self action:@selector(displayHelpView) forControlEvents:UIControlEventTouchUpInside];
//    [self.view insertSubview:helpButton belowSubview:self.visualizer];
//    [self.view addConstraints:  @[
//                                  [NSLayoutConstraint constraintWithItem:helpButton
//                                                               attribute:NSLayoutAttributeBottom
//                                                               relatedBy:NSLayoutRelationEqual
//                                                                  toItem:self.visualizer
//                                                               attribute:NSLayoutAttributeTop
//                                                              multiplier:1.0
//                                                                constant:10.0
//                                   ],
//                                  [NSLayoutConstraint constraintWithItem:helpButton
//                                                               attribute:NSLayoutAttributeRight
//                                                               relatedBy:NSLayoutRelationEqual
//                                                                  toItem:self.view
//                                                               attribute:NSLayoutAttributeRight
//                                                              multiplier:1.0
//                                                                constant:10.0
//                                   ],
//                                  [NSLayoutConstraint constraintWithItem:helpButton
//                                                               attribute:NSLayoutAttributeHeight
//                                                               relatedBy:NSLayoutRelationEqual
//                                                                  toItem:nil
//                                                               attribute:NSLayoutAttributeNotAnAttribute
//                                                              multiplier:1.0
//                                                                constant:buttonSize
//                                   ],
//                                  [NSLayoutConstraint constraintWithItem:helpButton
//                                                               attribute:NSLayoutAttributeWidth
//                                                               relatedBy:NSLayoutRelationEqual
//                                                                  toItem:nil
//                                                               attribute:NSLayoutAttributeNotAnAttribute
//                                                              multiplier:1.0
//                                                                constant:buttonSize
//                                   ]]
//     ];
    
    // **************************************************************************************************
    //
    // Initialize the list of lists diagnostic mode UI ...
    //
//    navigationItems = [[NSMutableArray alloc] init];
    indexItems = [[NSMutableArray alloc] init];
    configItems = [[NSMutableArray alloc] init];
    errorItems = [[NSMutableArray alloc] init];
    warningItems = [[NSMutableArray alloc] init];
    payloadItems = [[NSMutableArray alloc] init];
    payloadItemNumber = 0;
    
    self.viewMode = kMyViewMode_Index;
    
//    [navigationItems addObject:@"< Index"];
//    
//    [indexItems addObject:@"Configuration"];
//    [indexItems addObject:@"Errors"];
//    [indexItems addObject:@"Warnings"];
//    [indexItems addObject:@"Payoffs"];
//    [indexItems addObject:@"Payloads"];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setDateStyle:NSDateFormatterNoStyle];
    
    // Install the knock knock gesture recognizer to reveal the list of lists diagnostics view (overlay)
    //
    UITapGestureRecognizer *secretKnock = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleShowDiagnosticsOverlay)];
    secretKnock.numberOfTapsRequired = 5;
#if (TARGET_IPHONE_SIMULATOR)
    secretKnock.numberOfTouchesRequired = 1;
#else
    secretKnock.numberOfTouchesRequired = 3;
#endif
    [self.view addGestureRecognizer:secretKnock];

    secretKnock = nil;

    // At startup, the main panel UI will have archived payoffs data to show
    // but the diagnostics UI has not yet collected anything for the current session.
    //
    // Reduce user confusion over this issue by simulating one payload and one payoff
    // into diagnostics UI from the main panel UI data
    //
    [self performSelector:@selector(diagnosticsUISimulatePayloadsFromPayoffsHistory) withObject:nil afterDelay:0.1];

    // **************************************************************************************************
    //
    // Setup the DMSManager and app-defined image and audio sources...
    //
    [self initializeDms];
    [self registerNotificationCenterHooks];
    
    // This version of DMSDemo application automatically opens a new DMSDK session on startup and keeps it open for the life of the application.
    // Then as the view comes and goes, the session remains open, but the image/audio sources might be stopped and restarted, or the
    // image/audio profiles might be temporarily set to idle to suspend detection.
    //
    // Other applications might choose to open a DMS session modally during some activities, then close DMS session and release nearly all
    // DMSDK resources while the application moves on to other activities.
    //
    [self performSelector:@selector(openDMSSession) withObject:nil afterDelay:0.25];

    [self performSelector:@selector(method_addButtonToScan) withObject:nil afterDelay:0.25];
    
    
    [self.cameraPreviewContainer setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.crosshairs.center = self.cameraPreviewContainer.center;
    
    single = [Singleton retriveSingleton];
    
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"mobileteam",@"logonId",
                                @"passw0rd",@"logonPassword", nil];
    
    GetProductDataFromServer *obj_loginIdentity = [GetProductDataFromServer new];
    obj_loginIdentity.delegate = self;
//    [obj_loginIdentity method_loginidentity_serviceName:k_webServiceLoginIdentity dic_credentials:parameters];
    [obj_loginIdentity method_guestLogin:k_webServiceGuestLogin];
    obj_loginIdentity = nil;
    
    
//    GetProductDataFromServer *obj_getPomotionCodeData = [GetProductDataFromServer new];
//    [obj_getPomotionCodeData method_getPromotionCode:self.dic_tokens serviceName:k_webServicePomotionCode];
//    obj_getPomotionCodeData.delegate = self;
//    DLog(@"promotion code service hit");
//    obj_getPomotionCodeData = nil;
}

//-(void)method_getPromotionCode
//{
//    GetProductDataFromServer *obj_getPomotionCodeData = [GetProductDataFromServer new];
//    [obj_getPomotionCodeData method_getPromotionCode:nil];
//    DLog(@"promotion code service hit");
//    obj_getPomotionCodeData = nil;
//}
- (void) clearAppDataLists
{
    [errorItems removeAllObjects];
    [warningItems removeAllObjects];
    [payloadItems removeAllObjects];
    payloadItemNumber = 0;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [self.dmsManager clearAudioPayloadCache];
    [self.dmsManager clearImagePayloadCache];
    [self.dmsManager clearAudioBuffers];
    
    [self clearAppDataLists];
}



- (void)dealloc
{
//    [self.dmsManager closeSession];
    
    
    
    [self unRegisterNotificationCenterHooks];
    
//    navigationItems = nil;
    indexItems = nil;
    configItems = nil;
    errorItems = nil;
    warningItems = nil;
    payloadItems = nil;
    payloadItemNumber = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
//    [super viewWillAppear:animated];
    
 
    if (! bool_isComingFromActivity)
    {
        if (self.dmsManager)
        {
            self.dmsManager.delegate = self;
        }
        
        button_scan.hidden = NO;
        
        bool_viewDisappeared = NO;
        
        DLog(@"DMSDemo scnnerViewController viewWillAppear");
        
        
        // clear any selections or stale state in dignostics UI on return from a detail view
        
        
        // start or resume the image and audio sources and resume processing incoming media data
        //
        if(self.dmsManager.currentStatus == DMSStatusOpen)
        {
            [self startDMSSources];
        }
        
        [self setReadersToIdle];
//        [button_scan setTitle:@"Start Scan" forState:UIControlStateNormal];
        
        
        button_scan.center = self.view.center;
        
//        [button_scan setFrame:CGRectMake(button_scan.frame.origin.x,
//                                         self.view.frame.size.height-100.0,
//                                         button_scan.frame.size.width,
//                                         button_scan.frame.size.height)];

        
        [button_scan setFrame:CGRectMake(IsPhone ? button_scan.frame.origin.x : 307,
                                         self.view.frame.size.height-100.0,
                                         IsPhone ? button_scan.frame.size.width : self.view.frame.size.width/5,
                                         button_scan.frame.size.height)];
        
        
        [self.view bringSubviewToFront:button_scan];
//        self.tableView_.hidden = NO;
        
        if (button_scan)
        {
            button_scan.center = self.view.center;
            
            [button_scan setFrame:CGRectMake(IsPhone ? button_scan.frame.origin.x : 307,
                                             self.view.frame.size.height-100.0,
                                             IsPhone ? button_scan.frame.size.width : self.view.frame.size.width/5,
                                             button_scan.frame.size.height)];
            
//            [button_scan setFrame:CGRectMake((self.view.frame.size.width/2)-50,
//                                             self.view.frame.size.height-50,
//                                             100,
//                                             50)];
        }
    }
    self.tableView_.delegate = self;
    self.tableView_.dataSource = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    AVCaptureDevice *flashLight = [self flashLight];
    if ([flashLight lockForConfiguration:nil])
    {
        [flashLight setTorchMode:AVCaptureTorchModeOff];
        [flashLight unlockForConfiguration];
    }
    
    
    
    
    bool_viewDisappeared = YES;
    
    warningItems = nil;
    indexItems = nil;
    configItems = nil;
    errorItems = nil;
    warningItems = nil;
    payloadItems = nil;
    self.dateFormatter = nil;
    self.crosshairs = nil;
    
    
    
    [self method_buttonLayer:[UIColor greenColor]];
    
    [button_scan.layer removeAllAnimations];
    
    [self method_removeOldDataFromCache];
    
    self.tableView_.dataSource = nil;
    self.tableView_.delegate = nil;
}

- (void) viewDidDisappear:(BOOL)animated
{
    DLog(@"DMSDemo scnnerViewController viewDidDisappear");
    
    [self.imageCameraSource stop];
    
    
    [self method_removeOldDataFromCache];
    
    
    
    
    // stop or pause the image and audio sources and suspend processing
    // incoming media data until we return to the capture activity.
    //
    [self stopDMSSources];
    [super viewDidDisappear:animated];
}



#pragma mark - Misc UI and status methods
-(BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (void) addCameraPreview {
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.imageCameraSource.captureSession];
    
    CALayer *viewLayer = [self.cameraPreviewContainer layer];
    
    CGRect bounds = self.cameraPreviewContainer.bounds;
    [self.previewLayer setFrame:bounds];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [viewLayer addSublayer:self.previewLayer];
}

- (void) removeCameraPreview {
    if(self.previewLayer) {
        [self.previewLayer removeFromSuperlayer];
        self.previewLayer = nil;
    }
}

-(void)showSpinner {
    self.progressHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    self.progressHUD.center = self.view.center;//self.crosshairs.center;
    self.customSpinner = [[CustomSpinnerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.progressHUD setCustomView:self.customSpinner];
    [self.progressHUD setMode:MBProgressHUDModeCustomView];
    [self.view.window addSubview:self.progressHUD];
    [self.progressHUD show:YES];
    
//    NSTimer *t = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(removeSpinner) userInfo:0 repeats:NO];
//    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
}

-(void)removeSpinner {
    self.progressHUD.removeFromSuperViewOnHide = YES;
    [self.progressHUD hide:YES];
    [self.customSpinner stopAnimation];
    self.progressHUD = nil;
    self.customSpinner = nil;
}

- (void) handleWarningFrom:(NSString*)source message:(NSString*)msg {
    
    warningItem* item = [[warningItem alloc] init];
    item.date = [NSDate date];
    item.source = source;
    item.message = msg;
    
    [warningItems addObject:item];
}

- (void) handleError:(NSError*)error
{
    [self removeSpinner];
    
    
    [errorItems addObject:error];
    
    // TODO:  A future SDK distribution will move this error parsing up into DMSManager,
    //        defining a small number of error codes all in the DMSManagerErrorDomain.
    //
    NSString* title = @"Error Reported";
    NSString* message = [NSString stringWithFormat:@"%@", error.domain];
    
    if([error.domain isEqualToString:@"NSURLErrorDomain"])
    {
        title = @"Network Error";
        switch (error.code)
        {
            case -1003: message = @"Could not access resolver server.";  break;
            case -1009: message = @"Could not access network.  Please check your network settings and try again.";  break;
        }
    }
    else if([error.domain isEqualToString:@"DMHttpRequestErrorDomain"])
    {
        title = @"Resolver Error";
        switch (error.code)
        {
            case 401:
            case 403: message = @"Invalid resolver username/password credentials."; break;
            case 404: message = @"Resource not found on this server."; break;
        }
    }
    else if([error.domain isEqualToString:DigimarcResolverErrorDomain])
    {
        switch (error.code)
        {
            case kDMResolverErrorUnknownPayloadType:
                title = @"Resolver Error";
                message = @"Unknown payload type.";
                break;
                
                // No need to show this error to user, just return
            case kDMResolverErrorRequestCanceled:
                return;
        }
    }

    [self setReadersToIdle];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil
                          ];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self.dmsManager clearImagePayloadCache];
    [self.dmsManager clearAudioPayloadCache];
    [self setReadersToOn];
}


/****************************************************************************************
 *
 *    DMSManager Interfaces
 *
 ****************************************************************************************/

#pragma mark - Initialize DMSDK and DMResolver

- (void) initializeDms {
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create and Initialize DMSManager] */
    // create DMSManager
    //
    self.dmsManager = [DMSManager sharedDMSManager];
    self.dmsManager.delegate = self;
    
    // Default cache depth is 5; setting it here to 1 so we will receive new payload notification
    // any time the payload value changes, but not for repeat detections of the same payload value.
    // This pattern works best for the DMSDemo sample UI payoffs history table reordering functionality.
    //
    self.dmsManager.imagePayloadCacheMaxDepth = 1;
    self.dmsManager.audioPayloadCacheMaxDepth = 1;
    
    // Setup the optional reporting and logging criteria
    //
    self.dmsManager.reportOnlyNewDetections = NO;
    self.dmsManager.reportOptionalReaderInfoDiagnostics = YES;
    
    self.dmsManager.logImageReaderDispatches = NO;
    self.dmsManager.logImageReaderDetections = NO;
    self.dmsManager.logImageReaderDetectionReaderInfo = NO;
    
    self.dmsManager.logAudioReaderDispatches = NO;
    self.dmsManager.logAudioReaderDetections = NO;
    self.dmsManager.logAudioReaderDetectionReaderInfo = NO;
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create and Initialize DMSManager] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create and Initialize DMResolver] */
    // Create and Initialize DMResolver
    //
    self.dmResolver = [DMSManager sharedDMResolver];
    if(self.dmResolver) {
        
        //  Create a helper PayloadToPayoffResolver class to manage network availability
        // and handle async DMResolver queries.
        //
        self.payloadToPayoffResolver = [[PayloadToPayoffResolver alloc] init];
        self.payloadToPayoffResolver.delegate = self;
        self.payloadToPayoffResolver.resolver = self.dmResolver;
        [self.payloadToPayoffResolver updateResolverIsAvailable];
        
        // Supply DMResolver credentials
        //
        // NOTE:
        // The customer programmer must acquire Digimarc Resolver API credentials separately and install them
        // in .../DMS/config/DMResolverSettings.h. Otherwise DMRESOLVER_USERNAME remains undefined and
        // the compiler will issue an error here.
        //
        // If DMRESOLVER_USERNAME is defined and non-empty, compile time validation succeeds.
        // But the credentials themselves are not validated with DMResolver until the first attempt
        // to resolve a payload using PayloadToPayoffResolver.
        //
#ifdef DMRESOLVER_USERNAME
        self.dmResolver.userName = DMRESOLVER_USERNAME;
        self.dmResolver.password = DMRESOLVER_PASSWORD;
#else
#error Digimarc Resolver userName and password must be supplied in ../config/DMResolverSettings.h
#endif
    }
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create and Initialize DMResolver] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Configure Readers Array and Profiles] */
    // Load DMSDK readers configuration from JSON resource file
    //
    NSString* configFile = @"DMSReadersConfig";
    
    NSBundle* bundle = [NSBundle bundleForClass:[self class]];
    NSString* cfg = [bundle pathForResource:configFile ofType:@"json"];
    [self.dmsManager loadReadersConfigFromJSONFile:cfg];
    
    // Set initial profiles
    //
    self.dmsManager.imageReadingProfile = DMSProfileLow;
    self.dmsManager.audioReadingProfile = DMSProfileLow;
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Configure Readers Array and Profiles] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create Image Source] */
    // Create image media source
    //
    self.imageCameraSource = [[DMSImageCameraSource alloc] init];
    
    // Optional: setup any image source properties or options here ...
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create Image Source] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create Audio Source] */
    // Create audio media source
    //
    self.audioMicSource = [[DMSAudioMicAQSource alloc] init];
    
    // Optional: let DMSAudioMicAQSource manage the app's AVAudioSession
    //
    [self.audioMicSource setupAVAudioSession];
    self.audioMicSource.enableStallDetection = YES;
    
    // Hook up the audio visualizer connection
    //
    self.audioMicSource.delegate = self;
    [self.audioMicSource addObserver:self
                          forKeyPath:@"volume"
                             options:0
                             context:@"volumeDidChange"
     ];
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create Audio Source] */
}

#pragma mark - Open/Close DMSDK session


/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Open DMSManager Session 1] */
// Open and close DMSManager session
//
// The DMSDemo application starts a session immediately after initializing the DMSDK,
// and keeps the same session open for the life of the application.   For other application designs,
// you might tie the duration of the DMS session to the life of a particular view or application
// activity.  In this case you would call openDMSSession when the entering the view or activity
// and closeDMSSession when leaving the view or activity.
//
- (void) openDMSSession {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [self.dmsManager openSessionWithImageSource:self.imageCameraSource audioSource:self.audioMicSource];
    [self startDMSSources];
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Open DMSManager Session 1] */
#if (TARGET_IPHONE_SIMULATOR)
    // When building for the simulator, there is no actual camera device.
    // Simulate a couple of image watermark read notifications to exercise the rest of the DMSDemo UI.
    //
    DMPayload* payload1 = [[DMPayload alloc] initWithCpmPath:@"GC41.KE.WOM1.v5.2770"];    // 10096
    DMPayload* payload2 = [[DMPayload alloc] initWithCpmPath:@"GC41.KE.WOM1.v5.FEE6"];    // 62524
    [self dms:self.dmsManager detectedImageWatermark:payload1 withReaderInfo:nil isNew:YES];
    [self dms:self.dmsManager detectedImageWatermark:payload2 withReaderInfo:nil isNew:YES];
#endif
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Open DMSManager Session 2] */
}

- (void) closeDMSSession {
    // closeSession will automatically stop both the imageSource and audioSource if they are currently running.
    [self.dmsManager closeSession];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Open DMSManager Session 2] */


#pragma mark - Start and Stop DMSDK and image/audio media sources on UI view appear/disappear

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Start/Stop DMS and Media Sources] */
// Start and stop media sources
//
// These helper functions start and stop both the image and audio media sources.
// The current DMSManager session remains open. This allows the same start/stop methods
// to be used for both initial startup and temporary pause and resume.
//
// This is the recommended pattern when leaving the main view UI (or diagnostic UI overlay)
// to temporarily look at Help, a WebView, or one of the diagnostic UI detail views. This is
// triggered by the viewWillDisappear: notification on leaving, and the viewWillAppear: notification
// on returning.
//
- (void) startDMSSources {
    DLog(@"DMSDemo scnnerViewController startDMSSources");
    
    if(self.dmsManager && self.dmsManager.currentStatus == DMSStatusOpen) {
        [self.dmsManager startImageSource];
        [self.dmsManager startAudioSource];   // nop if already running
    }
    
    // The image source will have a new AVCaptureSession, so refresh the link to the preview layer
    [self removeCameraPreview];
    [self addCameraPreview];
}

- (void) stopDMSSources {
    DLog(@"DMSDemo scnnerViewController stopDMSSources");
    
    [self.dmsManager stopImageSource];
    [self.dmsManager stopAudioSource];
}
/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Start/Stop DMS and Media Sources] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Pause/Resume DMS While Media Sources Running] */
// Pause and resume media sources
//
// In some cases, the application may want to temporarily suspend DMSDK watermark and barcode detection
// while keeping the media sources, camera preview, and audio visualizer active. This is done by leaving the
// image and audio media sources running while temporarily selecting the idle reader profiles.
//
// The DMSDemo app presents one use case for this capabilitiy by pausing and resuming
// media sources while showing a popup alert on error notifications.
//
-(void)setReadersToIdle {
    self.lastImageProfile = self.dmsManager.imageReadingProfile;
    self.lastAudioProfile = self.dmsManager.audioReadingProfile;
    [self.dmsManager setImageReadingProfile:DMSProfileIdle];
    [self.dmsManager setAudioReadingProfile:DMSProfileIdle];
}

-(void)setReadersToOn {
    if (self.lastImageProfile)
        [self.dmsManager setImageReadingProfile:self.lastImageProfile];
    else
        [self.dmsManager setImageReadingProfile:DMSProfileLow];
    
    if (self.lastAudioProfile)
        [self.dmsManager setAudioReadingProfile:self.lastAudioProfile];
    else
        [self.dmsManager setAudioReadingProfile:DMSProfileLow];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Pause/Resume DMS While Media Sources Running] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Automatically Start/Stop DMS] */
// Handle view and app transition notifications
//
// With iOS7, viewWillAppear: and viewWillDisappear: are no longer called on leaving or returning
// to foreground application status. We now need to specifically hook these notifications.
//
- (void) registerNotificationCenterHooks {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackgroundNotificationHandler:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForegroundNotificationHandler:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void) unRegisterNotificationCenterHooks {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.audioMicSource removeObserver:self forKeyPath:@"volume" context:@"volumeDidChange"];
    
//    [self.audioMicSource addObserver:self
//                          forKeyPath:@"volume"
//                             options:0
//                             context:@"volumeDidChange"
}

- (void) applicationWillEnterForegroundNotificationHandler:(NSNotification*)notification {
    if(self.dmsManager.currentStatus == DMSStatusOpen) {
        DLog(@"DMSDemo scnnerViewController applicationWillEnterForegroundNotificationHandler");
        [self startDMSSources];
    }
}

- (void) applicationDidEnterBackgroundNotificationHandler:(NSNotification*)notification {
    DLog(@"DMSDemo scnnerViewController applicationDidEnterBackgroundNotificationHandler");
    [self stopDMSSources];
}

-(void)method_removeOldDataFromCache
{
    [errorItems removeAllObjects];
    [warningItems removeAllObjects];
    [payloadItems removeAllObjects];
    
    payloadItemNumber = 0;
    
    [self.dmsManager clearAudioPayloadCache];
    [self.dmsManager clearImagePayloadCache];
    [self.dmsManager clearAudioBuffers];
}



/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Automatically Start/Stop DMS] */


#pragma mark - DMSAudioVisualizerDelegate protocol

// Relay incoming audio data to visualizer UI.  This is a parallel reporting channel to the similar audio data flowing directly
// between the audioSource and DMSDK.
//
-(void) audioVisualizerDataAvailableWithData:(float *)data count:(int)count {
    [self.visualizer setVisualizerDataWithData:data count:count];
}


#pragma mark - DMSDelegate protocol

- (void) dms:(DMSManager*)dms statusChanged:(DMSStatus)status {
    NSLog(@"DMSDK Status Changed: %@", [DMSManager stringFromDMSStatus:status]);
    if(status == DMSStatusOpen) {
        [self.imageCameraSource start];
    }
}

- (void) dms:(DMSManager*)dms reportedError:(NSError*)error {
    NSLog(@"DMSDK reported error with domain:%@ code:%ld description:%@ reason:%@",
          error.domain, (long)error.code,
          [error.userInfo objectForKey:NSLocalizedDescriptionKey],
          [error.userInfo objectForKey:NSLocalizedFailureReasonErrorKey] );
    
    [self handleError:error];
}

- (void) dms:(DMSManager*)dms reportedWarningFrom:(NSString*)source message:(NSString*)msg {
    NSLog(@"DMSDK reported warning with source:%@ message:%@", source, msg);
    
    [self handleWarningFrom:source message:msg];
}

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [On Watermark Or Barcode Payload Detection] */
// Handle watermark and barcode detection
//
// The DMSDK reports separate notifications for audio watermarks, image watermarks, barcodes, and QR codes.
// For this DMSDemo app, we just redirect all 4 notifications to the same handler to translate payloads to payoffs.
//
- (void) dms:(DMSManager*)dms detectedImageWatermark:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    
    [self diagnosticsUISavePayload:payload withReaderInfo:readerInfo isNew:isNew];
    
    // If this is a new payload, then start the process of translating it to a payoff.
    //
    if(isNew) {
        [self resolvePayload:payload withReaderInfo:readerInfo context:@"DMSDemo"];
    }
}

- (void) dms:(DMSManager*)dms detectedAudioWatermark:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    [self dms:dms detectedImageWatermark:payload withReaderInfo:readerInfo isNew:isNew];
}

- (void) dms:(DMSManager*)dms detectedBarcode:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    
    [self dms:dms detectedImageWatermark:payload withReaderInfo:readerInfo isNew:isNew];
}

- (void) dms:(DMSManager*)dms detectedQRcode:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    
    [self dms:dms detectedImageWatermark:payload withReaderInfo:readerInfo isNew:isNew];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [On Watermark Or Barcode Payload Detection] */


#pragma mark - PayloadToPayoffResolver interface

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Resolve Payload Identifier to Payoff Content] */
// Resolve payloads to payoffs
//
- (void) resolvePayload:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo context:(NSString*)context {
    if(payload) {
        
        // Verify the user has provided us with DMResolver credentials (see notes in [self initializeDms]).
        //
        DMResolver* resolver = self.dmResolver;
        if((!resolver.userName) || (0 >= resolver.userName.length)
           || (!resolver.password) || (0 >= resolver.password.length)
           || (!resolver.serviceURL)) {
            
            NSString* source = @"Sample Application";
            NSString* msg = @"Bad or missing DMResolver username/password credentials in DMResolverSettings.plist.  "
            "Contact your Digimarc representative to assign unique credentials for your application.  "
            "These are required for all applications using the Digimarc Discover ID Manager Resolver service.";
            
            [self.dmsManager postWarningFrom:source message:msg];
            NSLog(@"%@: %@", source, msg);
        } else {
            
            // Show the Circle-D spinner branding whenever resolving a payload to payoff,
            // as required by Digimarc Mobile Application Verification Checklist.
            //
            if (!self.progressHUD && !self.customSpinner)
            {
                [self showSpinner];
                
                [self method_apertureSound];
            }
            
            // If the user has provided invalid DMResolver credentials, the resolver reports an error via either
            // [self payloadResolver:reportedError:] or [self payloadResolver:reportedWarningFrom:message:].
            //
            // Otherwise, the resolver resolves the payload to its associated payoff
            // and returns a resolve result containing both payload and payoff via
            // [self payloadResolver:resolvedNewPayoffWithResult:].
            //
            [self.payloadToPayoffResolver resolvePayload:payload];
        }
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Resolve Payload Identifier to Payoff Content] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Handling QR Code Resolver Errors] */
// Handling QR Code Resolver Errors
//

- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr reportedError:(NSError*)error {
    if(error.code == kDMResolverErrorQRCodeDoesNotContainURL && [error.domain isEqualToString:DigimarcResolverErrorDomain]) {
        // Downgrade QRCodes that do not contain a URL errors to warnings
        //
        [self handleWarningFrom:@"DMResolver" message:error.localizedDescription];
    } else {
        [self handleError:error];
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Handling QR Code Resolver Errors] */

- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr reportedWarningFrom:(NSString*)source message:(NSString*)msg {
    [self handleWarningFrom:source message:msg];
}

-(void)newItemAvailable:(PayoffDoc *)doc
{
     NSMutableArray *array_temp = [NSMutableArray arrayWithArray:[PayoffDatabase loadPayoffDocs]];
    for (int i=0; i<[array_temp count]; )
    {
        PayoffDoc* existingDoc = (PayoffDoc*)array_temp[i];
        if ([doc.payoffItem.payload isEqual:existingDoc.payoffItem.payload])
        {
            NSLog(@"Incoming payload:%@, found previous item with same payload value in history.  Deleting existing (duplicate) payoff.", doc.payoffItem.payload);
            [existingDoc deleteDoc];
            [array_temp removeObjectAtIndex:i];
        }
        else
        {
            // if not removing an existing doc, then increment iterator
            i++;
        }
        
        existingDoc = nil;
    }
    
    [array_temp insertObject:doc atIndex:0];
    [doc saveData];
    
    if ([array_temp count] > 30)
    {
        PayoffDoc *d = [array_temp lastObject];
        [d deleteDoc];
        [array_temp removeLastObject];
        d = nil;
    }
    
    array_temp = nil;
}

- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr resolvedNewPayoffWithResult:(DMResolveResult*)result
{
    self.dmsManager.delegate = nil;

    [self removeSpinner];
    
    if (bool_viewDisappeared == YES)
    {
//        [self.dmsManager closeSession];
        
        [self method_removeOldDataFromCache];
        return;
    }



    
//    NSLog(@"result: %@ result", result);
    NSLog(@"PayloadToPayoffResolver resolvePayload:%@ identified NEW PAYOFF:%@", result.payload, result.payoff.title);
    
    // save the payoff info of interest to us in the main panel UI payoff items database
    //
    __block PayoffDoc *newDoc = [[PayoffDoc alloc] init];
    newDoc.payoffItem.payload = result.payload;
    newDoc.payoffItem.title = result.payoff.title;
    if(! [result.payload isQRCode])
    {
        newDoc.payoffItem.subtitle = result.payoff.subtitle;
    }
    newDoc.payoffItem.actionToken = ((DMStandardPayoff *)result.payoff).actionToken;
    newDoc.payoffItem.content = ((DMStandardPayoff *)result.payoff).content;

    [self newItemAvailable:newDoc];
    
    // Do the heavy lifting thumbnail image graphics processing on a background thread
    //
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *imageData = NULL;
        if(result.payoff.thumbnailImageURL) {
            imageData = [NSData dataWithContentsOfURL:result.payoff.thumbnailImageURL];
        }
        
        if(imageData) {
            
            // If the payoff specified a thumbnail image URL
            // And if the raw thumbnail image data could be found and downloaded from the URL
            // Then use it as our icon
            //
            UIImage *image = [UIImage imageWithData:imageData];
            CGFloat newWidth = image.size.height < image.size.width ? image.size.height : image.size.width;
            CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, CGRectMake(0, 0, newWidth, newWidth));
            
            CGSize s = CGSizeMake(100, 100);
            UIGraphicsBeginImageContext(s);
            [[UIImage imageWithCGImage:imageRef] drawInRect:CGRectMake(0,0,s.width,s.height)];
            UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            imageData = UIImageJPEGRepresentation(newImage, 1.0);
            
        } else {
            
            // Else use a default icon that varies by payload type
            //
            switch(result.payload.payloadCategory) {
                default:
                case DMPayloadCategoryImage:
                    imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"DefaultImage.png"], 1.0);
                    break;
                    
                case DMPayloadCategoryAudio:
                    imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"AudioImage.png"], 1.0);
                    break;
                    
                case DMPayloadCategoryBarcode:
                    if([result.payload isQRCode]) {
                        imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"QRImage.png"], 1.0);
                    } else {
                        imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"BarImage.png"], 1.0);
                    }
                    break;
            }
        }
        
        // return to the main thread to update the main panel UI
        //
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [newDoc.payoffItem setImageData:imageData];
            [newDoc saveData];

            newDoc = nil;
            
            if(self.viewMode == kMyViewMode_Payoffs)
            {
            }
        });
    });
    
    [self method_stopBeep];
    
    
    if ([result.payoff.title intValue] == 0)
    {
        DLog(@"it means this is capture life");
    }
    else
    {
        DLog(@"it means this is ARC");
    }

    
//    DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:
//                 [NSString stringWithFormat:@"%@", result.payload], k_payload,
//                 result.payoff.title, k_title,
//                 result.payoff.subtitle, k_subTitle,
//                 ((DMStandardPayoff *)result.payoff).actionToken, k_accessToken,
//                 ((DMStandardPayoff *)result.payoff).content, k_content,
//                 result.payoff.thumbnailImageURL, k_imageURL,
//                 nil]);

    
    DLog(@"payload]: %@", [NSString stringWithFormat:@"%@", result.payload]);
    DLog(@"title: %@", result.payoff.title);
    DLog(@"subtitle: %@", result.payoff.subtitle);
    DLog(@"actionToken: %@", ((DMStandardPayoff *)result.payoff).actionToken);
    DLog(@"content: %@", ((DMStandardPayoff *)result.payoff).content);
    DLog(@"thumbnailImageURL: %@", result.payoff.thumbnailImageURL);
    DLog(@"description: %@", result.payoff.description);
    
//    self.dic_productInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
//                            [NSString stringWithFormat:@"%@", result.payload], k_payload,
//                            result.payoff.title, k_title,
//                            result.payoff.subtitle, k_subTitle,
//                            ((DMStandardPayoff *)result.payoff).actionToken, k_accessToken,
//                            ((DMStandardPayoff *)result.payoff).content, k_content,
//                            result.payoff.thumbnailImageURL, k_imageURL,
//                            nil];

    
    
    self.dic_productInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                            result.payoff.title, k_title, nil];

    
    
//    [self method_setupAndGoToTabBar];
    
//    bool_isComingFromActivity = NO;
//    
//    single.productDetail = nil;
//    
//    single.string_productID = [NSString stringWithFormat:@"%@", result.payoff.title];
//    
//    [self clearPayoffsHistory];
//    
//    
//    RXCustomTabBar *rxTabBar = (RXCustomTabBar *)self.tabBarController;
////    rxTabBar.selectedIndex = 4;
//    [rxTabBar selectTab:4];
    
    [self method_setupAndGoToTabBar];
}


#pragma mark - method_setupAndGoToTabBar
-(void)method_setupAndGoToTabBar
{
    bool_isComingFromActivity = NO;
    
    single.productDetail = nil;
    
    single.string_productID = [NSString stringWithFormat:@"%@", [self.dic_productInfo valueForKey:k_title]];
    
    DLog(@"%@", single.string_productID);
    
    [self clearPayoffsHistory];
    
    

    RXCustomTabBar *rxTabBar = (RXCustomTabBar *)self.tabBarController;
    
    rxTabBar.selectedIndex = 4;
    [rxTabBar selectTab:4];
    
//    if (IsPhone)
//    {
//        rxTabBar.selectedIndex = 4;
//        [rxTabBar selectTab:4];
//    }
//    else
//    {
//        rxTabBar.selectedIndex = 3;
//        [rxTabBar selectTab:3];
//    }
    
    
    
//    [self clearPayoffsHistory];
//    
//    bool_isComingFromActivity = NO;
//    
//    ProductDetailViewController *obj = (ProductDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
//    obj.dic_dataFromPrevControler = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[self.dic_productInfo valueForKey:k_title], k_title, nil];
//    
//    single.productDetail = nil;
//    
//    
//    [self.navigationController pushViewController:obj animated:YES];
}


#pragma mark - prepareForSegue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if ([segue.identifier isEqualToString:@"segue_productPage"])
//    {
//        bool_isComingFromActivity = NO;
//        
//        ProductDetailViewController *obj = (ProductDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
//        obj.dic_dataFromPrevControler = (NSMutableDictionary *)self.dic_productInfo;
//    }
}



- (NSUInteger) payoffsHistoryCount
{
//    return payoffsTableViewController.payoffsHistory.count;
    return 0;
}

- (void) clearPayoffsHistory {
    // remove all payoffs from the main UI data structure
//    while(payoffsTableViewController.payoffsHistory.count > 0)
//    {
//        PayoffDoc* doc = [payoffsTableViewController.payoffsHistory objectAtIndex:0];
//        [payoffsTableViewController.payoffsHistory removeObjectAtIndex:0];
//        [doc deleteDoc];
//    }
//    // refresh the main UI display
//    [payoffsTableViewController.tableView reloadData];
//    
//    // refresh the diags UI display if currently visible
//    if(self.viewMode == kMyViewMode_Payoffs)
//    {
//    }
}

#pragma mark - Payoffs History Table delegate (main panel UI)

-(void)didSelectItemWithTitle:(NSString*)title actionToken:(NSString *)actionToken content:(NSString *)content {
    [self reportActionAndDisplayContentWithTitle:title actionToken:actionToken content:content];
}


#pragma mark - Diagnostics UI

/****************************************************************************************
 *
 *    DMSDemo Application -- List of Lists diagnostics user interface
 *
 ****************************************************************************************/

- (void) toggleShowDiagnosticsOverlay
{
    
//    if (self.tableView.hidden)
//        self.tableView.hidden = NO;
//    else
//        self.tableView.hidden = YES;
}

- (void) diagnosticsUISimulatePayloadsFromPayoffsHistory {
    
    // walk payoffs history from back to front
    // inserting each item into front of diagnostics UI tables
    // result should be same sequence order in both main and diagnostic UI views
    //
//    for(NSInteger i = payoffsTableViewController.payoffsHistory.count; i > 0; i--) {
//        PayoffDoc* doc = payoffsTableViewController.payoffsHistory[i - 1];
//        PayoffItem* payoffItem = doc.payoffItem;
//        
//        // Inject a matching payload item into diagnostics UI payloads list
//        //
//        PayloadItem* payloadItem = [PayloadItem new];
//        payloadItem.itemNumber = payloadItemNumber++;
//        payloadItem.reportedDate = payoffItem.date;
//        payloadItem.payload = payoffItem.payload;
//        payloadItem.readerInfo = nil;
//        
//        [payloadItems insertObject:payloadItem atIndex:0];
//        
//        // The diagnostics UI payoffs list works directly from the main panel UI payoffsTableViewController.payoffsHistory data
//        // so no need to inject anything for payoffs.
//    }
}

- (void) diagnosticsUISavePayload:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo isNew:(BOOL)isNew {
    // Save the incoming DMPayload info of interest to us for the diagnostics UI
    //
    PayloadItem* item = [PayloadItem new];
    item.itemNumber = payloadItemNumber++;
    item.reportedDate = [NSDate date];
    item.payload = payload;
    item.readerInfo = readerInfo;
    
    [payloadItems insertObject:item atIndex:0];
    
    if(self.viewMode == kMyViewMode_Payloads)
    {
    }
}

- (NSString*) labelForPayload:(DMPayload*) payload {
    
    // This method summarizes the full DMPayload Common Payload Model (CPM) path string
    // to a user-friendly shorthand suitable for display in the diagnostics UI.
    //
    NSString* value = payload.valueString;
    NSString* type = nil;
    switch(payload.getPayloadCategory) {
        case DMPayloadCategoryImage:
            type = [NSString stringWithFormat:@"Image %@", payload.protocolVersion];
            break;
            
        case DMPayloadCategoryAudio:
            type = [NSString stringWithFormat:@"Audio %@", payload.protocol];
            break;
            
        case DMPayloadCategoryBarcode:
            type = [NSString stringWithFormat:@"Barcode %@", payload.protocol];
            break;
            
        default:
            type = @"Unknown";
            break;
    }
    NSString* label = [NSString stringWithFormat:@"%@ %@", type, value];
    return label;
}

#pragma mark - Display Detail Views (diagnostics UI)
//
//-(void) displayHelpView {
//    HelpViewController *helpVC = [[HelpViewController alloc] init];
//    [self presentViewController:helpVC animated:NO completion:nil];
//}


//- (void) displayTextViewWithText:(NSString*)text {
//    
//    TextViewController *textViewController = [[TextViewController alloc] initWithNibName:@"TextViewController" bundle:[NSBundle mainBundle]];
//    textViewController.contents = text;
//    
//    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:textViewController];
//    [textViewController.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Done"
//                                                                                              style:UIBarButtonItemStyleDone
//                                                                                             target:textViewController
//                                                                                             action:@selector(removeFromScreen)
//                                                              ]
//     ];
//    [self presentViewController:navC animated:YES completion:nil];
//}

#pragma mark - Display Payoffs (diagnostics UI, main panel UI, help UI)

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [ReportAction When Displaying a Payoff] */
// Report action
//
// Report all payoff URL viewings to DMResolver as required by Digimarc Mobile Application Verification Checklist.
// These are used for statistics and service hit count reporting to content owners.
//
-(void)reportActionAndDisplayContentWithTitle:(NSString*)title actionToken:(NSString *)token content:(NSString *)content {
    
    if(!token) {
        NSLog(@"ReportAction skipped for payoff:%@ with null action token", title);
    } else {
        NSLog(@"ReportAction started for payoff:%@ with action token:%@", title, token);
        
        [self.dmResolver reportAction:token completionBlock:^(NSString* actionToken, NSError* error){
            
            if(error) {
                NSLog(@"ReportAction failed for action token:%@ with error:%@", actionToken, error);
            } else {
                NSLog(@"ReportAction succeeded for action token:%@", actionToken);
            }
        }];
    }
    
    [self performSelector:@selector(displayURLInWebView:) withObject:[NSURL URLWithString:content] afterDelay:0.25];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [ReportAction When Displaying a Payoff] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [displayURLInWebView] */
// Display the payoff
//
// Display the URL in a modal web view, similar to the Help, Configuration, and Text detail views.
// If this is a payoff URL, the DMResolver reportAction:completionBlock: method should be
// called prior to this display.
//
-(void) displayURLInWebView:(NSURL *)url {
    
    WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController"
                                                                     bundle:[NSBundle mainBundle]];
    [webView setUrl:url];
    
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
    [webView.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                                   style:UIBarButtonItemStyleDone
                                                                                  target:webView
                                                                                  action:@selector(removeFromScreen)
                                                   ]
     ];
    [self presentViewController:navC animated:YES completion:nil];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [displayURLInWebView] */


#pragma mark - tableview dataSource and delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_promotionCode count]?[self.array_promotionCode count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_promotionCodes *cell = (cell_promotionCodes *)[tableView dequeueReusableCellWithIdentifier:@"cell_promotionCodes"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_promotionCodes" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell.label_promotionName setText:[[self.array_promotionCode objectAtIndex:indexPath.row] valueForKey:@"promotionName"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView_ deselectRowAtIndexPath:indexPath animated:YES];
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.google.com/"]];
}



#pragma mark - web service delegate
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    if ([k_webServiceLoginIdentity isEqualToString:serviceName])
    {
        GetProductDataFromServer *obj_getPomotionCodeData = [GetProductDataFromServer new];
        [obj_getPomotionCodeData method_getPromotionCode:dic_ serviceName:k_webServicePomotionCode];
        obj_getPomotionCodeData.delegate = self;
        DLog(@"promotion code service hit");
        obj_getPomotionCodeData = nil;

    }
    
    if ([k_webServiceGuestLogin isEqualToString:serviceName])
    {
        GetProductDataFromServer *obj_getPomotionCodeData = [GetProductDataFromServer new];
        [obj_getPomotionCodeData method_getPromotionCode:dic_ serviceName:k_webServicePomotionCode];
        obj_getPomotionCodeData.delegate = self;
        DLog(@"promotion code service hit");
        obj_getPomotionCodeData = nil;
    }
    
    
    
    
    if ([serviceName isEqualToString:k_webServicePomotionCode])
    {
        self.array_promotionCode = [[NSArray alloc] initWithArray:array_];
        NSLog(@"%@",self.array_promotionCode);
        [self.tableView_ reloadData];
    }

}
@end