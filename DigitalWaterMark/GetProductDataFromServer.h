#import <Foundation/Foundation.h>

#import "ProductsDataClass.h"

@protocol GetDateFromServer <NSObject>

@optional

//-(void)method_isDataSucess:(BOOL)bool_;


//-(void)method_data;

-(void)method_failureResponse;
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString*)serviceName;
-(void)method_dataFromServer:(ProductsDataClass *)product;

@end

@interface GetProductDataFromServer : NSObject
{
}

@property(nonatomic, strong)id <GetDateFromServer> delegate;

-(void)method_makeIDFromPayLoad:(id)id_;
- (void)method_fetchLocation:(NSString *)id_ City:(NSString *)city State:(NSString *)state Miles:(NSString *)miles;
-(void)method_fetchLocationByZipcode_Latitude:(NSString *)lat Longitude:(NSString *)lon;
-(void)method_addToCart:(NSDictionary *)dic serviceName:(NSString*)serviceName;
-(void)method_fetchDataToEditProduct:(id)id_;
-(void)method_guestLogin:(NSString*)serviceName;

-(void)method_shoppingCartList_serviceName:(NSString*)serviceName;
-(void)method_removeShoppingCart:(NSDictionary *)dic serviceName:(NSString*)serviceName;
-(void)method_editShoppingCart:(NSDictionary *)dic serviceName:(NSString*)serviceName;

-(void)method_getPromotionCode:(NSDictionary *)dic serviceName:(NSString*)serviceName;
-(void)method_loginidentity_serviceName:(NSString*)serviceName dic_credentials:(NSDictionary *)dic;
-(void)method_getProductImageBySKU:(NSString*)id_ serviceName:(NSString*)serviceName;
-(void)method_autocompleteAddress_input:(NSString *)input;
-(void)method_inventoryAvailability:(NSDictionary *)dic serviceName:(NSString*)serviceName;
@end