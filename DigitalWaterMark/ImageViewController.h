#import <UIKit/UIKit.h>
#import "Client.h"

@interface ImageViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,ClientDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *statusIndicator;
- (IBAction)takePhoto:(id)sender;

@end
