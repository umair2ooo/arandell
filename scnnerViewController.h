#import <UIKit/UIKit.h>
#import "DMSDelegateProtocol.h"
#import "DMResolver.h"
#import "DMSAudioMicAQSource.h"
#import "DMSImageCameraSource.h"

#import "PayloadToPayoffResolver.h"
#import "PayoffHistoryTableViewController.h"

@class DMSImageCameraSource;
@class DMSAudioMicAQSource;

@interface warningItem : NSObject
@property NSDate* date;
@property NSString* source;
@property NSString* message;
@end

@interface scnnerViewController : UIViewController
<
UIAlertViewDelegate,
DMSDelegateProtocol,
PayloadToPayoffResolverDelegate,
DMSAudioVisualizerProtocol,
PayoffHistoryTableDelegate
>

//-(void)method_getPromotionCode;

// DMSDelegate Integration
// (made public here for access by MyConfigController)
//
@property (retain) DMSManager* dmsManager;
@property (retain) DMResolver* dmResolver;
@property (retain) DMSImageCameraSource* imageCameraSource;
@property (retain) DMSAudioMicAQSource* audioMicSource;

//@property(nonatomic, strong)NSDictionary *dic_tokens;


// DMS Resolver & PayoffsHistory
//
@property (retain) PayloadToPayoffResolver* payloadToPayoffResolver;
- (NSUInteger) payoffsHistoryCount;
- (void) clearPayoffsHistory;

// Demo application user inteface
@property (retain) IBOutlet UIView* cameraPreviewContainer;

@property NSInteger viewMode;
@property BOOL cameraViewActive;


@end
