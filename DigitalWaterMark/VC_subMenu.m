#import "VC_subMenu.h"

#import "Singleton.h"

@interface VC_subMenu ()<UITableViewDataSource, UITableViewDelegate>
{
    Singleton *single;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;

- (IBAction)action_back:(id)sender;

@end

@implementation VC_subMenu

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    single = [Singleton retriveSingleton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table View DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IsPhone)
    {
        return 80;
    }
    else
    {
        return 123;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SampleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        NSLog(@"cai no init da cell");
    }

    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"Scanned History";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Help";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        return nil;
    }
    
    if (!IsPhone)
    {
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:20.0]];
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


#pragma mark - Table View Delegates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    single.isLogOut = false;
    
    
    if (indexPath.row == 0)
    {
        [self performSegueWithIdentifier:@"segue_goToScanHistory" sender:nil];
    }
    else if(indexPath.row == 1)
    {
        [self performSegueWithIdentifier:@"segue_goToHelp" sender:nil];
    }
}


@end