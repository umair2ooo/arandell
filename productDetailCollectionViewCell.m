#import "productDetailCollectionViewCell.h"

@implementation productDetailCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self)
    {
        // Initialization code
    }
    return self;
}

-(void)method_setValues:(NSDictionary *)dic
{
    DLog(@"dic: %@", dic);
    
    self.imageView_product.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", [dic valueForKey:k_id]]];
}

@end