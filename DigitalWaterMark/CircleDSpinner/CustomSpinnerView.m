/*************************************************************************************************** 
 
 
 The technology detailed in this software is the subject of various pending and issued patents, 
 both internationally and in the United States, including one or more of the following patents: 
 
 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827; 
 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366; 
 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487; 
 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686; 
 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1; 
 and JP-3949679, all owned by Digimarc Corporation. 
 
 Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software 
 conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark, 
 or copyright rights. 
 
 This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation, 
 USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is 
 important that this software be used, copied and/or disclosed only in accordance with such 
 agreements. 
 
 © Copyright, Digimarc Corporation, USA.  All Rights Reserved. 
 
 $File: //depot/Tech/RELEASE/DMSDK/v1.4/DMSDK/Platform/iOS/DMS/DMSDemo/src/CircleDSpinner/CustomSpinnerView.m $ 
 $Revision: #1 $ 
 $Date: 2014/07/30 $ 
 
 ***************************************************************************************************/

#import "CustomSpinnerView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomSpinnerView

@synthesize centerGraphic, spinnerGraphics, aDisplayLink;

-(void)setup
{
    currSpinnerGraphic=0;
//    self.centerGraphic = [UIImage imageNamed:@"DigimarcSpinnerBackground"];
    self.centerGraphic = [UIImage imageNamed:@"DigimarcSpinnerBackground1"];
    self.spinnerGraphics = [NSArray arrayWithObjects:
                            [UIImage imageNamed:@"DigimarcSpinner1"],
                            [UIImage imageNamed:@"DigimarcSpinner2"],
                            [UIImage imageNamed:@"DigimarcSpinner3"],
                            [UIImage imageNamed:@"DigimarcSpinner4"],
                            [UIImage imageNamed:@"DigimarcSpinner5"],
                            [UIImage imageNamed:@"DigimarcSpinner6"], 
                            [UIImage imageNamed:@"DigimarcSpinner7"],
                            [UIImage imageNamed:@"DigimarcSpinner8"],
                            [UIImage imageNamed:@"DigimarcSpinner9"],
                            [UIImage imageNamed:@"DigimarcSpinner10"], 
                            [UIImage imageNamed:@"DigimarcSpinner11"],
                            [UIImage imageNamed:@"DigimarcSpinner12"], 
                            [UIImage imageNamed:@"DigimarcSpinner13"], 
                            [UIImage imageNamed:@"DigimarcSpinner14"], 
                            [UIImage imageNamed:@"DigimarcSpinner15"], 
                            [UIImage imageNamed:@"DigimarcSpinner16"],       nil];
    self.backgroundColor = [UIColor clearColor];
}

-(void)startAnimation
{
    self.aDisplayLink = [[UIScreen mainScreen] displayLinkWithTarget:self selector:@selector(advance)];
    [self.aDisplayLink setFrameInterval:4];
    [self.aDisplayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}

-(void)stopAnimation
{
    if(self.aDisplayLink)
    {
        [self.aDisplayLink invalidate];
        self.aDisplayLink = nil;
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if(newSuperview)
        [self startAnimation];
    else
        [self stopAnimation];
}

-(void)dealloc
{
    [self.aDisplayLink invalidate];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)awakeFromNib
{
    [self setup];
}

-(void)advance
{
    currSpinnerGraphic++;
    if(currSpinnerGraphic>=[self.spinnerGraphics count])
        currSpinnerGraphic=0;
    [self setNeedsDisplay];
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    float midx = rect.size.width/2.0f;
    float midy = rect.size.height/2.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, midx, -midy);
    
    CGSize centerGraphicSize = [centerGraphic size];
    
    CGContextDrawImage(context, CGRectMake(-centerGraphicSize.width/2.0f, -centerGraphicSize.height/2.0f, centerGraphicSize.width, centerGraphicSize.height), centerGraphic.CGImage);
    
    
    UIImage *currentSpinnerGraphic = [self.spinnerGraphics objectAtIndex:currSpinnerGraphic];
    CGSize currentSpinnerGraphicSize = [currentSpinnerGraphic size];
    CGContextDrawImage(context, CGRectMake(-currentSpinnerGraphicSize.width/2.0f, -currentSpinnerGraphicSize.height/2.0f, currentSpinnerGraphicSize.width, currentSpinnerGraphicSize.height), currentSpinnerGraphic.CGImage);
}


@end
