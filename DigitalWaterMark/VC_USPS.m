#import "VC_USPS.h"

#import "Singleton.h"
#import "ScannedHistoryViewController.h"
@interface VC_USPS ()
{
    Singleton *single;
}

- (IBAction)action_activity:(id)sender;
- (IBAction)action_back:(id)sender;
- (IBAction)action_exit_USPS:(UIStoryboardSegue *)segue;


@end

@implementation VC_USPS


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    single = [Singleton retriveSingleton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(method_tabBar:)
                                                 name:k_notificationForTabBar
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForUSPS";
    
    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - action_exit_cart
- (IBAction)action_exit_USPS:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForUSPS"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;
        
        DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil]);
        
        single.string_productID = [NSString stringWithFormat:@"%@", obj.payOff_item.title];
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                       selector:@selector(method_popViewController)
                                       userInfo:nil
                                        repeats:NO];
    }
    else
    {
        DLog(@"Done only");
    }
}

#pragma mark - method_popViewController
-(void)method_popViewController
{
    [self.navigationController popViewControllerAnimated:NO];
}



#pragma mark - NSNotification method
-(void)method_tabBar:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:k_notificationForTabBar])
        [self.navigationController popToRootViewControllerAnimated:NO];
}
@end