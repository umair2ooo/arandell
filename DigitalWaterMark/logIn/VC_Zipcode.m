#import "VC_Zipcode.h"

#import <MapKit/MapKit.h>

@interface VC_Zipcode ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *text_ZipCode;
@property (weak, nonatomic) IBOutlet UILabel *label_LocationMsg;
@property (weak, nonatomic) IBOutlet UIImageView *image_locationIcon;



- (IBAction)action_ZipCode:(id)sender;
- (IBAction)action_Cancel:(id)sender;


@end

@implementation VC_Zipcode


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    if (![CLLocationManager locationServicesEnabled])
    {
        [self.label_LocationMsg setHidden:NO];
        [self.image_locationIcon setHidden:NO];
    }
    else
    {
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            [self.label_LocationMsg setHidden:NO];
            [self.image_locationIcon setHidden:NO];
        }
        else
        {
            [self.label_LocationMsg setHidden:YES];
            [self.image_locationIcon setHidden:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}




#pragma mark - action_ZipCode
- (IBAction)action_ZipCode:(id)sender
{
    [self.view endEditing:YES];
    
    if ([self.text_ZipCode.text length] >= 3)
    {
        [self performSegueWithIdentifier:@"segue_exitToLoginViaRegister" sender:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Failure"
                                    message:@"Invalid zip code."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}


#pragma mark - action_Cancel
- (IBAction)action_Cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - text field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end