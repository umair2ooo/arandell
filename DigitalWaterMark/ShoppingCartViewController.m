#import "ShoppingCartViewController.h"

#import "Cell_shoppingCart.h"
#import "ProductsDataClass.h"
#import "Singleton.h"
#import "SubmitFormViewController.h"
#import "AsyncImageView.h"
#import "EditCart_viewController.h"
#import "ScannedHistoryViewController.h"
#import "GetProductDataFromServer.h"
#import "ActivityIndicator.h"
#import "RXCustomTabBar.h"


#define k_combinationalProductId @"combinationalProductId"
#define k_mainProductId @"mainProductId"
#define k_Price @"Price"
#define k_prodName @"productName"
#define k_inputQuantity @"inputQuantity"
#define k_productImageURL @"productImageURL"
#define k_orderItemId @"orderItemId"



@interface ShoppingCartViewController ()<DeleteObject,GetDateFromServer>
{
    Singleton *single;
    int int_indexOfRemovingObject;
    int int_indexOfEditingObject;
}

@property(nonatomic, strong)ProductsDataClass *productsDataClass_obj;

@property(nonatomic, strong)NSString *string_quantity;

@property (weak, nonatomic) IBOutlet UITableView *tableView_;
@property (weak, nonatomic) IBOutlet UIView *view_heading;
@property (weak, nonatomic) IBOutlet UIButton *button_checkOut;
@property (weak, nonatomic) IBOutlet UILabel *label_totalItems;
@property (weak, nonatomic) IBOutlet UILabel *label_totalPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;
@property(nonatomic, strong)ProductsDataClass *obj_ProductsDataClass;


- (IBAction)action_activity:(id)sender;
- (IBAction)action_exit_cart:(UIStoryboardSegue *)segue;


@end


@implementation ShoppingCartViewController

@synthesize productsDataClass_obj = _productsDataClass_obj;
@synthesize string_quantity = _string_quantity;

#pragma mark - calculate total price and number of items
-(void)method_calculateTotal
{
    if ([single.array_cartObjects count])
    {
        __block int int_totalItems = 0;
        __block float float_totalPrice = 0.0;
        
        __block float float_price_temp = 0.0;
        
        
        [single.array_cartObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             DLog(@"%@", [obj valueForKey:k_Price]);
             
             NSArray *array_temp = [[obj valueForKey:k_Price] componentsSeparatedByString:@"$"];
             float_price_temp = [[array_temp objectAtIndex:1] floatValue];
             array_temp = nil;
             
             
             DLog(@"%d", [[obj valueForKey:k_inputQuantity] intValue]);
             DLog(@"%f", float_price_temp );
             DLog(@"%f", float_totalPrice);
             
             float_totalPrice = float_totalPrice + (float_price_temp * [[obj valueForKey:k_inputQuantity] intValue]);
             
             
             DLog(@"%f", float_totalPrice);
             
             int_totalItems = int_totalItems + [[obj valueForKey:k_inputQuantity] intValue];
             
             
             if (idx == [single.array_cartObjects count]-1)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.label_totalItems.text = [NSString stringWithFormat:@"%d", int_totalItems];
                     self.label_totalPrice.text = [NSString stringWithFormat:@"%.2f", float_totalPrice];
                 });
             }
         }];
    }
    else
    {
        self.label_totalItems.text = @"0";
        self.label_totalPrice.text = @"0.00";
    }
}


#pragma mark - method_updateFrame
-(void)method_updateFrame
{
    self.view_heading.center = self.view.center;
    self.tableView_.center = self.view.center;
//    self.button_checkOut.center = self.view.center;
    
    [self.view_heading setFrame:CGRectMake(0,
                                           64,
                                           self.view.frame.size.width,
                                           self.view_heading.frame.size.height)];
    
    [self.tableView_ setFrame:CGRectMake(0,
                                         self.view_heading.frame.origin.y+self.view_heading.frame.size.height,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height -(self.view_heading.frame.origin.y+self.view_heading.frame.size.height+self.tabBarController.tabBar.frame.size.height+self.button_checkOut.frame.size.height))];

    [self.button_checkOut setFrame:CGRectMake(self.button_checkOut.frame.origin.x,
                                              self.tableView_.frame.origin.y+self.tableView_.frame.size.height,
                                              self.button_checkOut.frame.size.width,
                                              self.button_checkOut.frame.size.height)];
}


#pragma mark - cycle
-(void)viewWillAppear:(BOOL)animated
{
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
    
    [single.array_cartObjects count]?(self.button_checkOut.enabled = YES):(self.button_checkOut.enabled = NO);
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];



    [self method_calculateTotal];

    [self.tableView_ reloadData];


//    self.obj_GetProductDataFromServer = [GetProductDataFromServer new];
//    self.obj_GetProductDataFromServer.delegate = self;
//    [self.obj_GetProductDataFromServer method_shoppingCartList];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    single = [Singleton retriveSingleton];
    
    [self method_updateFrame];


    UINib *nib = nil;
    
    if (IsPhone)
    {
        nib = [UINib nibWithNibName:@"Cell_shoppingCart" bundle:nil];
    }
    else
    {
        nib = [UINib nibWithNibName:@"Cell_shoppingCart_iPad" bundle:nil];
    }
    
    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_shoppingCart"];
    
    
//    UINib *nib = [UINib nibWithNibName:@"Cell_shoppingCart" bundle:nil];
//    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_shoppingCart"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - table view delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [single.array_cartObjects count]?[single.array_cartObjects count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_shoppingCart *cell = (Cell_shoppingCart *)[tableView dequeueReusableCellWithIdentifier:@"Cell_shoppingCart"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_shoppingCart" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.delegate = self;

    
    DLog(@"%@", [single.array_cartObjects objectAtIndex:indexPath.row]);
    

    
    cell.imageView_product.image = [UIImage imageNamed:@"Placeholder.png"];
    [cell.imageView_product loadImageFromURL:[NSString stringWithFormat:@"%@",[[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:k_productImageURL]]];
    
    
    cell.label_price.text = [[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:k_Price];
    cell.label_productName.text = [[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:k_prodName];
    cell.label_quantity.text = [[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:k_inputQuantity];
    
    if ([[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@k_colorURL])
    {
        [cell.view_colorsAndSizes setHidden:NO];
        
        cell.imageView_color.image = [UIImage imageNamed:@"Placeholder.png"];
        [cell.imageView_color loadImageFromURL:[NSString stringWithFormat:@"%@wcsstore/%@", k_url,[[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@k_colorURL]]];
        
        cell.label_size.text = [[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@k_size];
    }
    else
    {
        [cell.view_colorsAndSizes setHidden:YES];
    }
    
    
    [cell setTag:indexPath.row];
    
    
    //    [[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:k_combinationalProductId];
    //    [[single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:k_mainProductId];
    
    
//    [NSDictionary dictionaryWithObjectsAndKeys:
//     self.button_inputQuantity.titleLabel.text, @"inputQuantity",
//     single.productDetail.p_imageURL, @"productImageURL", nil]
    
    
    
    
    
//    self.obj_ProductsDataClass = [single.array_cartObjects objectAtIndex:indexPath.row];
//    cell.label_price.text = self.obj_ProductsDataClass.p_price;
//    cell.label_quantity.text = [NSString stringWithFormat:@"(QTY:%@)",self.obj_ProductsDataClass.p_quantity];
//    
//    self.productsDataClass_obj = [single.array_cartObjects objectAtIndex:indexPath.row];
    
    
    
//    self.productsDataClass_obj = nil;
//    
//    self.productsDataClass_obj = [single.array_cartObjects objectAtIndex:indexPath.row];
//    
//    cell.label_productName.text = self.productsDataClass_obj.p_name;
//    
//    
//    cell.imageView_product.image = [UIImage imageNamed:@"Placeholder.png"];
//    [cell.imageView_product loadImageFromURL:[self.productsDataClass_obj.p_imageURL absoluteString]];
//    
//    
//    cell.imageView_size.image = [UIImage imageNamed:@"Placeholder.png"];
//    [cell.imageView_size loadImageFromURL:[self.productsDataClass_obj.p_size_array objectAtIndex:0]];
//    
//    
//    cell.label_color.text = self.productsDataClass_obj.p_colorName;
//    cell.label_quantity.text = [NSString stringWithFormat:@"%d", self.productsDataClass_obj.p_quantityRequired];
//    
//    
//    NSArray *array_temp = [self.productsDataClass_obj.p_price componentsSeparatedByString:@"$: "];
//    cell.label_price.text = [NSString stringWithFormat:@"%.2f", [[array_temp objectAtIndex:1] floatValue] * self.productsDataClass_obj.p_quantityRequired];
//    array_temp = nil;
//    
//    [cell setTag:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - method_deleteObject
-(void)method_deleteObject:(int)tag
{
//    DLog(@"%d", tag);
//    
//    self.obj_ProductsDataClass = nil;
//    self.obj_ProductsDataClass = [single.array_cartObjects objectAtIndex:tag];
//    
//    GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
//    [obj_getProductData method_removeShoppingCart:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                   self.obj_ProductsDataClass.p_id,@"catEntryId",
//                                                   single.string_orderId, @"orderId",
//                                                   self.obj_ProductsDataClass.p_orderItemId,@"orderItemId", nil]];
//    
//    obj_getProductData = nil;
//    
//    
//
//    [self method_calculateTotal];
//    
//    [self.tableView_ reloadData];
    
    
    
    [[ActivityIndicator currentIndicator] show];
//
//    
    DLog(@"%d", tag);
    
    int_indexOfRemovingObject = tag;
    
    GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
    
    
//    {
//        orderId = 20015;
//        orderItem =     (
//                         {
//                             orderItemId = 60018;
//                         }
//                         );
//    }


    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                              [[single.array_cartObjects objectAtIndex:tag] valueForKey:k_orderItemId], @"orderItemId", nil];
    
    
    
    DLog(@"%@", [NSDictionary dictionaryWithObjectsAndKeys:
                 single.string_orderId, @"orderId",
                 [NSArray arrayWithObject:dic_temp], @"orderItem", nil]);
    
    
    
    

    obj_getProductData.delegate = self;
    
    
    [obj_getProductData method_removeShoppingCart:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   single.string_orderId,@"orderId",
                                                   [NSArray arrayWithObject:dic_temp], @"orderItem", nil] serviceName:k_webServiceRemoveCart];
    
    obj_getProductData = nil;
}




#pragma mark - method_editObject
-(void)method_editObject:(int)tag
{
    DLog(@"%d", tag);
    
    
    int_indexOfEditingObject = tag;
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Quantity"
                              message:@"Please enter quantity:"
                              delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:@"Cancel", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    /* Display a numerical keypad for this text field */
    
    UITextField *textField = [alertView textFieldAtIndex:0];
    
    textField.text = [[single.array_cartObjects objectAtIndex:tag] valueForKey:k_inputQuantity];
    
    textField.keyboardType = UIKeyboardTypeNumberPad;
    
    [alertView show];
    
    
//    EditCart_viewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EditCart_viewController"];
//    [self.navigationController pushViewController:obj animated:YES];
}


#pragma mark- uialert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if ([textField.text length])
        {
            self.string_quantity = textField.text;
            
            
            NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [[single.array_cartObjects objectAtIndex:int_indexOfEditingObject] valueForKey:k_orderItemId], k_orderItemId,
                                      textField.text, @"quantity",  nil];
            
            NSArray *array_temp = [NSArray arrayWithObjects:dic_temp, nil];
            
            NSDictionary *dic_tempTwo = [NSDictionary dictionaryWithObjectsAndKeys:
                                         array_temp, @k_orderItem,
                                         single.string_orderId, @"orderId", nil];
            
            DLog(@"%@", dic_tempTwo);
            
            
            GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
            obj_getProductData.delegate = self;
            [obj_getProductData method_editShoppingCart:dic_tempTwo serviceName:k_webServiceEditCart];
            obj_getProductData = nil;
            
//            [self.button_inputQuantity setTitle:textField.text forState:UIControlStateNormal];
        }
    }
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_submitForm"])
    {
//        SubmitFormViewController *obj = (SubmitFormViewController *)segue.destinationViewController;
//        obj.productsDataClass_obj = nil;
//        return;
    }
}


#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForCart";
    
    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}


#pragma mark - action_exit_cart
- (IBAction)action_exit_cart:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForCart"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;

        DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil]);

        GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
        obj_getProductData.delegate = self;
        
        single.string_productID = [NSString stringWithFormat:@"%@", obj.payOff_item.title];
        
        [obj_getProductData method_makeIDFromPayLoad:single.string_productID];

        obj_getProductData = nil;
        obj = nil;
    }
    else
    {
        DLog(@"Done only");
    }
}



#pragma mark - webservice response delegate
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    [[ActivityIndicator currentIndicator] hideAfterDelay];
    
    if ([serviceName isEqualToString:k_webServiceRemoveCart])
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Item removed successfully"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
        
        
        [single.array_cartObjects removeObjectAtIndex:int_indexOfRemovingObject];
        
        
        [self method_calculateTotal];
        
        [self.tableView_ reloadData];
    }
    
    if ([serviceName isEqualToString:k_webServiceEditCart])
    {
        if (isSuccess)
        {
            NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[single.array_cartObjects objectAtIndex:int_indexOfEditingObject]];
            
            
            [dic_temp setValue:self.string_quantity forKey:@"inputQuantity"];
            
            [single.array_cartObjects replaceObjectAtIndex:int_indexOfEditingObject withObject:dic_temp];
            
            DLog(@"%@", [single.array_cartObjects objectAtIndex:int_indexOfEditingObject]);
            
            
            [self method_calculateTotal];
            [self.tableView_ reloadData];
        }
    }
    
    if ([serviceName isEqualToString:k_webServiceGetMainProduct])
    {
        RXCustomTabBar *rxTabBar = (RXCustomTabBar *)self.tabBarController;
        
        [rxTabBar selectTab:4];
        
//        if (IsPhone)
//        {
//            [rxTabBar selectTab:4];
//        }
//        else
//        {
//            [rxTabBar selectTab:3];
//        }
        
        
//        self.tabBarController.selectedIndex = 4;
    }
    
    
//    if (self.array_ShoppingCartList)
//    {
//        [self.array_ShoppingCartList removeAllObjects];
//        self.array_ShoppingCartList = nil;
//    }
//    
//    
//    self.array_ShoppingCartList = [[NSMutableArray alloc] initWithArray:array_];
//    
//    self.obj_ProductsDataClass = [self.array_ShoppingCartList objectAtIndex:0];
//    
//    DLog(@"orderItemId:%@",self.obj_ProductsDataClass.p_orderItemId);
//    
//    //DLog(@"%@,%@",[dic_ valueForKey:@"grandTotal"],[dic_ valueForKey:@"grandTotalCurrency"]);
//    
//    if ([self.array_ShoppingCartList count])
//    {
//        self.label_totalItems.text = [NSString stringWithFormat:@"%ld",(unsigned long)[self.array_ShoppingCartList count]];
//    }
//    self.label_totalPrice.text = [dic_ valueForKey:@"grandTotal"];
//    [self.tableView_ reloadData];
}


-(void)method_failureResponse
{
    [[ActivityIndicator currentIndicator] hide];
    
    [[[UIAlertView alloc] initWithTitle:@"Sorry for inconvenience"
                                message:@"Input quantity is too high"
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil, nil]
     show];
    
    [self.tableView_ reloadData];
}


//-(void)method_data                                                          // item removed
//{
//    [[ActivityIndicator currentIndicator] hide];
//    
//    [[[UIAlertView alloc] initWithTitle:nil
//                                message:@"Item removed successfully"
//                               delegate:nil
//                      cancelButtonTitle:@"Ok"
//                      otherButtonTitles:nil, nil]
//     show];
//    
//    
//    [single.array_cartObjects removeObjectAtIndex:int_indexOfRemovingObject];
//    
//    
//    [self method_calculateTotal];
//    
//    [self.tableView_ reloadData];
//}


//-(void)method_isDataSucess:(BOOL)bool_
//{
//    if (bool_)              // it means positive response from web-service of edit product
//    {
//        NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[single.array_cartObjects objectAtIndex:int_indexOfEditingObject]];
//        
//        
//        [dic_temp setValue:self.string_quantity forKey:@"inputQuantity"];
//        
//        [single.array_cartObjects replaceObjectAtIndex:int_indexOfEditingObject withObject:dic_temp];
//        
//        DLog(@"%@", [single.array_cartObjects objectAtIndex:int_indexOfEditingObject]);
//        
//        
//        [self method_calculateTotal];
//        [self.tableView_ reloadData];
//    }
//}

@end