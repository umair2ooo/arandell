#import "Help_ViewController.h"

#import "Singleton.h"
#import "GetProductDataFromServer.h"
#import "ScannedHistoryViewController.h"

@interface Help_ViewController ()<GetDateFromServer>
{
    Singleton *single;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;

- (IBAction)action_exit_help:(UIStoryboardSegue *)segue;
- (IBAction)action_activity:(id)sender;

- (IBAction)action_back:(id)sender;

@end

@implementation Help_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    single = [Singleton retriveSingleton];

    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}
*/



#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForHelp";
    
    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}



#pragma mark - action_exit_help
- (IBAction)action_exit_help:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForHelp"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;

        DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil]);

        GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
        obj_getProductData.delegate = self;
        [obj_getProductData method_makeIDFromPayLoad:obj.payOff_item.title];

        obj_getProductData = nil;
        obj = nil;
    }
    else
    {
        DLog(@"Done only");
    }
}



#pragma mark - web service delegate
-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString*)serviceName
{
    if ([serviceName isEqualToString:k_webServiceGetMainProduct])
    {
        self.tabBarController.selectedIndex = 2;
    }
}


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end