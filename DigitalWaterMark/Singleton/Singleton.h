#import <Foundation/Foundation.h>


#import <UIKit/UIKit.h>
#import "ProductsDataClass.h"

@interface Singleton : NSObject
{
}
@property(nonatomic, strong)NSMutableArray *array_cartObjects;
@property(nonatomic, strong)NSURL *string_imageURL;

@property(readwrite)BOOL bool_isFollowModule;

@property(nonatomic, strong)NSMutableArray *array_colorsImages;
@property(nonatomic, strong)NSMutableArray *array_sizesImages;

@property(nonatomic, strong)ProductsDataClass *productDetail;
@property(nonatomic, strong)NSString *string_productID;

@property(nonatomic, strong)NSString *string_exitSegueIdentifier;

@property(nonatomic, strong)NSString *string_WCToken;
@property(nonatomic, strong)NSString *string_WCTrustedToken;
@property(nonatomic, strong)NSString *string_orderId;
@property(nonatomic, strong)NSString *string_singleSKUUniqueID;

@property(nonatomic)BOOL isLogOut;

@property(nonatomic, strong)UIImage *image_ARImage;





+(Singleton *)retriveSingleton;

//-(BOOL) method_connectToInternet;
-(BOOL) method_NSStringIsValidEmail:(NSString *)checkString;
-(BOOL) method_checkOnlySpaces:(NSString *)str;
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber;

+(UIImage *)method_setHeaderImage:(NSString *)id_;

@end