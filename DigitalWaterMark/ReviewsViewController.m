#import "ReviewsViewController.h"

#import "Cell_Reviews.h"
#import "AsyncImageView.h"
#import "Singleton.h"
#import "ScannedHistoryViewController.h"
#import "GetProductDataFromServer.h"


@interface ReviewsViewController ()<GetDateFromServer>
{
    NSArray *array_reviews;
    Singleton *single;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView_;
@property (weak, nonatomic) IBOutlet AsyncImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *label_productName;
@property (weak, nonatomic) IBOutlet UIView *view_backSubView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_header;

- (IBAction)action_activity:(id)sender;
- (IBAction)action_exit_review:(UIStoryboardSegue *)segue;
-(IBAction)action_goBack:(id)sender;

@end

@implementation ReviewsViewController

//@synthesize productsDataClass_obj = _productsDataClass_obj;

#pragma mark - method_updateFrame
-(void)method_updateFrame
{
    self.view_backSubView.center = self.view.center;
    self.tableView_.center = self.view.center;
    
    [self.view_backSubView setFrame:CGRectMake(0,
                                               72,
                                               self.view.frame.size.width,
                                               self.view_backSubView.frame.size.height)];
    
    [self.tableView_ setFrame:CGRectMake(0,
                                         self.view_backSubView.frame.origin.y+self.view_backSubView.frame.size.height,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height -(self.view_backSubView.frame.origin.y+self.view_backSubView.frame.size.height+self.tabBarController.tabBar.frame.size.height))];
}


#pragma mark - action_goBack
-(IBAction)action_goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    single = [Singleton retriveSingleton];
    
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@k_backButtonImage]
//                                                                             style:UIBarButtonItemStyleDone
//                                                                            target:self
//                                                                            action:@selector(action_goBack)];
    self.navigationItem.hidesBackButton = YES;
    
    array_reviews = [NSArray arrayWithObjects:single.productDetail.p_reviews, nil];
    
    [self method_updateFrame];

    //cancel loading previous image for cell
   // [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imageView_product];
    //set placeholder image or cell won't update when image is loaded
    self.imageView_product.image = [UIImage imageNamed:@"Placeholder.png"];
    //load the image
   // self.imageView_product.imageURL = self.productsDataClass_obj.p_imageURL;
    [self.imageView_product loadImageFromURL:[single.productDetail.p_imageURL absoluteString]];



    self.label_productName.text = single.productDetail.p_name;



    UINib *nib = [UINib nibWithNibName:@"Cell_Reviews" bundle:nil];
    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_Reviews"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.imageView_header setImage:[Singleton method_setHeaderImage:single.string_productID]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(method_tabBar:)
                                                 name:k_notificationForTabBar
                                               object:nil];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - table view delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_reviews count]?[array_reviews count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_Reviews *cell = (Cell_Reviews *)[tableView dequeueReusableCellWithIdentifier:@"Cell_Reviews"];
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_Reviews" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.label_review.text = [array_reviews objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - action_activity
- (IBAction)action_activity:(id)sender
{
    single.string_exitSegueIdentifier = @"segue_selectedProductForReview";
    
    ScannedHistoryViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ScannedHistoryViewController"];
    [self.navigationController presentViewController:obj animated:YES completion:nil];
}


#pragma mark - action_exit_review
- (IBAction)action_exit_review:(UIStoryboardSegue *)segue
{
    if ([segue.identifier isEqualToString:@"segue_selectedProductForReview"])
    {
        ScannedHistoryViewController *obj = (ScannedHistoryViewController *) segue.sourceViewController;
        
        DLog(@"%@", [[NSDictionary alloc] initWithObjectsAndKeys:obj.payOff_item.title, k_title, nil]);
        
//        GetProductDataFromServer *obj_getProductData = [GetProductDataFromServer new];
//        obj_getProductData.delegate = self;
        
        
        single.string_productID = [NSString stringWithFormat:@"%@", obj.payOff_item.title];
        
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                       selector:@selector(method_popViewController)
                                       userInfo:nil
                                        repeats:NO];
        
//        [self.navigationController popToRootViewControllerAnimated:NO];
        
        
//        [obj_getProductData method_makeIDFromPayLoad:single.string_productID];
        
//        obj_getProductData = nil;
//        obj = nil;
    }
    else
    {
        DLog(@"Done only");
    }
}


-(void)method_popViewController
{
    [self.navigationController popViewControllerAnimated:NO];
}




-(void)method_dataWithParams:(NSArray *)array_ dic:(NSDictionary *)dic_ isSuccess:(BOOL)isSuccess serviceName:(NSString *)serviceName
{
    if ([serviceName isEqualToString:k_webServiceGetMainProduct])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma mark - NSNotification method
-(void)method_tabBar:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:k_notificationForTabBar])
        [self.navigationController popToRootViewControllerAnimated:NO];
}
@end